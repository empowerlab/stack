---
id: roadmap
title: Roadmap
---

The Empower Stack is only at the early-alpha stage right now. You'll find on this page our next priorities and what we're planning to implement next.

Remember this project is not commercial, no editor, no investor. To move forward faster, help will be needed even on tiny things, like reviewing, testing, documenting etc...

## Next steps

In general, our following priorities will be to promote the project and ensure it become usable in production. To do this, we need to:
- Review the golang libraries and ensure they are of high quality, especially the ORM. We are still looking for Golang experts to help us with this regard.
- Implement exemplary tests, to have at least 70% coverage. This much is needed to reference the project in some important lists, like [awesome-go](https://github.com/avelino/awesome-go) for instance. We will also implement contrat-driven testing with [Pact](pact.io), to avoid accidentally breaking consumer services which use your APIs.
- Implement benchmarks, to have an idea of how the stack compare to other ORM-like projects, and have a reference to improve the performance in the future.
- Improve React-Admin, the GUI is not entirely usable right now and the OpenCRUD driver does not seem to be maintained anymore. The One2many fields especially need to be improved.
- Having at least a dozen projects considering to use the stack and working on a prototype with us. Having feedback will be critical.


## In the future

To start as early as possible, we reduced the scope of the stack to the bare minimum to be usable, with the CRUD backends, the gateway, the authentification, and the distributing tracing. Here what is on the roadmap for the following features:

- Providing client libraries to build custom web frontends and mobile applications. Right now we only provide the Admin GUI as client, you'll need to build the clients yourself and use the API Gateway.
  We are considering to use React as frontend technology, especially [Next.js](nextjs.org) to ensure the server-side rendering, necessary for proper search engine referencement. We also plan to use React Native to build the mobile applications, coupled with React we may be able to have the clients be built into the same codebase, sharing some functions like the ones getting data from the gateway.
- Implementing Prometheus to collect metrics on how the production is running, sending alerts when something goes wrong. This should also allow us to automatically revert deployment if a sudden rise of errors occurs.
- Implementing service mesh with [Istio](istio.io) and [Envoy](envoyproxy.io). This is very important to manage good communication between your services, implementing circuit breaker, zero-trust network, and some other patterns.
- Having a driver to search data into [Vault](vaultproject.io). This will allow us to completely secure some data, ensuring the stack become usable in some highly secure and critical organizations.
