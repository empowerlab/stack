---
id: kubernetes
title: Installing and deploying on your Kubernetes/production environment
sidebar_label: Deploying on Kubernetes
---

## Your Kubernetes cluster

The Empower Stack is designed to be used on [Kubernetes](https://kubernetes.io/), the now de facto standard to orchestrate containers on the cloud.

You can install Kubernetes on your own servers if you want, but maintaining the installation is a difficult task and we instead recommend to use one of the managed Kubernetes services provided by most of the cloud providers.
You can find such a Kubernetes service for as low as 10$/month, like [the service of Digital Ocean](https://www.digitalocean.com/products/kubernetes/) which we use for our own tests, the example project works with a single droplet which is perfect for people looking to learn Kubernetes and microservices on their spare time. 
It's important to say it again, the stack works with any Kubernetes installation, you are not forced to use the services of any specific cloud provider.

Once your Kubernetes cluster is created, your cloud provider shall give you instructions to connect to your cluster. This usually means:
- [Installing the kubectl cli tool](https://kubernetes.io/fr/docs/tasks/tools/install-kubectl/), which will allow you to perform administrative operations on your Kubernetes cluster.
- Downloading the certificate on your cloud provider website so you can connect to your cluster with kubectl.

We also strongly recommend to [install the Kubernetes Dashboard](https://kubernetes.io/docs/tasks/access-application-cluster/web-ui-dashboard/), which will provide a GUI of your Kubernetes Cluster.


## Link your Gitlab repository

Now that you have a working Kubernetes, we have to link it to your Gitlab repository. We assume that you have already forked the example repository and so have control of the repository settings.

Follow the steps in [the Gitlab documentation to add an existing cluster](https://docs.gitlab.com/ee/user/project/clusters/add_remove_clusters.html#add-existing-cluster). Don't be fooled by the quote of GKE and EKS services, you can add any Kubernetes cluster by following these methods.
You will mostly have to provide:
- The name of your Kubernetes cluster.
- The API URL Gitlab will connect to.
- The CA certificate and the token so Gitlab can authenticate.

Also, configure the base domain of your services (for example demo.empower.sh if the services will be served at admin.demo.empower.sh, api.demo.empower.sh etc...).

Finally, deploy [the Gitlab managed apps](https://docs.gitlab.com/ee/user/clusters/applications.html). If your cluster is correctly configured, you just have to click on the corresponding buttons and Gitlab will take care of everything.
You don't want to install everything, you mainly want to install:
- The helm app, needed by Gitlab to deploy your applications.
- The cert-manager app, which will manage the SSL/Letsencrypt certificate of your application URLs.

Be careful about installing the ingress app. This is also an important app, needed to open access to your applications to the public Internet, but using the Gitlab app for this will usually tell your cloud provider to install a load balancer, which can be expensive.
This is what you want to do for a production environment, but if you are just fooling around on your personal time and want to keep cost at the bare minimum, another solution is provided later on this page.


## The gitlab-ci.yml file

That's it. If your trigger a Gitlab pipeline, it shall deploy your whole architecture on your Kubernetes cluster.
All steps are described in the gitlab-ci.yml file at the root of your application. Let's break them down.

- First, Gitlab CI will build the Docker images which will be deployed on Kubernetes and store them on his container registry. Note that these images are also used by [the docker-compose.yml file for setting your local environment](local.md).
- Then tests and linters will be executed to make sure no regressions were introduced.
- Finally, Gitlab CI will create the deployments on your Kubernetes cluster, and all resources they need.

For the deployments, it will use the Kubernetes yaml files in the kubernetes/ folder, giving you complete control of what is deployed on your cluster.
Also, the deployment will be done in a specific namespace, depending on the branch of your Git commit. This means you can easily create a full testing environment, without impacting the production namespace linked to the master branch.


## Publishing your applications (Ingress, SSL, DNS, and firewall)

In Kubernetes, your applications are exposed to the Internet through the Ingress resources, but by default, there is no engine installed to transfer the public requests to your services.

The easiest way to configure this just to install the Ingress app in your Gitlab repository, this is also the recommended way to establish a production environment because this will usually set up a load balancer on your cloud provider, the professional way to expose, scale and protect your services.

This is a great solution, but a load balancer is a cost we may want to avoid. In this case, we can use the [ingress-nginx service](https://kubernetes.github.io/ingress-nginx/). By using the NodePort mode, the ingress-nginx will listen to specific ports on any of your Kubernetes nodes, and redirect the requests to your services inside.

First, launch these commands to install ingress-nginx with the baremetal configuration:
```
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/static/mandatory.yaml
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/static/provider/baremetal/service-nodeport.yaml
```

Then you need to redirect all incoming requests on 80/HTTP and 443/HTTPS ports to your ingress-nginx service. To do this, you need to edit the ingress-nginx service resource in the ingress-nginx namespace and add theses lines:
```
spec:
  externalIPs:
  - 203.0.113.2
  - 203.0.113.3
```
Where the IPs are public IPs of your nodes. 

And that's it. The example repository contains Ingress resource for each of your services. By updating the hosts file of your system you should already be able to access the following urls:
admin.YOUR_BASE_DOMAIN
api.YOUR_BASE_DOMAIN
hydra.YOUR_BASE_DOMAIN
login.YOUR_BASE_DOMAIN
jaeger.YOUR_BASE_DOMAIN

To manage SSL on your application, you'll also need to [install the cert manager](https://docs.cert-manager.io/en/latest/getting-started/install/kubernetes.html):
```
kubectl apply -f https://github.com/jetstack/cert-manager/releases/download/v0.11.0/cert-manager.yaml
```

And install the LetsEncrypt issuer, to automatically generate your certificate. An example issuer is available in the example repository:
```
kubectl apply -f kubernetes/issuer-letsencrypt.yml
```

Now you can finally update the DNS record of your domain to point the URLs to your Kubernetes cluster. Also, note that the cert manager manages LetsEncrypt certificates, so when you point your DNS to the cluster it will become able to do the required verifications, and SSL will be automatically configured on your services.

Finally, make sure the 80 and 443 are not blocked by the firewall of your cloud provider.


## You can customize everything

Remember that you can change everything in your project repository. The core of the Empower Stack is contained in the [stack repository](https://gitlab.com/empowerlab/stack), this is where the libraries, the code generators, the special services like the login services, everything which needs to be a shared work are located.

This means that the example repository you used as a foundation for your project contains what you'd want to keep under your direct control. The stack provides as many libraries as needed to simplify the maintenance of your architecture, but in your project repository you are the one deciding how you are building it.
This includes your custom services, the code of the gateway, but also the gitlab-ci.yml and the kubernetes yaml files which are used to deploy on your Kubernetes Cluster.

The example repository provides a helpful starting point. Still, you can customize theses files to include any deployment pattern, any software, or legacy applications you need to create the architecture of your project or your company.

