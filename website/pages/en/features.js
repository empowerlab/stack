/**
 * Copyright (c) 2017-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

const React = require('react');

const CompLibrary = require('../../core/CompLibrary.js');

const Container = CompLibrary.Container;

function Help(props) {

  const {config: siteConfig} = props;
  const {baseUrl} = siteConfig;

  const titleStyle = {
    fontFamily: "Lato",
    textAlign: "center",
    margin: 0,
  }

  const titleSpanStyle = {
    borderBottom: "1px solid #50E3C2",
  }
  
  const clear = {
    clear: "both",
  }

  return (
    <div className="docMainWrapper wrapper">
      <Container className="mainContainer documentContainer postContainer">
        <div>
          <h1 style={titleStyle}><span style={titleSpanStyle}>The features</span></h1>
        </div>
        <div className="w-container featuresWContainer">
          <div className="w-row">
            <div className="w-col w-col-6 featuresText">
              <h2>Not a framework</h2>
              <p>
                When you use a framework, you learn the framework, not the language.
                <br/>
                One of our goals is to empower the developers, to let them learn how to use the best technologies around here.
                <br/><br/>
                This is why we provide libraries you call from your code, and use code generation instead to keep you productive.
              </p>
            </div>
            <div className="w-col w-col-6">
              <img src={`${baseUrl}img/features/stack.png`} alt="" className="imgFeaturesRight"/>
            </div>
          </div>
        </div>
        <div style={clear}/>
        <div className="w-container featuresWContainer">
          <div className="w-row">
            <div className="w-col w-col-6">
              <img src={`${baseUrl}img/features/api.png`} alt="" className="imgFeaturesLeft"/>
            </div>
            <div className="w-col w-col-6 featuresText">
              <h2>Build your Web Application</h2>
              <p>
                The web applications used by your customers have different requirements than your internal systems. It needs to scale to the enormous amount of users you may have, and to the various devices they may be using.
                <br/>
                <ul>
                  <li>Use only massively scalable technologies (Golang, GRPC, Protobuf, etc...)</li>
                  <li>Headless / API-based architecture</li>
                  <li>Use React and React Native to build your web and mobile application in a single codebase</li>
                  <li>Benchmarked libraries‍</li>
                </ul>
              </p>
            </div>
          </div>
        </div>
        <div style={clear}/>
        <div className="w-container featuresWContainer">
          <div className="w-row">
            <div className="w-col w-col-6 featuresText">
              <h2>Build your ERP</h2>
              <p>
                There are several definitions of an ERP system, to us this represents the services managing the internal processes of your organization.
                <br/>
                Keeping these processes flexible and maintainable is essential, and primarily depends on your backend engineer's productivity.
                <br/><br/>
                <ul>
                  <li>Excellent at managing CRUD operations</li>
                  <li>Easy to maintain administrative GUI</li>
                  <li>Centralized authentification</li>
                  <li>Complex and secured rules to access resources</li>
                </ul>
              </p>
            </div>
            <div className="w-col w-col-6">
              <img src={`${baseUrl}img/features/erp.png`} alt="" className="imgFeaturesRight"/>
            </div>
          </div>
        </div>
        <div style={clear}/>
        <div className="w-container featuresWContainer">
          <div className="w-row">
            <div className="w-col w-col-6">
              <img src={`${baseUrl}img/features/orm.png`} alt="" className="imgFeaturesLeft"/>
            </div>
            <div className="w-col w-col-6 featuresText">
              <h2>Next-gen ORM</h2>
              <p>
                Usually, the ORM is the part of an application controlling the transactions with the database.
                <br/>
                To ensure easy communications between your services, we pushed further the concept so our ORM can search for data in other services, in GraphQL/REST APIs, and any other source of data.
                <br/><br/>
                It can also operate the data from your legacy application, allowing you a slow and unpainful transition to the microservice architecture by ensuring compatibility between new and old systems.
              </p>
            </div>
          </div>
        </div>
        <div style={clear}/>
        <div className="w-container featuresWContainer">
          <div className="w-row">
            <div className="w-col w-col-6 featuresText">
              <h2>A complete DevOps process</h2>
              <p>
                Our exemplary application provides a complete process for your development cycle.
                <br/>
                <ul>
                  <li>Review issues and merge requests in the code repository</li>
                  <li>Build your application in Docker images</li>
                  <li>Avoid breaking other services thanks to contract-driven testing</li>
                  <li>Deploy your images on Kubernetes</li>
                  <li>Manage all the code of your team in a single monorepo</li>
                </ul>
                All this process is completely customizable through the CI and Kubernetes files, we provide an easy start but then it's up to you to build the process adapted to your organization. ‍
              </p>
            </div>
            <div className="w-col w-col-6">
              <img src={`${baseUrl}img/features/pipeline.png`} alt="" className="imgFeaturesRight"/>
            </div>
          </div>
        </div>
        <div style={clear}/>
        <div className="w-container featuresWContainer">
          <div className="w-row">
            <div className="w-col w-col-6 featuresText">
              <img src={`${baseUrl}img/features/monitoring.png`} alt="" className="imgFeaturesLeft"/>
            </div>
            <div className="w-col w-col-6">
              <h2>Monitoring and security</h2>
              <p>
                Finally, all the tools to build a state-of-the-art platform are included in the stack.
                <br/><br/>
                Including:
                <ul>
                  <li>Distributing tracing with Jaeger, to see the life of the requests</li>
                  <li>Alerts and metrics with Prometheus</li>
                  <li>Health checks and automatic rollbacks</li>
                  <li>Service mesh, to include circuit breakers and set a zero-trust network‍</li>
                </ul>
              </p>
            </div>
          </div>
        </div>
      </Container>
    </div>
  );
}

module.exports = Help;
