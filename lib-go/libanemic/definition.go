package libanemic

import (
	"fmt"
	"strings"

	"gitlab.com/empowerlab/stack/lib-go/libinfrastructure/libpersistence"
)

type Definitions struct {
	Package         string
	Path            string
	PersistencePath string
	// UseTenants bool
	slice []*ServiceDefinition
	byIds map[string]*ServiceDefinition
}

// Register is used to register a new definition into the service.
func (ds *Definitions) Register(a *ServiceDefinition) {

	if ds.byIds == nil {
		ds.byIds = map[string]*ServiceDefinition{}
	}

	ds.slice = append(ds.slice, a)
	ds.byIds[a.Name] = a

}

// Slice return the definitions as a slice.
func (ds *Definitions) Slice() []*ServiceDefinition {
	result := ds.slice

	return result
}

// GetByID return the specified definition by its ID.
func (ds *Definitions) GetByID(id string) *ServiceDefinition {
	d := ds.byIds[id]

	if d == nil {
		panic(fmt.Sprintf("The model definition %s doesn't exist", id))
	}
	return d
}

// Definition is used to declare the information of a model, so it can generate its code.
type ServiceDefinition struct {
	Name       string
	Aggregates []*libpersistence.AggregateDefinition
}

func (f *ServiceDefinition) Title() string {
	return strings.Title(f.Name)
}
