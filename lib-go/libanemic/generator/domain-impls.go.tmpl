package implementations

import (
	"time"

	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain"
	gen "{{.Path}}/gen/domains/domains/{{.Service.Name}}/gen"
	"{{.Path}}/gen/domains/domains/{{.Service.Name}}/gen/events"
	"{{.Path}}/gen/domains/domains/{{.Service.Name}}/gen/identities"
)

func New{{.Aggregate.AggregateRoot.Title}}(
	ctx libdomain.ApplicationContext, tenantId libdomain.TenantID, new{{.Aggregate.AggregateRoot.Title}}ID identities.{{.Aggregate.AggregateRoot.Title}}ID,
	{{- range .Aggregate.AggregateRoot.Properties }}
	{{- if checkReservedProperty .}}
	{{- if eq .Type "Many2oneType"}}
	{{- else if eq .Type "One2manyType"}}
	{{- else if eq .Type "Many2manyType"}}
	{{else if eq .Type "DatetimeType"}}
	{{else if eq .Type "DateType"}}
	{{else if eq .Type "IntegerType"}}
	{{.Name}} int,
	{{else if eq .Type "FloatType"}}
	{{.Name}} float64,
	{{else if eq .Type "BooleanType"}}
	{{.Name}} bool,
	{{- else if and (eq .Type "TextType") .IsTranslatable}}
	{{.Name}} map[string]string,
	{{- else }}
	{{.Name}} string,
	{{- end }}
	{{- end }}
	{{- end }}
) (*gen.{{.Aggregate.AggregateRoot.Title}}, *events.{{.Aggregate.AggregateRoot.Title}}CreatedEvent, error) {

	event := events.New{{.Aggregate.AggregateRoot.Title}}CreatedEvent(
		ctx,
		{{- range .Aggregate.AggregateRoot.Properties }}
		{{- if checkReservedProperty .}}
		{{- if eq .Type "Many2oneType"}}
		{{- else if eq .Type "One2manyType"}}
		{{- else if eq .Type "Many2manyType"}}
		{{else if eq .Type "DatetimeType"}}
		{{else if eq .Type "DateType"}}
		{{- else }}
		{{.Name}},
		{{- end }}
		{{- end }}
		{{- end }}
	)
	{{.Aggregate.AggregateRoot.Name}}, err := gen.New{{.Aggregate.AggregateRoot.Title}}Aggregate(
		ctx, tenantId, new{{.Aggregate.AggregateRoot.Title}}ID, event)
	if err != nil {
		return nil, nil, err
	}

	return {{.Aggregate.AggregateRoot.Name}}, event, nil
}

func init() {

	events.{{.Aggregate.AggregateRoot.Title}}When{{.Aggregate.AggregateRoot.Title}}CreatedImplementation = func(setters *gen.{{.Aggregate.AggregateRoot.Title}}Setters, event *events.{{.Aggregate.AggregateRoot.Title}}CreatedEvent) {
		{{- range .Aggregate.AggregateRoot.Properties }}
		{{- if checkReservedProperty .}}
		{{- if eq .Type "Many2oneType"}}
		{{- else if eq .Type "One2manyType"}}
		{{- else if eq .Type "Many2manyType"}}
		{{else if eq .Type "DatetimeType"}}
		{{else if eq .Type "DateType"}}
		{{- else }}
		setters.Set{{.Title}}(event.{{.Title}}())
		{{- end }}
		{{- end }}
		{{- end }}
	}
}

var _ time.Time