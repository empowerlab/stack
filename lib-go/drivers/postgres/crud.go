package postgres

import (
	"fmt"
	"strconv"
	"strings"

	// "github.com/jmoiron/sqlx"

	"github.com/pkg/errors"

	"gitlab.com/empowerlab/stack/lib-go/libdomain/libapplication"

	// "gitlab.com/empowerlab/stack/lib-go/libdata"
	"gitlab.com/empowerlab/stack/lib-go/libinfrastructure/libpersistence"
)

// Store is an interface containing the functions needed to build the arguments
// specifics to the model type.
// type Store interface {
// 	InsertBuildArgs([]libdata.ModelInterface) ([]string, map[string]interface{})
// 	UpdateBuildArgs(interface{}, *fieldmask.FieldMask) ([]string, map[string]interface{})
// }

// Insert will insert the new record in the database.
func (d *Driver) InsertDefault(
	t libpersistence.TableInterface, wk *libapplication.ApplicationContext, records []interface{},
	fields []string, args map[string]interface{},
	properties []string,
) (interface{}, error) {

	/* span, _ := opentracing.StartSpanFromContext(wk.Workflow().Context, "postgres-"+t.GetName()+"-insert")
	defer span.Finish()
	wk.Workflow().Context = opentracing.ContextWithSpan(wk.Workflow().Context, span) */

	fmt.Printf("records %+v\n", records)

	// fmt.Printf("%+v", tx.Tenant)
	// fields, args := insertBuildArgs(records)
	var values []string
	for i := range records {
		var recordValues []string
		for _, field := range fields {
			recordValues = append(recordValues, fmt.Sprintf(":%v_%s", i, field))
		}
		if t.UseTenants() { //tx.Cluster.MultiTenant {
			tenantName := fmt.Sprintf(":%v_tenant_id", i)
			recordValues = append([]string{tenantName}, recordValues...)
			args[fmt.Sprintf("%v_tenant_id", i)] = wk.Workflow().TenantID
		}
		values = append(values, strings.Join(recordValues, ","))
	}
	if t.UseTenants() {
		fields = append([]string{"tenant_id"}, fields...)
	}

	fmt.Printf("fields %s\n", fields)
	fmt.Printf("values %s\n", values)
	fmt.Printf("args %+v\n", args)
	fmt.Printf("properties %+v\n", properties)

	//nolint:gas
	q := fmt.Sprintf(
		"INSERT INTO \"%s\" (%s) VALUES (%s) RETURNING %s",
		t.Snake(), strings.Join(fields, ","), strings.Join(values, "),("),
		strings.Join(properties, ","))
	/*span.LogFields(
		otlog.String("query", q),
		otlog.Object("args", args),
	)*/
	fmt.Println(q)

	// rows, err := wk.Transaction.(*Tx).QueryRows(q, args)
	// if err != nil {
	// 	return nil, errors.Wrap(err, "Couldn't insert record")
	// }
	// rows, err := libapplication.ExecuteActivity(
	// 	wk.OrchestratorClient, wk, &InsertActivity{
	// 		Workflow: wk,
	// 		Query:    q,
	// 		Args:     args,
	// 	}, d)
	rows, err := wk.Transaction.(*Tx).QueryRows(wk, q, args)
	if err != nil {
		return nil, errors.Wrap(err, "Couldn't update record")
	}
	if err != nil {
		return nil, err
	}

	return rows, nil

}
func (d *Driver) Insert(
	t libpersistence.TableInterface, wk *libapplication.ApplicationContext, records []interface{},
	fields []string, args map[string]interface{},
	properties []string,
) (interface{}, error) {

	if d.InsertCustom != nil {
		return d.InsertCustom(d, t, wk, records, fields, args, properties)
	} else {
		return d.InsertDefault(t, wk, records, fields, args, properties)
	}
}

// Update will update the data in the filtered records.
func (d *Driver) Update(
	t libpersistence.TableInterface, wk *libapplication.ApplicationContext,
	filters []*libpersistence.Filter, record interface{}, set []string, args map[string]interface{},
	properties []string,
) (interface{}, error) {
	/*span, _ := opentracing.StartSpanFromContext(wk.Workflow().Context, "postgres-"+t.GetName()+"-update")
	defer span.Finish()
	wk.Workflow().Context = opentracing.ContextWithSpan(wk.Workflow().Context, span) */

	// set, args := updateBuildArgs(record, updateMask)

	filterQuery, filterArgs, err := buildFilters(wk, t, filters, nil)
	if err != nil {
		return nil, errors.Wrap(err, "Couldn't build where query")
	}
	for key, arg := range filterArgs {
		args[key] = arg
	}

	//nolint:gas
	q := fmt.Sprintf(
		"UPDATE \"%s\" SET %s %s RETURNING %s",
		t.Snake(), strings.Join(set, ","), filterQuery, strings.Join(properties, ","))
	/*span.LogFields(
		otlog.String("query", q),
		otlog.Object("args", args),
	)*/
	rows, err := wk.Transaction.(*Tx).QueryRows(wk, q, args)
	if err != nil {
		return nil, errors.Wrap(err, "Couldn't update record")
	}
	return rows, nil

}

// Select will return the filtered records.
func (d *Driver) Get(
	t *libpersistence.TableDefinition,
	wk *libapplication.ApplicationContext, id string,
) (interface{}, error) {
	return nil, nil
}

// Select will return the filtered records.
func (d *Driver) Select(
	model libpersistence.TableInterface, wk *libapplication.ApplicationContext,
	filters []*libpersistence.Filter, properties []string, options *libpersistence.OptionsListQuery,
) (interface{}, error) {
	/* span, _ := opentracing.StartSpanFromContext(wk.Workflow().Context, "postgres-"+model.GetName()+"-select")
	defer span.Finish()
	wk.Workflow().Context = opentracing.ContextWithSpan(wk.Workflow().Context, span) */

	filterQuery, args, err := buildFilters(wk, model, filters, options)
	if err != nil {
		return nil, errors.Wrap(err, "Couldn't build where query")
	}
	limitQuery := ""
	orderByQuery := ""
	if options != nil {
		if options.Limit != 0 {
			limitQuery = fmt.Sprintf("LIMIT %s", strconv.Itoa(int(options.Limit)))
		}
		if options.OrderBy != "" {
			// for _, o := range query.OrderBy {
			// 	if orderByQuery != "" {
			// 		orderByQuery = fmt.Sprintf("%s, ", orderByQuery)
			// 	}
			// 	orderByQuery = fmt.Sprintf("%s %s", orderByQuery, o.Field)
			// 	if o.Desc {
			// 		orderByQuery = fmt.Sprintf("%s %s", orderByQuery, "DESC")
			// 	}
			// }
			orderByQuery = fmt.Sprintf("ORDER BY %s", options.OrderBy)
		}
	}

	//nolint:gas
	q := fmt.Sprintf("SELECT %s from \"%s\" %s %s %s",
		strings.Join(properties, ","), model.Snake(), filterQuery, orderByQuery, limitQuery)
	/*span.LogFields(
		otlog.String("query", q),
		otlog.Object("args", args),
	)*/
	if wk.TransactionClient == d {
		rows, err := wk.Transaction.(*Tx).QueryRows(wk, q, args)
		if err != nil {
			return nil, errors.Wrap(err, "Couldn't get record")
		}
		return rows, nil
	} else {
		tx := &Tx{Sqlx: d.connection.MustBegin()}
		defer tx.Sqlx.Commit()

		rows, err := tx.QueryRows(wk, q, args)
		if err != nil {
			return nil, errors.Wrap(err, "Couldn't get record")
		}
		return rows, nil
	}
}

// Delete will delete the filtered records.
func (d *Driver) Delete(
	model libpersistence.TableInterface,
	wk *libapplication.ApplicationContext, filters []*libpersistence.Filter,
) error {
	/* span, _ := opentracing.StartSpanFromContext(wk.Workflow().Context, "postgres-"+model.GetName()+"-delete")
	defer span.Finish()
	wk.Workflow().Context = opentracing.ContextWithSpan(wk.Workflow().Context, span) */

	filterQuery, args, err := buildFilters(wk, model, filters, nil)
	if err != nil {
		return errors.Wrap(err, "Couldn't build where query")
	}

	q := fmt.Sprintf(
		"DELETE FROM \"%s\" %s", model.Snake(), filterQuery)
	/*span.LogFields(
		otlog.String("query", q),
		otlog.Object("args", args),
	)*/
	// nolint: gas
	err = wk.Transaction.(*Tx).Exec(wk, q, args)
	if err != nil {
		return errors.Wrap(err, "Couldn't delete record")
	}
	return nil
}

// func (d *Driver) CheckIDExist(
// 	model *libpersistence.TableDefinition,
// 	wk *libapplication.Workflow, id string,
// ) error {
// 	rows, err := d.Select(model, wk, &baserepositorypb.ListQuery{
// 		Filters: []*baserepositorypb.Filter{{Filter: &baserepositorypb.Filter_Condition{Condition: &baserepositorypb.Condition{
// 			Property: libpersistence.IDProperty,
// 			Operator: &baserepositorypb.Condition_IsEqualTo{IsEqualTo: &baserepositorypb.IsEqualTo{Value: id}},
// 		}}}},
// 	})
// 	if err != nil {
// 		return err
// 	}

// 	for rows.(*sqlx.Rows).Next() {
// 		return nil
// 	}

// 	return errors.New("This ID does not exist")

// }
