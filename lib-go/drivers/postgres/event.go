package postgres

import (

	// libuuid "github.com/google/uuid"

	"encoding/json"
	"fmt"
	"strings"

	"github.com/pkg/errors"
	"gitlab.com/empowerlab/stack/lib-go/libdomain/libapplication"
	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain"

	// "gitlab.com/empowerlab/stack/lib-go/libdata"
	"gitlab.com/empowerlab/stack/lib-go/libinfrastructure/libpersistence"
	"gitlab.com/empowerlab/stack/lib-go/libinfrastructure/libpersistence/properties"
)

// RegisterEvents([]*Event) (interface{}, error)
// 	MarkDispatchedEvent(*Event) (interface{}, error)

var EventDefinition = &libpersistence.TableDefinition{
	Name: "event",
	Properties: []libpersistence.Property{
		properties.Many2one( "tenantID",false , "", nil, libdomain.OnDeleteCascade, false, 3,  4, nil, nil),
		properties.Text( "aggregate", true, 3, 4, nil, nil),
		properties.Many2one( "aggregateID",true , "", nil, libdomain.OnDeleteSetNull, false, 3,  4, nil, nil),
		properties.Integer( "aggregateVersion", true,3, 4, nil, nil),
		properties.Text( "event", true, 3, 4, nil, nil),
		properties.Integer( "eventVersion",  true, 3, 4, nil, nil),
		properties.Text( "diff", true, 3, 4, nil, nil),
		properties.Text( "payload", true, 3, 4, nil, nil),
		properties.Boolean( "dispatched", true, 3, 4, nil, nil),
		properties.Datetime("occurredAt", true, 3, 4, nil, nil),
		properties.Many2one( "occurredBy",false , "", nil, libdomain.OnDeleteSetNull, false, 3,  4, nil, nil),
	},
}

// RegisterEvent will register the event in the event model.
func (d *Driver) RegisterEvents(
	wk *libapplication.ApplicationContext,
	a interface{},
	events []*libapplication.Event) error {
	// e := libdata.EventModel.Store.(libdata.EventStore).NewEvent(
	// 	aggregate, aggregateID, name, payload)
	// fmt.Printf("event %+v", e)

	
	values := []string{}
	args := map[string]interface{}{}
	i := 0
	for _, event := range events {

		aggregate := event.Aggregate
		data := event.Data

		fmt.Printf("diff %+v\n", data.Diff)
		diffJson, err := json.Marshal(data.Diff)
		if err != nil {
			return err
		}

		fmt.Printf("payload %+v\n", data.Payload)
		payloadJson, err := json.Marshal(data.Payload)
		if err != nil {
			return err
		}

		// tx := &Tx{
		// 	Sqlx: d.connection.MustBegin()}

		var tenantID interface{}
		if data.TenantID != "" {
			tenantID = data.TenantID
		}

		var occurredBy *string
		if data.OccurredBy != "" {
			occurredBy = &data.OccurredBy
		}

		values = append(values, fmt.Sprintf(
			"(:%d_id, :%d_tenant_id, :%d_aggregate, :%d_aggregate_id, :%d_event, :%d_event_version, :%d_diff, :%d_payload, :%d_aggregate_version, :%d_dispatched, :%d_occurred_at, :%d_occurred_by )",
			i, i, i, i, i, i, i, i, i, i, i, i,
		))
		args[fmt.Sprintf("%d_id", i)] = data.ID
		args[fmt.Sprintf("%d_tenant_id", i)] = tenantID
		args[fmt.Sprintf("%d_aggregate", i)] = aggregate.AggregateRoot.Name
		args[fmt.Sprintf("%d_aggregate_id", i)] = data.AggregateID
		args[fmt.Sprintf("%d_event", i)] = data.Event
		args[fmt.Sprintf("%d_event_version", i)] = data.EventVersion
		args[fmt.Sprintf("%d_diff", i)] = diffJson
		args[fmt.Sprintf("%d_payload", i)] = payloadJson
		args[fmt.Sprintf("%d_aggregate_version", i)] = data.AggregateVersion
		args[fmt.Sprintf("%d_dispatched", i)] = false
		args[fmt.Sprintf("%d_occurred_at", i)] = data.OccurredAt
		args[fmt.Sprintf("%d_occurred_by", i)] = occurredBy
		
		i++

		if i == 100 {

			q := fmt.Sprintf(
				"INSERT INTO event (id, tenant_id, aggregate, aggregate_id, event, event_version, diff, payload, aggregate_version, dispatched, occurred_at, occurred_by) VALUES %s",
				strings.Join(values, ","))
			// if a.Definition().UseTenants {
			// 	q = "INSERT INTO event (tenant_id, aggregate, aggregate_id, action, payload) VALUES (:tenant_id, :aggregate, :aggregate_id, :action, :payload) RETURNING id"
			// 	args["tenant_id"] = wk.Workflow().TenantID
			// }
			fmt.Println(q)
			rows, err := wk.Transaction.(*Tx).QueryRows(wk, q, args)
			// event := &libdata.Event{
			// 	Aggregate: aggregate,
			// }
			for rows.Next() {
				err = rows.Scan(
				// &event.ID,
				// &event.AggregateUUID,
				// &event.Name,
				// &payloadJson,
				)
				if err != nil {
					// tx.Sqlx.Rollback()
					return errors.Wrap(err, "Couldn't scan")
				}
			}
			if err != nil {
				// tx.Sqlx.Rollback()
				return errors.Wrap(err, "Couldn't insert")
			}

			values = []string{}
			args = map[string]interface{}{}
			i = 0

		}

		// err = json.Unmarshal(payloadJson, &event.Payload)
		// if err != nil {
		// 	// tx.Sqlx.Rollback()
		// 	return nil, errors.Wrap(err, "Couldn't insert")
		// }

		// tx.Sqlx.Commit()
	}

	if i > 0 {
		q := fmt.Sprintf(
			"INSERT INTO event (id, tenant_id, aggregate, aggregate_id, event, event_version, diff, payload, aggregate_version, dispatched, occurred_at, occurred_by) VALUES %s",
			strings.Join(values, ","))
		// if a.Definition().UseTenants {
		// 	q = "INSERT INTO event (tenant_id, aggregate, aggregate_id, action, payload) VALUES (:tenant_id, :aggregate, :aggregate_id, :action, :payload) RETURNING id"
		// 	args["tenant_id"] = wk.Workflow().TenantID
		// }
		fmt.Println(q)
		rows, err := wk.Transaction.(*Tx).QueryRows(wk, q, args)
		// event := &libdata.Event{
		// 	Aggregate: aggregate,
		// }
		for rows.Next() {
			err = rows.Scan(
			// &event.ID,
			// &event.AggregateUUID,
			// &event.Name,
			// &payloadJson,
			)
			if err != nil {
				// tx.Sqlx.Rollback()
				return errors.Wrap(err, "Couldn't scan")
			}
		}
		if err != nil {
			// tx.Sqlx.Rollback()
			return errors.Wrap(err, "Couldn't insert")
		}
	}

	return nil
	// return d.Insert(tx, libdata.EventModel, []interface{}{e})
}

func (d *Driver) MarkDispatchedEvent(
	wk *libapplication.ApplicationContext,
	event *libapplication.Event,
) error {

	tx := &Tx{
		Sqlx: d.connection.MustBegin()}

	q := "UPDATE event set dispatched = True WHERE id = :id"
	args := map[string]interface{}{
		"id": event.Data.ID,
		// "id":           libuuid.New().String(),
		// "aggregate":    event.Aggregate.Definition().AggregateRoot.Name,
		// "tenant_id":    wk.Workflow().TenantID,
		// "aggregate_id": event.AggregateID,
	}
	rows, err := tx.QueryRows(wk, q, args)
	for rows.Next() {
	}
	if err != nil {
		tx.Sqlx.Rollback()
		return err
	}
	tx.Sqlx.Commit()

	return nil
}

func (d *Driver) GetAggregateHistory(
	wk *libapplication.ApplicationContext,
	a *libdomain.AggregateDefinition, id string,
) ([]*libdomain.EventHistory, error) {

	// return nil, nil

	// // tx := &Tx{
	// // 	Sqlx: d.connection.MustBegin()}

	q := "SELECT id, aggregate_version, event, event_version, diff, payload, occurred_at, occurred_by FROM event WHERE aggregate = :aggregate AND tenant_id = :tenant_id AND aggregate_id = :id order by aggregate_version desc"
	args := map[string]interface{}{
		"aggregate": a.AggregateRoot.Name,
		"id":        id,
		"tenant_id": wk.Workflow().TenantID,
	}
	// if a.Definition().UseTenants {
	// 	q = q + " AND tenant_id = :tenant_id"
	// 	args["tenant_id"] = wk.Workflow().TenantID
	// }
	fmt.Println(q)
	rows, err := wk.Transaction.(*Tx).QueryRows(wk, q, args)
	// event := &libdata.Event{
	// 	Aggregate: aggregate,
	// }
	var events []*libdomain.EventHistory
	for rows.Next() {
		data := &libdomain.EventHistory{}
		// var payload string
		err = rows.Scan(
			&data.ID,
			&data.Version,
			&data.Event,
			&data.EventVersion,
			&data.Diff,
			&data.Payload,
			&data.OccurredAt,
			&data.OccurredBy,
		)
		if err != nil {
			// tx.Sqlx.Rollback()
			return nil, errors.Wrap(err, "Couldn't scan")
		}

		// err = json.Unmarshal([]byte(payload), &data.Payload)
		// if err != nil {
		// 	// tx.Sqlx.Rollback()
		// 	return nil, err
		// }

		events = append(events, data)
	}
	if err != nil {
		// tx.Sqlx.Rollback()
		return nil, errors.Wrap(err, "Couldn't insert")
	}

	fmt.Printf("events %+v\n", events)

	return events, nil
}

func (d *Driver) GetAggregateEvents(
	wk *libapplication.ApplicationContext,
	a *libdomain.AggregateDefinition, id string,
) ([]*libapplication.Event, error) {

	// return nil, nil

	// // tx := &Tx{
	// // 	Sqlx: d.connection.MustBegin()}

	q := "SELECT id, tenant_id, aggregate_id, event, payload FROM event WHERE aggregate = :aggregate AND tenant_id = :tenant_id AND aggregate_id = :id order by aggregate_version asc"
	args := map[string]interface{}{
		"aggregate": a.AggregateRoot.Name,
		"id":        id,
		"tenant_id": wk.Workflow().TenantID,
	}
	// if a.Definition().UseTenants {
	// 	q = q + " AND tenant_id = :tenant_id"
	// 	args["tenant_id"] = wk.Workflow().TenantID
	// }
	fmt.Println(q)
	rows, err := wk.Transaction.(*Tx).QueryRows(wk, q, args)
	// event := &libdata.Event{
	// 	Aggregate: aggregate,
	// }
	var events []*libapplication.Event
	for rows.Next() {
		data := &libapplication.EventData{}
		var payload string
		err = rows.Scan(
			&data.ID,
			&data.TenantID,
			&data.AggregateID,
			&data.Event,
			&payload,
			// &event.AggregateUUID,
			// &event.Name,
			// &payloadJson,
		)
		if err != nil {
			// tx.Sqlx.Rollback()
			return nil, errors.Wrap(err, "Couldn't scan")
		}

		err = json.Unmarshal([]byte(payload), &data.Payload)
		if err != nil {
			// tx.Sqlx.Rollback()
			return nil, err
		}

		event := &libapplication.Event{
			Aggregate: a,
			Data:      data,
		}

		events = append(events, event)
	}
	if err != nil {
		// tx.Sqlx.Rollback()
		return nil, errors.Wrap(err, "Couldn't insert")
	}

	return events, nil
}

// func (d *Driver) EventsOK(
// 	wk *libdata.Workflow, aggregate string, aggregateID string, name string, payload []byte,
// ) (libdata.EventInterface, error) {
// 	// e := libdata.EventModel.Store.(libdata.EventStore).NewEvent(
// 	// 	aggregate, aggregateID, name, payload)
// 	// fmt.Printf("event %+v", e)
// 	return nil, nil
// 	// return d.Insert(tx, libdata.EventModel, []interface{}{e})
// }

// func (d *Driver) EventsKO(
// 	wk *libdata.Workflow, aggregate string, aggregateID string, name string, payload []byte,
// ) (libdata.EventInterface, error) {
// 	// e := libdata.EventModel.Store.(libdata.EventStore).NewEvent(
// 	// 	aggregate, aggregateID, name, payload)
// 	// fmt.Printf("event %+v", e)
// 	return nil, nil
// 	// return d.Insert(tx, libdata.EventModel, []interface{}{e})
// }
