package postgres

import (
	"bytes"
	"fmt"
	"log"
	"strings"
	"text/template"

	// Let's import the pg packages here since we are obviously using the postgres driver
	"github.com/iancoleman/strcase"
	_ "github.com/jackc/pgx/stdlib"

	// Let's import the pg packages here since we are obviously using the postgres driver
	_ "github.com/lib/pq"

	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"

	"gitlab.com/empowerlab/stack/lib-go/libdomain/libapplication"
	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain"
	domainproperties "gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain/properties"

	// "gitlab.com/empowerlab/stack/lib-go/libdata"
	"gitlab.com/empowerlab/stack/lib-go/libinfrastructure/libpersistence"
	"gitlab.com/empowerlab/stack/lib-go/libinfrastructure/libpersistence/properties"
)

type ClientInfo struct {
	URL string
}

func NewClient(info *ClientInfo) (*Driver, error) {

	connection, err := sqlx.Connect("pgx", info.URL)
	if err != nil {
		fmt.Println("Couldn't connect with postgres TODO")
		return nil, errors.Wrap(err, "Couldn't connect with postgres "+info.URL)
	}

	return &Driver{
		connection: connection,
	}, nil
}

// Driver contains all the function needed to be recognized as a libdata driver
type Driver struct {
	connection    *sqlx.DB
	useEventStore bool
	InsertCustom  func(
		*Driver,
		libpersistence.TableInterface, *libapplication.ApplicationContext, []interface{},
		[]string, map[string]interface{},
		[]string,
	) (interface{}, error)
}

func (d *Driver) GetConnection() *sqlx.DB {
	return d.connection
}

func (d *Driver) SupportTransaction() bool {
	return true
}

// InitDB will connect to the postgres database and return the connection
// func (d *Driver) InitDB(
// 	writeInfo *libdata.ClusterInfo, readInfo *libdata.ClusterInfo,
// ) (*libdata.Cluster, error) {
// 	// writeCluster, err := sqlx.Connect(
// 	// 	"pgx", writeInfo.URL)
// 	// if err != nil {
// 	// 	fmt.Println("Couldn't connect with postgres TODO")
// 	// 	// return nil, errors.Wrap(err, "Couldn't connect with postgres")
// 	// }

// 	// var readCluster *sqlx.DB
// 	// if readInfo == writeInfo {
// 	// 	readCluster = writeCluster
// 	// } else {
// 	// 	readCluster, err = sqlx.Connect(
// 	// 		"pgx", readInfo.URL)
// 	// 	if err != nil {
// 	// 		fmt.Println("Couldn't connect with postgres TODO")
// 	// 		// return nil, errors.Wrap(err, "Couldn't connect with postgres")
// 	// 	}
// 	// }

// 	return nil, nil
// 	// return &libdata.Cluster{
// 	// 	Driver:       d,
// 	// 	WriteCluster: writeCluster,
// 	// 	ReadCluster:  readCluster}, nil
// }

// // CreateTenant will create the tenant in postgres, either a new schema or a new tenant record
// func (d *Driver) CreateTenant(wk *libdata.Workflow,
// 	models []*libdata.ModelDefinition, tenant *libdata.Tenant) error {

// 	// q := fmt.Sprintf("CREATE SCHEMA IF NOT EXISTS \"%s\"", tenant.Name)
// 	// if err := postgresDBExecute(d, q, map[string]interface{}{}); err != nil {
// 	// 	return errors.Wrap(err, "Couldn't create schema")
// 	// }

// 	// err := d.updateTenant(wk, models, tenant)
// 	// if err != nil {
// 	// 	return errors.Wrap(err, "Couldn't update tenant")
// 	// }

// 	// if cluster.MultiTenant {
// 	// 	// nolint:gas
// 	// 	q = fmt.Sprintf("INSERT INTO tenant (name) VALUES ('%s')", tenant.Name)
// 	// 	if err := postgresDBExecute(cluster, q, map[string]interface{}{}); err != nil {
// 	// 		return errors.Wrap(err, "Couldn't insert tenant")
// 	// 	}
// 	// }

// 	return nil
// }

// //nolint: gocyclo
// func (d *Driver) updateTenant(
// 	wk *libdata.Workflow, models []*libdata.ModelDefinition, tenant *libdata.Tenant,
// ) error {

func (d *Driver) GenerateSchema(
	tables []*libpersistence.TableDefinition,
) string {

	// fmt.Println(d)

	var dInterface libpersistence.DatasourceClient
	dInterface = d

	tables = append(tables, EventDefinition)

	// q := fmt.Sprintf("SET search_path TO \"%s\"", tenant.Name)
	// if err := postgresDBExecute(d, q, map[string]interface{}{}); err != nil {
	// 	return errors.Wrap(err, "Couldn't set search path")
	// }

	// db := cluster.WriteCluster.(*sqlx.DB)
	// driver, err := postgres.WithInstance(db.DB, &postgres.Config{})
	// if err != nil {
	// 	return errors.Wrap(err, "Couldn't get driver")
	// }

	// fmt.Printf("%+v", driver.config)
	// m, err := migrate.NewWithDatabaseInstance(
	// 	fmt.Sprintf("file://%spostgres", libdata.MigrationPath),
	// 	"postgres", driver)

	// if err != nil {
	// 	return errors.Wrap(err, "Couldn't get migration")
	// }
	// if err := m.Up(); err != nil {
	// 	switch errors.Cause(err).Error() {
	// 	case "no change":
	// 		return nil
	// 	default:
	// 		return errors.Wrap(err, "Couldn't execute migration")
	// 	}
	// }

	// credentials := fmt.Sprintf(
	// 	"host=%s user=%s dbname=%s password=%s",
	// 	cluster.WriteInfo.Host, // cluster.WriteInfo.Port,
	// 	cluster.WriteInfo.User, cluster.WriteInfo.Database, cluster.WriteInfo.Password)
	// fmt.Println("credentials ", credentials)
	// db, err := gorm.Open("postgres", credentials)
	// if err != nil {
	//   return errors.Wrap(err, "failed to connect database")
	// }
	// defer db.Close()

	// db.Exec(fmt.Sprintf("set search_path to %s", tenant.Name))

	schema := ""
	// for _, table := range tables {
	// 	// tables := aggregate.GetTables()

	// 	// for _, table := range tables {

	// 	if table.GetAbstract() {
	// 		continue
	// 	}

	// 	// if table.DisableDatabaseStore {
	// 	// 	continue
	// 	// }

	// 	q := fmt.Sprintf("CREATE TABLE IF NOT EXISTS \"%s\" ();", table.Snake())
	// 	if !table.GetDisableID() {
	// 		q = fmt.Sprintf(
	// 			"CREATE TABLE IF NOT EXISTS \"%s\" (id UUID PRIMARY KEY);", table.Snake())
	// 	}
	// 	// fmt.Println(q)
	// 	schema += q + "\n"
	// }

	
	schema += "schema \"public\" {\n"
	schema += "}\n"

	for _, table := range tables {
		// tables := aggregate.GetTables()
		// for _, table := range tables {

		if table.GetAbstract() {
			continue
		}

		schema += fmt.Sprintf("table \"%s\" {\n", table.Snake())
		schema += "  schema = schema.public\n"

		// fmt.Println("!!!!!!!!")
		// fmt.Println("!!!!!!!!")
		// fmt.Printf("%+v\n", table)

		// if entity.DisableDatabaseStore {
		// 	continue
		// }

		if table.UseTenants() {
			if true {
				schema += "  column \"tenant_id\" {\n"
				schema += "    type = uuid\n"
				schema += "  }\n"
			} else {
				q := fmt.Sprintf(
					"ALTER TABLE \"%s\" ADD COLUMN IF NOT EXISTS tenant_id UUID NOT NULL;",
					table.Snake())
				fmt.Println(q)
				// if err := postgresDBExecute(d, q, map[string]interface{}{}); err != nil {
				// 	return errors.Wrap(err, "Couldn't create model")
				// }
			}
		}
		if !table.GetDisableID() {
			schema += "  column \"id\" {\n"
			schema += "    type = uuid\n"
			schema += "  }\n"
		}

		var foreignKeys string
		for _, property := range table.StoredProperties() {

			// fmt.Printf("%+v\n", property.GetName())

			if property.Type() == properties.One2manyPropertyType {
				continue
			}

			if property.Type() == properties.Many2manyPropertyType {
				continue
			}

			if true {
				if property.GetExternalDatasource() != nil {
					continue
				}
			} else {
				if table.GetExternalDatasource() != nil && property.GetExternalDatasource() != nil && property.GetExternalDatasource() != dInterface {
					continue
				}
				if table.GetExternalDatasource() == nil && property.GetExternalDatasource() == nil {
					continue
				}
			}

			// if property.Type() == properties.SelectionPropertyType {
			// 	var values []string
			// 	for _, value := range property.(*properties.Selection).Options {
			// 		values = append(values, fmt.Sprintf("'%s'", value))
			// 	}
			// 	q := fmt.Sprintf("CREATE TYPE %s AS ENUM (%s);", table.Snake()+"_"+property.Snake()+"_enum", strings.Join(values, ","))
			// 	fmt.Println(q)
			// 	if err := postgresDBExecute(d, q, map[string]interface{}{}); err != nil {
			// 		return errors.Wrap(err, "Couldn't create model")
			// 	}
			// }

			propertyDefinition := property.Snake()
			var columnType string
			switch property.Type() {
			case properties.TextPropertyType:
				if property.IsTranslatable() {
					columnType = "jsonb"
				} else {
					columnType = "text"
				}
			case properties.SelectionPropertyType:
				columnType = "text"
				// columnType = table.Snake() + "_" + property.Snake() + "_enum"
			case properties.IntegerPropertyType:
				columnType = "integer"
			case properties.FloatPropertyType:
				columnType = "float"
			case properties.BooleanPropertyType:
				columnType = "boolean"
			case properties.DatePropertyType:
				columnType = "date"
			case properties.DatetimePropertyType:
				columnType = "timestamp"
			case properties.Many2onePropertyType:
				columnType = "uuid"
			case properties.JSONPropertyType:
				columnType = "jsonb"
			case properties.BinaryPropertyType:
				columnType = "bytea"
			}
			propertyDefinition = propertyDefinition + " " + columnType

			schema += fmt.Sprintf("  column \"%s\" {\n", property.Snake())
			schema += fmt.Sprintf("    type = %s\n", columnType)
			if !property.GetRequired() {
				schema += "    null = true\n"
			}
			schema += "  }\n"

			if property.Type() == properties.Many2onePropertyType && property.GetReference() != nil {
				foreignKeys += fmt.Sprintf("  foreign_key \"%s_fk\" {\n", property.Snake())
				foreignKeys += fmt.Sprintf("    columns = [column.%s]\n", property.Snake())
				foreignKeys += fmt.Sprintf("    ref_columns = [table.%s.column.id]\n", property.GetReference().Snake())
				
				// if !property.GetReference().GetDisableDatabaseStore() {
				// 	propertyDefinition = fmt.Sprintf(
				// 		"%s REFERENCES \"%s\"",
				// 		propertyDefinition, property.GetReference().Snake())
				// }

				if property.(*properties.Many2oneDefinition).OnDelete == libdomain.OnDeleteCascade {
					foreignKeys += "    on_delete = CASCADE\n"
				}
				if property.(*properties.Many2oneDefinition).OnDelete == libdomain.OnDeleteSetNull {
					foreignKeys += "    on_delete = SET_NULL\n"
				}
				foreignKeys += "  }\n"
			}

			// if property.GetPrimaryKey() {
			// 	propertyDefinition += " PRIMARY KEY"
			// } else
			// if property.GetRequired() {
			// 	propertyDefinition += " NOT NULL"
			// }

			// q := fmt.Sprintf("ALTER TABLE \"%s\" ADD COLUMN IF NOT EXISTS %s;", table.Snake(), propertyDefinition)
			// // fmt.Println(q)
			// schema += q + "\n"
			// if err := postgresDBExecute(d, q, map[string]interface{}{}); err != nil {
			// 	return errors.Wrap(err, "Couldn't create model")
			// }

			if property.Type() == properties.TextPropertyType && property.(*properties.TextDefinition).Translatable == domainproperties.TranslatableWYSIWYG {
				schema += fmt.Sprintf("  column \"%s_wysiwyg\" {\n", property.Snake())
				schema += fmt.Sprintf("    type = %s\n", "jsonb")
				schema += "    null = false\n"
				schema += "  }\n"
	
				// q := fmt.Sprintf("ALTER TABLE \"%s\" ADD COLUMN IF NOT EXISTS %s;", table.Snake(), property.Snake()+"_wysiwyg JSONB")
				// // fmt.Println(q)
				// schema += q + "\n"
				// // if err := postgresDBExecute(d, q, map[string]interface{}{}); err != nil {
				// // 	return errors.Wrap(err, "Couldn't create model")
				// // }
			}

		}

		if true && !table.GetDisableDatetime() {
			// q := fmt.Sprintf(
			// 	"ALTER TABLE \"%s\" ADD COLUMN IF NOT EXISTS created_at TIMESTAMP NOT NULL DEFAULT NOW();",
			// 	table.Snake())
			// fmt.Println(q)
			// if err := postgresDBExecute(d, q, map[string]interface{}{}); err != nil {
			// 	return errors.Wrap(err, "Couldn't create model")
			// }
			// q = fmt.Sprintf(
			// 	"ALTER TABLE \"%s\" ADD COLUMN IF NOT EXISTS updated_at TIMESTAMP NOT NULL DEFAULT NOW();",
			// 	table.Snake())
			// fmt.Println(q)
			// if err := postgresDBExecute(d, q, map[string]interface{}{}); err != nil {
			// 	return errors.Wrap(err, "Couldn't create model")
			// }
			// if table.SafeDelete {
			// 	q = fmt.Sprintf(
			// 		"ALTER TABLE \"%s\" ADD COLUMN IF NOT EXISTS deleted_at TIMESTAMP NOT NULL DEFAULT NOW();",
			// 		table.Snake())
			// 	fmt.Println(q)
			// 	if err := postgresDBExecute(d, q, map[string]interface{}{}); err != nil {
			// 		return errors.Wrap(err, "Couldn't create model")
			// 	}
			// }
			schema += "  column \"external_module\" {\n"
			schema += "    type = text\n"
			schema += "    null = true\n"
			schema += "  }\n"
			// q := fmt.Sprintf("ALTER TABLE \"%s\" ADD COLUMN IF NOT EXISTS external_module TEXT;", table.Snake())
			// // fmt.Println(q)
			// schema += q + "\n"
			// if err := postgresDBExecute(d, q, map[string]interface{}{}); err != nil {
			// 	return errors.Wrap(err, "Couldn't create model")
			// }
			schema += "  column \"external_id\" {\n"
			schema += "    type = text\n"
			schema += "    null = true\n"
			schema += "  }\n"
			// q = fmt.Sprintf("ALTER TABLE \"%s\" ADD COLUMN IF NOT EXISTS external_id TEXT;", table.Snake())
			// // fmt.Println(q)
			// schema += q + "\n"
			// if err := postgresDBExecute(d, q, map[string]interface{}{}); err != nil {
			// 	return errors.Wrap(err, "Couldn't create model")
			// }
		}

		schema += "  primary_key {\n"
		schema += "    columns = [\n"
		if table.GetDisableID() {
			// var keys []string
			for _, key := range table.GetKeys() {
				schema += fmt.Sprintf("    column.%s\n", key.Snake())
			}
			// q := fmt.Sprintf(
			// 	"ALTER TABLE \"%s\" ADD CONSTRAINT %s_pkey PRIMARY KEY (%s);",
			// 	table.Snake(), table.Snake(), strings.Join(keys, ","))
			// // fmt.Println(q)
			// schema += q + "\n"
			// if err := postgresDBExecute(d, q, map[string]interface{}{}); err != nil {
			// 	fmt.Println("Constraint already exist")
			// }
		} else {
			primaryKeys := []string{}
			for _, property := range table.Properties {
				if property.GetPrimaryKey() {
					primaryKeys = append(primaryKeys, property.Snake())
				}
			}
			if len(primaryKeys) > 0 {
				schema += "      column.aggregate_id,\n"
				for _, key := range primaryKeys {
					schema += fmt.Sprintf("      column.%s,\n", key)
				}
			} else {
				schema += "      column.id\n"
			}
		}
		schema += "    ]\n"
		schema += "  }\n"
		
		for _, constraint := range table.UniqueConstraints {
			schema += fmt.Sprintf("  unique \"%s\" {\n", constraint.Name)
			schema += "    columns = [\n"
			for _, column := range constraint.Columns {
				schema += fmt.Sprintf("      column.%s,\n", column)
			}
			schema += "	   ]\n"
			schema += "  }\n"
		}

		schema += foreignKeys
		schema += "}\n"


		for _, property := range table.NestedProperties() {

			if property.Type() == properties.Many2manyPropertyType {

				if true && property.GetExternalDatasource() != nil {
					continue
				}
				if false && property.GetExternalDatasource() != dInterface {
					continue
				}

				
				many2many := property.(*properties.Many2manyDefinition)
				onDelete := ""
				if many2many.OnDelete == libpersistence.OnDeleteCascade {
					onDelete = "    on_delete = CASCADE\n"
				}
				
				schema += fmt.Sprintf("table \"%s\" {\n", strcase.ToSnake(many2many.Relation))
				schema += "  schema = schema.public\n"

				// tenant := ""
				// tenant2 := ""
				if table.UseTenants() {
					schema += "  column \"tenant_id\" {\n"
					schema += "    type = uuid\n"
					schema += "  }\n"
					// tenant = "tenant_id UUID REFERENCES \"tenant\" ON DELETE CASCADE NOT NULL,"
					// tenant2 = "tenant_id,"
				}
				schema += fmt.Sprintf("  column \"%s\" {\n", many2many.InverseSnake())
				schema += "    type = uuid\n"
				schema += "  }\n"
				schema += fmt.Sprintf("  column \"%s\" {\n", many2many.TargetSnake())
				schema += "    type = uuid\n"
				schema += "  }\n"
				schema += "  primary_key {\n"
				schema += "    columns = [\n"
				if table.UseTenants() {
					schema += "      column.tenant_id,\n"
				}
				schema += fmt.Sprintf("      column.%s,\n", many2many.InverseSnake())
				schema += fmt.Sprintf("      column.%s,\n", many2many.TargetSnake())
				schema += "    ]\n"
				schema += "  }\n"
				if table.UseTenants() {
					schema += "  foreign_key \"tenant_fk\" {\n"
					schema += "    columns = [column.tenant_id]\n"
					schema += "    ref_columns = [table.tenant.column.id]\n"
					schema += "  }\n"
				}
				schema += fmt.Sprintf("  foreign_key \"%s_fk\" {\n", many2many.InverseSnake())
				schema += fmt.Sprintf("    columns = [column.%s]\n", many2many.InverseSnake())
				schema += fmt.Sprintf("    ref_columns = [table.%s.column.id]\n", table.Snake())
				schema += "    on_delete = CASCADE\n"
				schema += "  }\n"
				schema += fmt.Sprintf("  foreign_key \"%s_fk\" {\n", many2many.TargetSnake())
				schema += fmt.Sprintf("    columns = [column.%s]\n", many2many.TargetSnake())
				schema += fmt.Sprintf("    ref_columns = [table.%s.column.id]\n", many2many.GetReference().Snake())
				schema += onDelete
				schema += "  }\n"
				schema += "}\n"
				// if !property.GetReference().GetDisableDatabaseStore() {
				// 	propertyDefinition = fmt.Sprintf(
				// 		"%s REFERENCES \"%s\"",
				// 		propertyDefinition, property.GetReference().Snake())
				// }

				// if property.(*properties.Many2one).OnDelete == libpersistence.OnDeleteCascade {
				// 	schema += "    on_delete = CASCADE\n"
				// }
				// if property.(*properties.Many2one).OnDelete == libpersistence.OnDeleteSetNull {
				// 	schema += "    on_delete = SET NULL\n"
				// }
				// schema += "  }\n"
				// q := fmt.Sprintf(
				// 	"CREATE TABLE IF NOT EXISTS \"%s\" (%s%s UUID REFERENCES \"%s\" ON DELETE CASCADE NOT NULL, %s UUID REFERENCES \"%s\" %s NOT NULL, PRIMARY KEY (%s%s, %s));",
				// 	strcase.ToSnake(many2many.Relation), tenant, many2many.InverseSnake(), table.Snake(),
				// 	many2many.TargetSnake(), many2many.GetReference().Snake(), onDelete,
				// 	tenant2, many2many.InverseSnake(), many2many.TargetSnake())
				// // fmt.Println(q)
				// schema += q + "\n"
				// if err := postgresDBExecute(d, q, map[string]interface{}{
				// 	// "name": model.GetTemplateData().Snake,
				// }); err != nil {
				// 	return errors.Wrap(err, "Couldn't create model")
				// }
			}
		}

		schema += "\n"

		// }
	}

	return schema

}

// nolint: gocyclo
func (d *Driver) MigrateTables(
	tables map[*libpersistence.TableDefinition]*libpersistence.TableDefinition, mainDatasource bool,
) error {

	return nil

	fmt.Println(d)

	var dInterface libpersistence.DatasourceClient
	dInterface = d

	tables[EventDefinition] = EventDefinition

	// q := fmt.Sprintf("SET search_path TO \"%s\"", tenant.Name)
	// if err := postgresDBExecute(d, q, map[string]interface{}{}); err != nil {
	// 	return errors.Wrap(err, "Couldn't set search path")
	// }

	// db := cluster.WriteCluster.(*sqlx.DB)
	// driver, err := postgres.WithInstance(db.DB, &postgres.Config{})
	// if err != nil {
	// 	return errors.Wrap(err, "Couldn't get driver")
	// }

	// fmt.Printf("%+v", driver.config)
	// m, err := migrate.NewWithDatabaseInstance(
	// 	fmt.Sprintf("file://%spostgres", libdata.MigrationPath),
	// 	"postgres", driver)

	// if err != nil {
	// 	return errors.Wrap(err, "Couldn't get migration")
	// }
	// if err := m.Up(); err != nil {
	// 	switch errors.Cause(err).Error() {
	// 	case "no change":
	// 		return nil
	// 	default:
	// 		return errors.Wrap(err, "Couldn't execute migration")
	// 	}
	// }

	// credentials := fmt.Sprintf(
	// 	"host=%s user=%s dbname=%s password=%s",
	// 	cluster.WriteInfo.Host, // cluster.WriteInfo.Port,
	// 	cluster.WriteInfo.User, cluster.WriteInfo.Database, cluster.WriteInfo.Password)
	// fmt.Println("credentials ", credentials)
	// db, err := gorm.Open("postgres", credentials)
	// if err != nil {
	//   return errors.Wrap(err, "failed to connect database")
	// }
	// defer db.Close()

	// db.Exec(fmt.Sprintf("set search_path to %s", tenant.Name))

	for _, table := range tables {
		// tables := aggregate.GetTables()

		// for _, table := range tables {

		if table.GetAbstract() {
			continue
		}

		// if table.DisableDatabaseStore {
		// 	continue
		// }

		q := fmt.Sprintf("CREATE TABLE IF NOT EXISTS \"%s\" ();", table.Snake())
		if !table.GetDisableID() {
			q = fmt.Sprintf(
				"CREATE TABLE IF NOT EXISTS \"%s\" (id UUID PRIMARY KEY);", table.Snake())
		}
		fmt.Println(q)
		if err := postgresDBExecute(d, q, map[string]interface{}{
			// "name": model.GetTemplateData().Snake,
		}); err != nil {
			return errors.Wrap(err, "Couldn't create model")
		}
		// }
	}

	for _, table := range tables {
		// tables := aggregate.GetTables()
		// for _, table := range tables {

		if table.GetAbstract() {
			continue
		}

		fmt.Println("!!!!!!!!")
		fmt.Println("!!!!!!!!")
		fmt.Printf("%+v\n", table)

		// if entity.DisableDatabaseStore {
		// 	continue
		// }

		if table.UseTenants() {
			if mainDatasource {
				q := fmt.Sprintf(
					"ALTER TABLE \"%s\" ADD COLUMN IF NOT EXISTS tenant_id UUID REFERENCES \"tenant\" NOT NULL;",
					table.Snake())
				fmt.Println(q)
				if err := postgresDBExecute(d, q, map[string]interface{}{}); err != nil {
					return errors.Wrap(err, "Couldn't create model")
				}
			} else {
				q := fmt.Sprintf(
					"ALTER TABLE \"%s\" ADD COLUMN IF NOT EXISTS tenant_id UUID NOT NULL;",
					table.Snake())
				fmt.Println(q)
				if err := postgresDBExecute(d, q, map[string]interface{}{}); err != nil {
					return errors.Wrap(err, "Couldn't create model")
				}
			}
		}

		for _, property := range table.StoredProperties() {

			fmt.Printf("%+v\n", property.GetName())

			if property.Type() == properties.One2manyPropertyType {
				continue
			}

			if property.Type() == properties.Many2manyPropertyType {
				continue
			}

			if mainDatasource {
				if property.GetExternalDatasource() != nil {
					continue
				}
			} else {
				if table.GetExternalDatasource() != nil && property.GetExternalDatasource() != nil && property.GetExternalDatasource() != dInterface {
					continue
				}
				if table.GetExternalDatasource() == nil && property.GetExternalDatasource() == nil {
					continue
				}
			}

			// if property.Type() == properties.SelectionPropertyType {
			// 	var values []string
			// 	for _, value := range property.(*properties.Selection).Options {
			// 		values = append(values, fmt.Sprintf("'%s'", value))
			// 	}
			// 	q := fmt.Sprintf("CREATE TYPE %s AS ENUM (%s);", table.Snake()+"_"+property.Snake()+"_enum", strings.Join(values, ","))
			// 	fmt.Println(q)
			// 	if err := postgresDBExecute(d, q, map[string]interface{}{}); err != nil {
			// 		return errors.Wrap(err, "Couldn't create model")
			// 	}
			// }

			propertyDefinition := property.Snake()
			var columnType string
			switch property.Type() {
			case properties.TextPropertyType:
				if property.IsTranslatable() {
					columnType = "JSONB"
				} else {
					columnType = "TEXT"
				}
			case properties.SelectionPropertyType:
				columnType = "TEXT"
				// columnType = table.Snake() + "_" + property.Snake() + "_enum"
			case properties.IntegerPropertyType:
				columnType = "INTEGER"
			case properties.FloatPropertyType:
				columnType = "FLOAT"
			case properties.BooleanPropertyType:
				columnType = "BOOLEAN"
			case properties.DatePropertyType:
				columnType = "DATE"
			case properties.DatetimePropertyType:
				columnType = "TIMESTAMP"
			case properties.Many2onePropertyType:
				columnType = "UUID"
			case properties.JSONPropertyType:
				columnType = "JSONB"
			case properties.BinaryPropertyType:
				columnType = "BYTEA"
			}
			propertyDefinition = propertyDefinition + " " + columnType

			if property.Type() == properties.Many2onePropertyType && property.GetReference() != nil {
				if !property.GetReference().GetDisableDatabaseStore() {
					propertyDefinition = fmt.Sprintf(
						"%s REFERENCES \"%s\"",
						propertyDefinition, property.GetReference().Snake())
				}

				if property.(*properties.Many2oneDefinition).OnDelete == libdomain.OnDeleteCascade {
					propertyDefinition = fmt.Sprintf(
						"%s ON DELETE CASCADE", propertyDefinition)
				}
				if property.(*properties.Many2oneDefinition).OnDelete == libdomain.OnDeleteSetNull {
					propertyDefinition = fmt.Sprintf(
						"%s ON DELETE SET NULL", propertyDefinition)
				}
			}

			// if property.GetPrimaryKey() {
			// 	propertyDefinition += " PRIMARY KEY"
			// } else
			if property.GetRequired() {
				propertyDefinition += " NOT NULL"
			}

			q := fmt.Sprintf("ALTER TABLE \"%s\" ADD COLUMN IF NOT EXISTS %s;", table.Snake(), propertyDefinition)
			fmt.Println(q)
			if err := postgresDBExecute(d, q, map[string]interface{}{}); err != nil {
				return errors.Wrap(err, "Couldn't create model")
			}

			if property.Type() == properties.TextPropertyType && property.(*properties.TextDefinition).Translatable == domainproperties.TranslatableWYSIWYG {
				q := fmt.Sprintf("ALTER TABLE \"%s\" ADD COLUMN IF NOT EXISTS %s;", table.Snake(), property.Snake()+"_wysiwyg JSONB")
				fmt.Println(q)
				if err := postgresDBExecute(d, q, map[string]interface{}{}); err != nil {
					return errors.Wrap(err, "Couldn't create model")
				}
			}

		}

		if mainDatasource && !table.GetDisableDatetime() {
			// q := fmt.Sprintf(
			// 	"ALTER TABLE \"%s\" ADD COLUMN IF NOT EXISTS created_at TIMESTAMP NOT NULL DEFAULT NOW();",
			// 	table.Snake())
			// fmt.Println(q)
			// if err := postgresDBExecute(d, q, map[string]interface{}{}); err != nil {
			// 	return errors.Wrap(err, "Couldn't create model")
			// }
			// q = fmt.Sprintf(
			// 	"ALTER TABLE \"%s\" ADD COLUMN IF NOT EXISTS updated_at TIMESTAMP NOT NULL DEFAULT NOW();",
			// 	table.Snake())
			// fmt.Println(q)
			// if err := postgresDBExecute(d, q, map[string]interface{}{}); err != nil {
			// 	return errors.Wrap(err, "Couldn't create model")
			// }
			// if table.SafeDelete {
			// 	q = fmt.Sprintf(
			// 		"ALTER TABLE \"%s\" ADD COLUMN IF NOT EXISTS deleted_at TIMESTAMP NOT NULL DEFAULT NOW();",
			// 		table.Snake())
			// 	fmt.Println(q)
			// 	if err := postgresDBExecute(d, q, map[string]interface{}{}); err != nil {
			// 		return errors.Wrap(err, "Couldn't create model")
			// 	}
			// }
			q := fmt.Sprintf("ALTER TABLE \"%s\" ADD COLUMN IF NOT EXISTS external_module TEXT;", table.Snake())
			fmt.Println(q)
			if err := postgresDBExecute(d, q, map[string]interface{}{}); err != nil {
				return errors.Wrap(err, "Couldn't create model")
			}
			q = fmt.Sprintf("ALTER TABLE \"%s\" ADD COLUMN IF NOT EXISTS external_id TEXT;", table.Snake())
			fmt.Println(q)
			if err := postgresDBExecute(d, q, map[string]interface{}{}); err != nil {
				return errors.Wrap(err, "Couldn't create model")
			}
		}

		if table.GetDisableID() {
			var keys []string
			for _, key := range table.GetKeys() {
				keys = append(keys, key.Snake())
			}
			q := fmt.Sprintf(
				"ALTER TABLE \"%s\" ADD CONSTRAINT %s_pkey PRIMARY KEY (%s);",
				table.Snake(), table.Snake(), strings.Join(keys, ","))
			fmt.Println(q)
			if err := postgresDBExecute(d, q, map[string]interface{}{}); err != nil {
				fmt.Println("Constraint already exist")
			}
		}

		for _, property := range table.NestedProperties() {

			if property.Type() == properties.Many2manyPropertyType {

				if mainDatasource && property.GetExternalDatasource() != nil {
					continue
				}
				if !mainDatasource && property.GetExternalDatasource() != dInterface {
					continue
				}

				many2many := property.(*properties.Many2manyDefinition)
				onDelete := ""
				if many2many.OnDelete == libpersistence.OnDeleteCascade {
					onDelete = "ON DELETE CASCADE"
				}
				tenant := ""
				tenant2 := ""
				if table.UseTenants() {
					tenant = "tenant_id UUID REFERENCES \"tenant\" ON DELETE CASCADE NOT NULL,"
					tenant2 = "tenant_id,"
				}
				q := fmt.Sprintf(
					"CREATE TABLE IF NOT EXISTS \"%s\" (%s%s UUID REFERENCES \"%s\" ON DELETE CASCADE NOT NULL, %s UUID REFERENCES \"%s\" %s NOT NULL, PRIMARY KEY (%s%s, %s));",
					strcase.ToSnake(many2many.Relation), tenant, many2many.InverseSnake(), table.Snake(),
					many2many.TargetSnake(), many2many.GetReference().Snake(), onDelete,
					tenant2, many2many.InverseSnake(), many2many.TargetSnake())
				fmt.Println(q)
				if err := postgresDBExecute(d, q, map[string]interface{}{
					// "name": model.GetTemplateData().Snake,
				}); err != nil {
					return errors.Wrap(err, "Couldn't create model")
				}
			}
		}

		// }
	}

	return nil

}

//nolint: megacheck
// func (d *Driver) postgresDropSchema(cluster *libdata.Cluster, schema string) error {

// 	q := fmt.Sprintf("DROP SCHEMA \"%s\" CASCADE", schema)
// 	if err := postgresDBExecute(d, q, map[string]interface{}{}); err != nil {
// 		return errors.Wrap(err, "Couldn't drop schema")
// 	}
// 	return nil
// }

// BeginTransaction will create a new transaction and return it.
func (d *Driver) BeginTransaction(
	wf *libapplication.ApplicationContext, useCache bool,
) error {

	fmt.Println("BEGIN TRANSACTION")
	fmt.Println(d)

	writeTx := &Tx{
		Sqlx: d.connection.MustBegin()}
	// txs := []*Tx{writeTx}
	// var readTx *Tx
	// if c.WriteCluster == c.ReadCluster {
	// 	readTx = writeTx
	// } else {
	// 	readTx = &Tx{
	// 		Sqlx: d.connection.MustBegin(),
	// 	}
	// 	txs = append(txs, readTx)
	// }

	// for _, tx := range txs {
	// 	q := fmt.Sprintf("SET search_path TO \"%s\"", tenant.Name)
	// 	args := map[string]interface{}{
	// 		// "tenant": tenant.Name,
	// 	}
	// 	if err := tx.Exec(q, args); err != nil {
	// 		return nil, errors.Wrap(err, "Couldn't set search path")
	// 	}
	// }

	wf.Transaction = writeTx

	return nil

}

// RollbackTransaction will cancel all the operations in the transaction.
func (d *Driver) RollbackTransaction(wk *libapplication.ApplicationContext) error {
	err := wk.Transaction.(*Tx).Sqlx.Rollback()
	if err != nil {
		return errors.Wrap(err, "Couldn't rollback")
	}
	// if tx.WriteTx != tx.ReadTx {
	// 	err := tx.ReadTx.(*Tx).Sqlx.Rollback()
	// 	if err != nil {
	// 		return errors.Wrap(err, "Couldn't rollback")
	// 	}
	// }
	return nil
}

// CommitTransaction will definitely save all the operations in the transaction.
func (d *Driver) CommitTransaction(wk *libapplication.ApplicationContext) error {

	fmt.Println("commit")
	fmt.Println(wk)
	fmt.Println(wk.Transaction)
	err := wk.Transaction.(*Tx).Sqlx.Commit()
	if err != nil {
		fmt.Println("commit err ", err)
		return errors.Wrap(err, "Couldn't commit")
	}
	// if tx.WriteTx != tx.ReadTx {
	// 	err := tx.ReadTx.(*Tx).Sqlx.Commit()
	// 	if err != nil {
	// 		return errors.Wrap(err, "Couldn't commit")
	// 	}
	// }
	return nil
}

//GetTenant will return the tenant.
// func (d *Driver) GetTenant(
// 	wk *libdata.Workflow, tenant *libdata.Tenant,
// ) (*libdata.Tenant, error) {
// 	// if tx.Cluster.MultiTenant {
// 	// 	tenants, err := tx.postgresSelect(tenantModel, []*Filter{{
// 	// 		Field: "name", Operator: "=", Operande: tenant,
// 	// 	}}, nil, nil)
// 	// 	if err != nil {
// 	// 		return nil, errors.Wrap(err, "Couldn't get tenant")
// 	// 	}
// 	// 	tenant := &Tenant{}
// 	// 	for tenants.Next() {
// 	// 		err = tenants.StructScan(tenant)
// 	// 		if err != nil {
// 	// 			return nil, errors.Wrap(err, "Couldn't scan to tenants")
// 	// 		}
// 	// 	}
// 	// 	if tenant == nil {
// 	// 		return nil, errors.New("Specified tenant doesn't exist")
// 	// 	}
// 	// 	return tenant, nil
// 	// }

// 	return tenant, nil
// }

// DBUnrecognizedOperatorError is an error returned when you use a non existing operator in filter.
type DBUnrecognizedOperatorError struct {
	O string
}

// Error return the error message.
func (e *DBUnrecognizedOperatorError) Error() string {
	return fmt.Sprintf("Unable to recognize operator %s.", e.O)
}

// DBRequestError is an error returned when the database return an error.
type DBRequestError struct {
	Q string
	N string
	R string
}

// Error return the error message.
func (e *DBRequestError) Error() string {
	if len(e.N) > 0 {
		return fmt.Sprintf("Couldn't execute request\n%s\n%s\n%s", e.Q, e.N, e.R)
	}
	if len(e.N) > 0 {
		return fmt.Sprintf("Couldn't execute request\n%s\n%s", e.Q, e.R)
	}
	return fmt.Sprintf("Couldn't execute request\n%s", e.Q)
}

func postgresDBExecute(d *Driver, q string, args interface{}) error {

	fmt.Println(d)
	fmt.Println(d.connection)

	stmt, err := d.connection.PrepareNamed(q)
	if err != nil {
		return errors.Wrap(err, (&DBRequestError{Q: q, R: fmt.Sprintf("%s", args)}).Error())
	}
	_, err = stmt.Exec(args)
	if err != nil {
		return errors.Wrap(err, (&DBRequestError{Q: q, R: fmt.Sprintf("%s", args)}).Error())
	}

	log.Print(q)
	return nil
}


func (d *Driver) GetImportRepository() string {
	return `
	"github.com/jmoiron/sqlx"
	"database/sql"
	// timestamp "github.com/golang/protobuf/ptypes/timestamp"
	`
}

func (d *Driver) GetScanType() string {
	return "*sqlx.Rows"
}
func (d *Driver) GetDriverName() string {
	return "Postgres"
}

func (d *Driver) GenerateInsertBuildArgsFunc(datasourceClient libpersistence.DatasourceClient, table libpersistence.TableInterface) string {
	data := struct {
		Datasource libpersistence.DatasourceClient
		Table      libpersistence.TableInterface
	}{
		Datasource: datasourceClient,
		Table:      table,
	}

	buf := &bytes.Buffer{}
	err := insertBuildArgsTemplate.Execute(buf, data)
	if err != nil {
		fmt.Println("execute ", err)
	}
	// content, err := format.Source(buf.Bytes())
	// if err != nil {
	// 	fmt.Println(buf)
	// 	fmt.Println("model ", err)
	// }
	return buf.String()
}

// nolint:lll
var insertBuildArgsTemplate = template.Must(template.New("").Funcs(template.FuncMap{}).Parse(`
func (d *{{.Table.AggregateDefinition.AggregateRoot.Name}}Repository) {{.Table.Name}}PostgresInsertBuildArgs(
		records []interface{}) ([]string, map[string]interface{}) {


			{{if .Table.IsEntity}}
			var {{.Table.Name}}s []*pb.{{.Table.Title}}
			for _, record := range records {
				{{.Table.Name}}s = append({{.Table.Name}}s, record.(*pb.{{.Table.Title}}))
			}
			{{else}}
			var {{.Table.Name}}s []*pb.{{.Table.Title}}
			for _, record := range records {
				command := record.(*pb.Set{{.Table.Title}}Command)
				for _, object := range command.{{.Table.Title}}s {
					object.AggregateID = command.AggregateID
					{{.Table.Name}}s = append({{.Table.Name}}s, object)
				}
			}
			{{end}}
			
			args := map[string]interface{}{}
			for i, storage := range {{.Table.Name}}s {
		
				{{if eq .Table.GetDisableID false}}
					args[fmt.Sprintf("%v_id", i)] = storage.ID
				{{end}}
				{{if not .Table.IsEntity}}
				args[fmt.Sprintf("%v_aggregate_id", i)] = storage.AggregateID
				{{end}}
				{{- range .Table.StoredProperties }}
				{{if eq $.Datasource .GetExternalDatasource}}
				
				{{if eq .Type "Many2oneTypeTemp"}}
				{{.GetName}} := storage.{{.Title}}
				{{else}}
					{{ if and (eq .Type "TextType") .IsTranslatable}}
					{{.GetName}}, err := json.Marshal(storage.{{.Title}})
					if err != nil {
						panic(err)
					}
					{{else if eq .Type "DatetimeType"}}
					{{.GetName}} := libutils.ConvertTimestampFromProto(storage.{{.Title}})
					{{else if eq .Type "DateType"}}
					{{.GetName}} := libutils.ConvertTimestampFromProto(storage.{{.Title}})
					{{else if eq .Type "JSONType"}}
					{{.GetName}} := storage.{{.Title}}.AsMap()
					{{else}}
					{{.GetName}} := &storage.{{.Title}}
					{{if eq .GoType "string"}}
					if *{{.GetName}} == "" {
						{{.GetName}} = nil
					}
					{{end}}
					{{if eq .Type "Many2oneType"}}
					{{if not .IsRepeated}}
					if *{{.GetName}} == "" {
						{{.GetName}} = nil
					}
					{{end}}
					{{end}}
					{{end}}
				{{end}}
				args[fmt.Sprintf("%v_{{.Snake}}", i)] = {{.GetName}}
				{{end}}

				{{- if and (eq .Type "TextType") (eq .Translatable "TranslatableWYSIWYG")}}
				{{.GetName}}WYSIWYG := &storage.{{.Title}}WYSIWYG
				args[fmt.Sprintf("%v_{{.Snake}}_wysiwyg", i)] = {{.GetName}}WYSIWYG
				{{- end}}

				{{- end }}	
				{{if .Table.IsEntity}}
				{{if not .Datasource}}
				args[fmt.Sprintf("%v_external_module", i)] = storage.ExternalModule
				args[fmt.Sprintf("%v_external_id", i)] = storage.ExternalID	
				{{end}}
				{{end}}
			}
		
			return []string{
				{{if eq .Table.GetDisableID false}}
				"id",
				{{end}}
				{{- range .Table.StoredProperties }}
				{{if eq $.Datasource .GetExternalDatasource}}
				"{{.Snake}}",
				{{end}}
				{{- if and (eq .Type "TextType") (eq .Translatable "TranslatableWYSIWYG")}}
				"{{.Snake}}_wysiwyg",
				{{- end}}
				{{- end }}
				{{if .Table.IsEntity}}
				{{if not .Datasource}}
				"external_module",
				"external_id",
				{{end}}
				{{end}}
			}, args
}
`))

func (d *Driver) GenerateScanFunc(datasourceClient libpersistence.DatasourceClient, prefix string, table libpersistence.TableInterface) string {
	data := struct {
		Datasource libpersistence.DatasourceClient
		Prefix     string
		Table      libpersistence.TableInterface
	}{
		Datasource: datasourceClient,
		Prefix:     prefix,
		Table:      table,
	}

	buf := &bytes.Buffer{}
	err := scanTemplate.Execute(buf, data)
	if err != nil {
		fmt.Println("execute ", err)
	}
	// content, err := format.Source(buf.Bytes())
	// if err != nil {
	// 	fmt.Println(buf)
	// 	fmt.Println("model ", err)
	// }
	return buf.String()
}

// nolint:lll
var scanTemplate = template.Must(template.New("").Funcs(template.FuncMap{}).Parse(`
func (d *{{.Table.AggregateDefinition.AggregateRoot.Name}}Repository) {{.Table.Name}}{{.Prefix}}Scan(
	rows interface{}, collection interface{}, wk libdomain.ApplicationContext, properties []string,
) ([]*pb.{{.Table.Title}}, error) {
	var records []*pb.{{.Table.Title}}
	for rows.(*sqlx.Rows).Next() {

		fmt.Println("properties ", properties)

		var scanProperties []interface{}
		for _, property := range properties {
			switch property {
			case "id":
				id := ""
				scanProperties = append(scanProperties, &id)
			{{if .Table.AggregateDefinition.UseTenants}}
			case "tenant_id":
				tenantID := ""
				scanProperties = append(scanProperties, &tenantID)
			{{end}}
			{{- range .Table.StoredProperties }}
			{{if not .IsRepeated}}
			case "{{.Snake}}":
				{{if eq .Type "DatetimeType"}}
				scanProperties = append(scanProperties, &time.Time{})
				{{else}}
				{{if eq .Type "DateType"}}
				scanProperties = append(scanProperties, &time.Time{})
				{{else}}
				scanProperties = append(scanProperties, &{{.DBType.Type}}{})
				{{end}}
				{{end}}
			{{else}}
			{{end}}
			{{- if and (eq .Type "TextType") (eq .Translatable "TranslatableWYSIWYG")}}
			case "{{.Snake}}_wysiwyg":
				scanProperties = append(scanProperties, &sql.NullString{})
			{{- end}}
			{{- end}}
			{{if $.Table.IsEntity}}
			case "external_module":
				scanProperties = append(scanProperties, &sql.NullString{})
			case "external_id":
				scanProperties = append(scanProperties, &sql.NullString{})
			{{end}}
			}
		}

		for _, property := range scanProperties {
			fmt.Printf("%T\n", property)
		}

		err := rows.(*sqlx.Rows).Scan(scanProperties...)
		if err != nil {
			fmt.Println(err)
			return nil, errors.Wrap(err, "Couldn't scan to templates")
		}

		var {{.Table.Name}} *pb.{{.Table.Title}}
		if collection == nil {
			{{.Table.Name}} = &pb.{{.Table.Title}}{
				OutOfCache: &pb.{{.Table.Title}}OutOfCache{},
			}
		} else {
			id := *scanProperties[0].(*string)
			{{.Table.Name}} = collection.(*{{.Table.Title}}Collection).GetByID({{.Table.Title}}ID(id)).Pb()
		}

		for i, property := range properties {
			switch property {
			case "id":
				{{.Table.Name}}.ID =  *scanProperties[i].(*string)
			{{if .Table.AggregateDefinition.UseTenants}}
			case "tenant_id":
				{{if .Table.AggregateDefinition.UseTenants}}{{.Table.Name}}.TenantID = *scanProperties[i].(*string){{end}}
			{{end}}
			{{- range .Table.StoredProperties }}
			{{if not .IsRepeated}}
			case "{{.Snake}}":
				{{ if and (eq .Type "TextType") .IsTranslatable}}
				var {{.GetName}} map[string]string
				err := json.Unmarshal([]byte(scanProperties[i].(*sql.NullString).String), &{{.GetName}})
				if err != nil {
					return nil, errors.Wrap(err, "Couldn't unmarshal translations")
				}
				for k, v := range {{.GetName}} {
					if v == "" {
						delete({{.GetName}}, k)
					}
				}
				{{$.Table.Name}}.{{.Title}} = {{.GetName}}
				{{else if eq .Type "DatetimeType"}}
				{{.GetName}} := scanProperties[i].(*time.Time)
				{{$.Table.Name}}.{{.Title}} = libutils.ConvertTimestampToProto(*{{.GetName}})
				{{else}}
				{{if eq .Type "DateType"}}
				{{.GetName}} := scanProperties[i].(*time.Time)
				{{$.Table.Name}}.{{.Title}} = libutils.ConvertTimestampToProto(*{{.GetName}})
				{{else if eq .Type "JSONType"}}
				var {{.GetName}} map[string]interface{}
				err := json.Unmarshal([]byte(scanProperties[i].(*sql.NullString).String), &{{.GetName}})
				if err != nil {
					return nil, errors.Wrap(err, "Couldn't unmarshal translations")
				}
				{{.GetName}}Struct, err := structpb.NewStruct({{.GetName}})
				if err != nil {
					return nil, errors.Wrap(err, "Couldn't unmarshal json")
				}
				{{$.Table.Name}}.{{.Title}} = {{.GetName}}Struct
				{{else}}
					{{.GetName}} := scanProperties[i].(*{{.DBType.Type}})
					{{if eq .GoType "int"}}
					{{$.Table.Name}}.{{.Title}} = int64({{.Name}}.{{.DBType.Value}})
					{{else}}
						{{if eq .GoType "float32"}}
						{{$.Table.Name}}.{{.Title}} = float32({{.Name}}.{{.DBType.Value}})
						{{else}}
							{{if eq .GoType "[]byte"}}
								{{$.Table.Name}}.{{.Title}} = []byte({{.Name}}.{{.DBType.Value}})
							{{else}}
								{{if eq .Type "Many2oneType"}}
									// {{$.Table.Name}}.{{.Title}} = &pb.{{.TitleWithoutID}}{ID: {{.GetName}}.{{.DBType.Value}} }
									{{$.Table.Name}}.{{.Title}} = {{.GetName}}.{{.DBType.Value}} 
								{{else}}
									{{if eq .GoType "[]string"}}
										{{$.Table.Name}}.{{.Title}} = []string{}
									{{else}}
										{{$.Table.Name}}.{{.Title}} = {{.GetName}}.{{.DBType.Value}}
									{{end}}
								{{end}}
							{{end}}
						{{end}}
					{{end}}
				{{end}}
				{{end}}
			{{end}}
			{{- if and (eq .Type "TextType") (eq .Translatable "TranslatableWYSIWYG")}}
			case "{{.Snake}}_wysiwyg":
				var {{.GetName}}WYSIWYG map[string]interface{}
				err := json.Unmarshal([]byte(scanProperties[i].(*sql.NullString).String), &{{.GetName}}WYSIWYG)
				if err != nil {
					return nil, errors.Wrap(err, "Couldn't unmarshal translations")
				}
				{{.GetName}}TranslatedTermsProto := map[string]*baserepositorypb.WYSIWYG_TranslatedTerm{}
				for term, translations := range {{.GetName}}WYSIWYG["translatedTerms"].(map[string]interface{}) {
					translationsProto := map[string]string{}
					for lang, translation := range translations.(map[string]interface{}) {
						translationsProto[lang] = translation.(string)
					}
					{{.GetName}}TranslatedTermsProto[term] = &baserepositorypb.WYSIWYG_TranslatedTerm{Translations: translationsProto}
				}
				var {{.GetName}}AssetsProto []*baserepositorypb.WYSIWYG_Asset
				for _, record := range {{.GetName}}WYSIWYG["assets"].([]interface{}) {
					asset := record.(map[string]interface{})
					{{.GetName}}AssetsProto = append({{.GetName}}AssetsProto, &baserepositorypb.WYSIWYG_Asset{
						ID: asset["id"].(string),
						Filename: asset["filename"].(string),
						ContentType: asset["contentType"].(string),
						Size: int64(asset["size"].(float64)),
						URL: asset["url"].(string),
					})
				}
				{{$.Table.Name}}.{{.Title}}WYSIWYG = &baserepositorypb.WYSIWYG{
					TranslatedTerms: {{.GetName}}TranslatedTermsProto,
					TranslatedTermsLoaded: true,
					Assets: {{.GetName}}AssetsProto,
					AssetsLoaded: true,
				}
			{{- end}}
			{{- else}}
			{{- end}}
			{{if $.Table.IsEntity}}
			case "external_module":
				externalModule := scanProperties[i].(*sql.NullString)
				{{.Table.Name}}.ExternalModule = externalModule.String
			case "external_id":
				externalID := scanProperties[i].(*sql.NullString)
				{{.Table.Name}}.ExternalID = externalID.String
			{{end}}
			}
		}

		records = append(records, {{.Table.Name}})
		fmt.Printf("record %+v\n", {{.Table.Name}})

	}
	if err := rows.(*sqlx.Rows).Err(); err != nil {
		panic(fmt.Sprintf("Error during postgresql request %s", err))
	}
	return records, nil
}
`))
