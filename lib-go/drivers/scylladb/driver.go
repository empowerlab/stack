package scylladb

import (
	"bytes"
	"context"
	"fmt"
	"html/template"
	"strings"
	"time"

	"github.com/gocql/gocql"
	"github.com/iancoleman/strcase"
	"gitlab.com/empowerlab/stack/lib-go/libdomain/libapplication"
	"gitlab.com/empowerlab/stack/lib-go/libinfrastructure/libpersistence"
	"gitlab.com/empowerlab/stack/lib-go/libinfrastructure/libpersistence/properties"
	// "gitlab.com/empowerlab/stack/lib-go/libdata"
)

type ClientInfo struct {
	Addr     string
	Keyspace string
}

func NewClient(info *ClientInfo) (*Client, error) {

	cluster := gocql.NewCluster(info.Addr)
	cluster.Keyspace = info.Keyspace
	cluster.Consistency = gocql.Quorum
	cluster.Timeout = 60 * time.Second
	cluster.ConnectTimeout = 60 * time.Second
	// connect to the cluster
	session, err := cluster.CreateSession()
	if err != nil {
		return nil, err
	}

	return &Client{
		session:  session,
		keyspace: info.Keyspace,
	}, nil
}

// Driver contains all the function needed to be recognized as a libdata driver
type Client struct {
	session  *gocql.Session
	keyspace string
}

func (d *Client) GetSession() *gocql.Session {
	return d.session
}


func (d *Client) GenerateSchema(
	tables []*libpersistence.TableDefinition,
) string {
	return ""
}

// nolint: gocyclo
func (d *Client) MigrateTables(
	tables map[*libpersistence.TableDefinition]*libpersistence.TableDefinition, mainDatasource bool,
) error {

	fmt.Println(d)

	var dInterface libpersistence.DatasourceClient
	dInterface = d

	ctx := context.Background()

	// q := fmt.Sprintf("SET search_path TO \"%s\"", tenant.Name)
	// if err := postgresDBExecute(d, q, map[string]interface{}{}); err != nil {
	// 	return errors.Wrap(err, "Couldn't set search path")
	// }

	// db := cluster.WriteCluster.(*sqlx.DB)
	// driver, err := postgres.WithInstance(db.DB, &postgres.Config{})
	// if err != nil {
	// 	return errors.Wrap(err, "Couldn't get driver")
	// }

	// fmt.Printf("%+v", driver.config)
	// m, err := migrate.NewWithDatabaseInstance(
	// 	fmt.Sprintf("file://%spostgres", libdata.MigrationPath),
	// 	"postgres", driver)

	// if err != nil {
	// 	return errors.Wrap(err, "Couldn't get migration")
	// }
	// if err := m.Up(); err != nil {
	// 	switch errors.Cause(err).Error() {
	// 	case "no change":
	// 		return nil
	// 	default:
	// 		return errors.Wrap(err, "Couldn't execute migration")
	// 	}
	// }

	// credentials := fmt.Sprintf(
	// 	"host=%s user=%s dbname=%s password=%s",
	// 	cluster.WriteInfo.Host, // cluster.WriteInfo.Port,
	// 	cluster.WriteInfo.User, cluster.WriteInfo.Database, cluster.WriteInfo.Password)
	// fmt.Println("credentials ", credentials)
	// db, err := gorm.Open("postgres", credentials)
	// if err != nil {
	//   return errors.Wrap(err, "failed to connect database")
	// }
	// defer db.Close()

	// db.Exec(fmt.Sprintf("set search_path to %s", tenant.Name))

	for _, table := range tables {
		// tables := aggregate.GetTables()

		// for _, table := range tables {

		if table.GetAbstract() {
			continue
		}

		// if table.DisableDatabaseStore {
		// 	continue
		// }

		var primaryKeys []string
		var primaryNames []string
		if table.UseTenants() {
			primaryKeys = append(primaryKeys, "tenant_id UUID")
			primaryNames = append(primaryNames, "tenant_id")
		}

		if table.ExternalDatasource != nil && len(table.Keys) > 0 {

			for _, field := range table.Keys {

				var columnType string
				switch field.Type() {
				case properties.TextPropertyType:
					columnType = "TEXT"
				case properties.IntegerPropertyType:
					columnType = "INT"
				case properties.FloatPropertyType:
					columnType = "DOUBLE"
				case properties.BooleanPropertyType:
					columnType = "BOOLEAN"
				case properties.DatePropertyType:
					columnType = "DATE"
				case properties.DatetimePropertyType:
					columnType = "TIMESTAMP"
				case properties.Many2onePropertyType:
					columnType = "UUID"
				case properties.JSONPropertyType:
					columnType = "JSON"
				case properties.BinaryPropertyType:
					columnType = "BYTEA"
				}

				primaryKeys = append(primaryKeys, field.Snake()+" "+columnType)
				primaryNames = append(primaryNames, field.Snake())
			}

		} else {
			primaryKeys = append(primaryKeys, "id UUID")
			primaryNames = append(primaryNames, "id")
		}
		primaryKeyDefinition := strings.Join(primaryNames, ",")
		if table.GetPrimaryKey() != "" {
			primaryKeyDefinition = table.GetPrimaryKey()
		}
		primaryKeys = append(primaryKeys, "PRIMARY KEY ("+primaryKeyDefinition+")")
		primaryKey := strings.Join(primaryKeys, ",")

		q := fmt.Sprintf(
			"CREATE TABLE IF NOT EXISTS \"%s\" (%s);", table.Snake(), primaryKey)
		fmt.Println(q)
		scanner := d.DBExecute(ctx, q, []interface{}{})
		if err := scanner.Err(); err != nil {
			return err
		}
		// }
	}

	for _, table := range tables {
		// tables := aggregate.GetTables()
		// for _, table := range tables {

		if table.GetAbstract() {
			continue
		}

		// if entity.DisableDatabaseStore {
		// 	continue
		// }

		if table.UseTenants() {

			exist := false
			q := `select * from system_schema.columns where keyspace_name = ? and table_name = ? and column_name = ?`
			scanner := d.DBExecute(ctx, q, []interface{}{
				d.keyspace, table.Snake(), "tenant_id",
			})
			for scanner.Next() {
				exist = true
			}
			// scanner.Err() closes the iterator, so scanner nor iter should be used afterwards.
			if err := scanner.Err(); err != nil {
				return err
			}

			if exist {
				// q := fmt.Sprintf(
				// 	"ALTER TABLE \"%s\" ALTER  tenant_id uuid;",
				// 	table.Snake())
				// fmt.Println(q)
				// scanner = scyllaDBExecute(ctx, d, q, []interface{}{})
				// if err := scanner.Err(); err != nil {
				// 	return err
				// }
			} else {
				q := fmt.Sprintf(
					"ALTER TABLE \"%s\" ADD  tenant_id uuid;",
					table.Snake())
				fmt.Println(q)
				scanner := d.DBExecute(ctx, q, []interface{}{})
				if err := scanner.Err(); err != nil {
					return err
				}

			}
		}

		for _, property := range table.StoredProperties() {

			// fmt.Printf("%+v\n", property.GetName())

			if property.Type() == properties.One2manyPropertyType {
				continue
			}

			if property.Type() == properties.Many2manyPropertyType {
				continue
			}

			if mainDatasource {
				if property.GetExternalDatasource() != nil {
					continue
				}
			} else {
				if table.GetExternalDatasource() != nil && property.GetExternalDatasource() != nil && property.GetExternalDatasource() != dInterface {
					continue
				}
				if table.GetExternalDatasource() == nil && property.GetExternalDatasource() == nil {
					continue
				}
			}

			propertyDefinition := property.Snake()

			exist := false
			q := `select * from system_schema.columns where keyspace_name = ? and table_name = ? and column_name = ?`
			scanner := d.DBExecute(ctx, q, []interface{}{
				d.keyspace, table.Snake(), property.Snake(),
			})
			for scanner.Next() {
				exist = true
			}
			// scanner.Err() closes the iterator, so scanner nor iter should be used afterwards.
			if err := scanner.Err(); err != nil {
				return err
			}
			if exist {
				continue
			}

			var columnType string
			switch property.Type() {
			case properties.TextPropertyType:
				columnType = "TEXT"
			case properties.IntegerPropertyType:
				columnType = "INT"
			case properties.FloatPropertyType:
				columnType = "DOUBLE"
			case properties.BooleanPropertyType:
				columnType = "BOOLEAN"
			case properties.DatePropertyType:
				columnType = "DATE"
			case properties.DatetimePropertyType:
				columnType = "TIMESTAMP"
			case properties.Many2onePropertyType:
				columnType = "UUID"
			case properties.JSONPropertyType:
				columnType = "JSON"
			case properties.BinaryPropertyType:
				columnType = "BYTEA"
			}
			propertyDefinition = propertyDefinition + " " + columnType

			// if property.Type() == properties.Many2onePropertyType && property.GetReference() != nil {
			// 	if !property.GetReference().GetDisableDatabaseStore() {
			// 		propertyDefinition = fmt.Sprintf(
			// 			"%s REFERENCES \"%s\"",
			// 			propertyDefinition, property.GetReference().Snake())
			// 	}

			// 	if property.(*properties.Many2one).OnDelete == libpersistence.OnDeleteCascade {
			// 		propertyDefinition = fmt.Sprintf(
			// 			"%s ON DELETE CASCADE", propertyDefinition)
			// 	}
			// 	if property.(*properties.Many2one).OnDelete == libpersistence.OnDeleteSetNull {
			// 		propertyDefinition = fmt.Sprintf(
			// 			"%s ON DELETE SET NULL", propertyDefinition)
			// 	}
			// }

			if property.GetPrimaryKey() {
				propertyDefinition += " PRIMARY KEY"
			}
			// else
			// if property.GetRequired() {
			// 	propertyDefinition += " NOT NULL"
			// }

			q = fmt.Sprintf("ALTER TABLE \"%s\" ADD %s;", table.Snake(), propertyDefinition)
			fmt.Println(q)
			scanner = d.DBExecute(ctx, q, []interface{}{})
			if err := scanner.Err(); err != nil {
				return err
			}

			if property.IsTranslatable() {
				q := fmt.Sprintf("ALTER TABLE \"%s\" ADD COLUMN IF NOT EXISTS %s;", table.Snake(), property.Snake()+"_translations JSONB")
				fmt.Println(q)
				scanner := d.DBExecute(ctx, q, []interface{}{})
				if err := scanner.Err(); err != nil {
					return err
				}
			}

			if property.IsIndexed() {
				q := fmt.Sprintf("CREATE INDEX IF NOT EXISTS %s ON \"%s\" (%s);", property.Snake()+"_"+table.Snake()+"_index", table.Snake(), property.Snake())
				fmt.Println(q)
				scanner := d.DBExecute(ctx, q, []interface{}{})
				if err := scanner.Err(); err != nil {
					return err
				}
			}

		}

		if mainDatasource && !table.GetDisableDatetime() {
			q := fmt.Sprintf(
				"ALTER TABLE \"%s\" ADD COLUMN IF NOT EXISTS created_at TIMESTAMP NOT NULL DEFAULT NOW();",
				table.Snake())
			fmt.Println(q)
			scanner := d.DBExecute(ctx, q, []interface{}{})
			if err := scanner.Err(); err != nil {
				return err
			}
			q = fmt.Sprintf(
				"ALTER TABLE \"%s\" ADD COLUMN IF NOT EXISTS updated_at TIMESTAMP NOT NULL DEFAULT NOW();",
				table.Snake())
			fmt.Println(q)
			scanner = d.DBExecute(ctx, q, []interface{}{})
			if err := scanner.Err(); err != nil {
				return err
			}
			// if table.SafeDelete {
			// 	q = fmt.Sprintf(
			// 		"ALTER TABLE \"%s\" ADD COLUMN IF NOT EXISTS deleted_at TIMESTAMP NOT NULL DEFAULT NOW();",
			// 		table.Snake())
			// 	fmt.Println(q)
			// 	if err := postgresDBExecute(d, q, map[string]interface{}{}); err != nil {
			// 		return errors.Wrap(err, "Couldn't create model")
			// 	}
			// }
			q = fmt.Sprintf("ALTER TABLE \"%s\" ADD COLUMN IF NOT EXISTS external_module TEXT;", table.Snake())
			fmt.Println(q)
			scanner = d.DBExecute(ctx, q, []interface{}{})
			if err := scanner.Err(); err != nil {
				return err
			}
			q = fmt.Sprintf("ALTER TABLE \"%s\" ADD COLUMN IF NOT EXISTS external_id TEXT;", table.Snake())
			fmt.Println(q)
			scanner = d.DBExecute(ctx, q, []interface{}{})
			if err := scanner.Err(); err != nil {
				return err
			}
		}

		// if  table.GetDisableID() {
		// 	var keys []string
		// 	for _, key := range table.GetKeys() {
		// 		keys = append(keys, key.Snake())
		// 	}
		// 	q := fmt.Sprintf(
		// 		"ALTER TABLE \"%s\" ADD CONSTRAINT %s_pkey PRIMARY KEY (%s);",
		// 		table.Snake(), table.Snake(), strings.Join(keys, ","))
		// 	fmt.Println(q)
		// 	scanner := scyllaDBExecute(ctx, d, q, []interface{}{})
		// 	if err := scanner.Err(); err != nil {
		// 		return err
		// 	}
		// }

		for _, property := range table.NestedProperties() {

			if property.Type() == properties.Many2manyPropertyType {

				if mainDatasource && property.GetExternalDatasource() != nil {
					continue
				}
				if !mainDatasource && property.GetExternalDatasource() != dInterface {
					continue
				}

				many2many := property.(*properties.Many2manyDefinition)
				onDelete := ""
				if many2many.OnDelete == libpersistence.OnDeleteCascade {
					onDelete = "ON DELETE CASCADE"
				}
				q := fmt.Sprintf(
					"CREATE TABLE IF NOT EXISTS \"%s\" (%s UUID REFERENCE \"%s\" ON DELETE CASCADE, %s UUID REFERENCE \"%s\" %s, PRIMARY KEY (%s, %s));",
					strcase.ToSnake(many2many.Relation), many2many.InverseSnake(), table.Snake(),
					many2many.TargetSnake(), many2many.GetReference().Snake(), onDelete,
					many2many.InverseSnake(), many2many.TargetSnake())
				fmt.Println(q)
				scanner := d.DBExecute(ctx, q, []interface{}{})
				if err := scanner.Err(); err != nil {
					return err
				}
			}
		}

		// for _, index := range table.GetIndexes() {
		// 	if index.DatasourceClient == d {
		// 		q := fmt.Sprintf("CREATE INDEX IF NOT EXISTS %s ON \"%s\" (%s);", index.Name, table.Snake(), strings.Join(index.Properties, ","))
		// 		fmt.Println(q)
		// 		scanner := d.DBExecute(ctx, q, []interface{}{})
		// 		if err := scanner.Err(); err != nil {
		// 			return err
		// 		}
		// 	}
		// }

		// }
	}

	return nil

}

func (d *Client) GetImportRepository() string {
	return `
	"github.com/gocql/gocql"
	`
}

func (d *Client) GetScanType() string {
	return "gocql.Scanner"
}
func (d *Client) GetDriverName() string {
	return "ScyllaDB"
}

// BeginTransaction will create a new transaction and return it.
func (d *Client) BeginTransaction(
	wf *libapplication.ApplicationContext, useCache bool,
) error {

	fmt.Println("BEGIN TRANSACTION")
	fmt.Println(d)

	// writeTx := &Tx{
	// 	Sqlx: d.connection.MustBegin()}
	// txs := []*Tx{writeTx}
	// var readTx *Tx
	// if c.WriteCluster == c.ReadCluster {
	// 	readTx = writeTx
	// } else {
	// 	readTx = &Tx{
	// 		Sqlx: d.connection.MustBegin(),
	// 	}
	// 	txs = append(txs, readTx)
	// }

	// for _, tx := range txs {
	// 	q := fmt.Sprintf("SET search_path TO \"%s\"", tenant.Name)
	// 	args := map[string]interface{}{
	// 		// "tenant": tenant.Name,
	// 	}
	// 	if err := tx.Exec(q, args); err != nil {
	// 		return nil, errors.Wrap(err, "Couldn't set search path")
	// 	}
	// }

	// wf.Transaction = writeTx

	return nil

}

// RollbackTransaction will cancel all the operations in the transaction.
func (d *Client) RollbackTransaction(wk *libapplication.ApplicationContext) error {
	// err := wk.Transaction.(*Tx).Sqlx.Rollback()
	// if err != nil {
	// 	return errors.Wrap(err, "Couldn't rollback")
	// }
	// if tx.WriteTx != tx.ReadTx {
	// 	err := tx.ReadTx.(*Tx).Sqlx.Rollback()
	// 	if err != nil {
	// 		return errors.Wrap(err, "Couldn't rollback")
	// 	}
	// }
	return nil
}

// CommitTransaction will definitely save all the operations in the transaction.
func (d *Client) CommitTransaction(wk *libapplication.ApplicationContext) error {

	// fmt.Println("commit")
	// fmt.Println(wk)
	// fmt.Println(wk.Transaction)
	// err := wk.Transaction.(*Tx).Sqlx.Commit()
	// if err != nil {
	// 	fmt.Println("commit err ", err)
	// 	return errors.Wrap(err, "Couldn't commit")
	// }
	// if tx.WriteTx != tx.ReadTx {
	// 	err := tx.ReadTx.(*Tx).Sqlx.Commit()
	// 	if err != nil {
	// 		return errors.Wrap(err, "Couldn't commit")
	// 	}
	// }
	return nil
}

func (d *Client) DBExecute(ctx context.Context, q string, args []interface{}) gocql.Scanner {

	fmt.Println("scyllaDBExecute ", q, args)

	scanner := d.session.Query(
		q, args...).Iter().Scanner()

	fmt.Printf("scyllaDBExecute %+v\n", scanner)

	return scanner
}

func (d *Client) GenerateInsertBuildArgsFunc(datasourceClient libpersistence.DatasourceClient, table libpersistence.TableInterface) string {
	data := struct {
		Datasource libpersistence.DatasourceClient
		Table      libpersistence.TableInterface
	}{
		Datasource: datasourceClient,
		Table:      table,
	}

	buf := &bytes.Buffer{}
	err := insertBuildArgsTemplate.Execute(buf, data)
	if err != nil {
		fmt.Println("execute ", err)
	}
	// content, err := format.Source(buf.Bytes())
	// if err != nil {
	// 	fmt.Println(buf)
	// 	fmt.Println("model ", err)
	// }
	return buf.String()
}

type Batch struct {
	client *Client
	limit int
	entries []gocql.BatchEntry
}

func (d *Client) NewBatch(limit int) *Batch {
	return &Batch{
		client: d,
		limit: limit,
	}
}
func (b *Batch) AddEntry(entry gocql.BatchEntry) {
	b.entries = append(b.entries, entry)
	if len(b.entries) >= b.limit {
		b.Execute()
		b.entries = []gocql.BatchEntry{}
	}
}
func (d *Batch) Execute() {
	b := d.client.session.NewBatch(gocql.LoggedBatch)
	b.Entries = append(b.Entries, d.entries...)
	for _, entry := range b.Entries {
		fmt.Printf("%s\n%+v\n", entry.Stmt, entry.Args)
	}
	err := d.client.session.ExecuteBatch(b)
	if err != nil {
		panic(err)
	}
}

// nolint:lll
var insertBuildArgsTemplate = template.Must(template.New("").Funcs(template.FuncMap{}).Parse(`
func (d *{{.Table.AggregateDefinition.AggregateRoot.Name}}Repository) {{.Table.Name}}ScyllaDBInsertBuildArgs(
		records []interface{}) ([]string, map[string]interface{}) {


			{{if .Table.IsEntity}}
			var {{.Table.Name}}s []*pb.{{.Table.Title}}
			for _, record := range records {
				{{.Table.Name}}s = append({{.Table.Name}}s, record.(*pb.{{.Table.Title}}))
			}
			{{else}}
			var {{.Table.Name}}s []*pb.{{.Table.Title}}
			for _, record := range records {
				command := record.(*pb.Set{{.Table.Title}}Command)
				for _, object := range command.{{.Table.Title}}s {
					object.AggregateID = command.AggregateID
					{{.Table.Name}}s = append({{.Table.Name}}s, object)
				}
			}
			{{end}}
			
			args := map[string]interface{}{}
			for i, storage := range {{.Table.Name}}s {
		
				{{if eq .Table.GetDisableID false}}
					args[fmt.Sprintf("%v_id", i)] = storage.ID
				{{end}}
				{{if not .Table.IsEntity}}
				args[fmt.Sprintf("%v_aggregate_id", i)] = storage.AggregateID
				{{end}}
				{{- range .Table.StoredProperties }}
				{{if eq $.Datasource .GetExternalDatasource}}
				
				{{if eq .Type "Many2oneTypeTemp"}}
				{{.GetName}} := storage.{{.Title}}
				{{else}}
					{{ if and (eq .Type "TextType") .IsTranslatable}}
					{{.GetName}}, err := json.Marshal(storage.{{.Title}})
					if err != nil {
						panic(err)
					}
					{{else if eq .Type "DatetimeType"}}
					{{.GetName}} := libutils.ConvertTimestampFromProto(storage.{{.Title}})
					{{else}}
					{{.GetName}} := &storage.{{.Title}}
					{{if eq .GoType "string"}}
					if *{{.GetName}} == "" {
						{{.GetName}} = nil
					}
					{{end}}
					{{if eq .Type "Many2oneType"}}
					{{if not .IsRepeated}}
					if *{{.GetName}} == "" {
						{{.GetName}} = nil
					}
					{{end}}
					{{end}}
					{{end}}
				{{end}}
				args[fmt.Sprintf("%v_{{.Snake}}", i)] = {{.GetName}}
				{{end}}
				{{- end }}	
				{{if .Table.IsEntity}}
				{{if not .Datasource}}
				args[fmt.Sprintf("%v_external_module", i)] = storage.ExternalModule
				args[fmt.Sprintf("%v_external_id", i)] = storage.ExternalID	
				{{end}}
				{{end}}
			}
		
			return []string{
				{{if eq .Table.GetDisableID false}}
				"id",
				{{end}}
				{{- range .Table.StoredProperties }}
				{{if eq $.Datasource .GetExternalDatasource}}
				"{{.Snake}}",
				{{end}}
				{{- end }}
				{{if .Table.IsEntity}}
				{{if not .Datasource}}
				"external_module",
				"external_id",
				{{end}}
				{{end}}
			}, args
}
`))

func (d *Client) GenerateScanFunc(datasourceClient libpersistence.DatasourceClient, prefix string, table libpersistence.TableInterface) string {
	data := struct {
		Datasource libpersistence.DatasourceClient
		Prefix     string
		Table      libpersistence.TableInterface
	}{
		Datasource: datasourceClient,
		Prefix:     prefix,
		Table:      table,
	}

	buf := &bytes.Buffer{}
	err := scanTemplate.Execute(buf, data)
	if err != nil {
		fmt.Println("execute ", err)
	}
	// content, err := format.Source(buf.Bytes())
	// if err != nil {
	// 	fmt.Println(buf)
	// 	fmt.Println("model ", err)
	// }
	return buf.String()
}

// nolint:lll
var scanTemplate = template.Must(template.New("").Funcs(template.FuncMap{}).Parse(`
func (d *{{.Table.AggregateDefinition.AggregateRoot.Name}}Repository) {{.Table.Name}}{{.Prefix}}Scan(
	rows interface{}, collection interface{}, wk libdomain.ApplicationContext, properties []string,
) ([]*pb.{{.Table.Title}}, error) {
	var records []*pb.{{.Table.Title}}
	scanner := rows.(gocql.Scanner)
	for scanner.Next() {

		fmt.Println("properties ", properties)

		var scanProperties []interface{}
		for _, property := range properties {
			switch property {
			case "id":
				id := ""
				scanProperties = append(scanProperties, &id)
			{{if .Table.AggregateDefinition.UseTenants}}
			case "tenant_id":
				tenantID := ""
				scanProperties = append(scanProperties, &tenantID)
			{{end}}
			{{- range .Table.StoredProperties }}
			{{if not $.Table.GetExternalDatasource}}
			{{if ne $.Datasource .GetExternalDatasource}}
			{{continue}}
			{{end}}
			{{end}}
			{{if not .IsRepeated}}
			{{if eq .Type "FloatType"}}
			case "{{.Snake}}":
				var {{.Name}} float64
				scanProperties = append(scanProperties, &{{.Name}})
			{{else if eq .Type "BooleanType"}}
			case "{{.Snake}}":
				var {{.Name}} bool
				scanProperties = append(scanProperties, &{{.Name}})
			{{else if eq .Type "IntegerType"}}
			case "{{.Snake}}":
				var {{.Name}} int64
				scanProperties = append(scanProperties, &{{.Name}})
			{{else if eq .Type "DatetimeType"}}
			case "{{.Snake}}":
				var {{.Name}} time.Time
				scanProperties = append(scanProperties, &{{.Name}})
			{{else}}
			case "{{.Snake}}":
				var {{.Name}} string
				scanProperties = append(scanProperties, &{{.Name}})
			{{end}}
			{{else}}
			{{end}}
			{{- end}}
			{{if not .Datasource}}
			{{if not .Table.GetDisableDatetime}}
			case "created_at":
				scanProperties = append(scanProperties, &timestamp.Timestamp{})
			case "updated_at":
				scanProperties = append(scanProperties, &timestamp.Timestamp{})
			{{end}}
			{{if $.Table.IsEntity}}
			case "external_module":
				scanProperties = append(scanProperties, &sql.NullString{})
			case "external_id":
				scanProperties = append(scanProperties, &sql.NullString{})
			{{end}}
			{{end}}
			}
		}

		for _, property := range scanProperties {
			fmt.Printf("%T\n", property)
		}

		err := scanner.Scan(scanProperties...)
		if err != nil {
			fmt.Println(err)
			return nil, errors.Wrap(err, "Couldn't scan to templates")
		}

		var {{.Table.Name}} *pb.{{.Table.Title}}
		if collection == nil {
			{{.Table.Name}} = &pb.{{.Table.Title}}{
				OutOfCache: &pb.{{.Table.Title}}OutOfCache{},
			}
		} else {
			id := *scanProperties[0].(*string)
			{{.Table.Name}} = collection.(*{{.Table.Title}}Collection).GetByID({{.Table.Title}}ID(id)).Pb()
		}

		for i, property := range properties {
			switch property {
			case "id":
				{{.Table.Name}}.ID =  *scanProperties[i].(*string)
			{{if .Table.AggregateDefinition.UseTenants}}
			case "tenant_id":
				{{if .Table.AggregateDefinition.UseTenants}}{{.Table.Name}}.TenantID = *scanProperties[i].(*string){{end}}
			{{end}}
			{{- range .Table.StoredProperties }}
			{{if not $.Table.GetExternalDatasource}}
			{{if ne $.Datasource .GetExternalDatasource}}
			{{continue}}
			{{end}}
			{{end}}
			{{if not .IsRepeated}}
			case "{{.Snake}}":
				{{ if and (eq .Type "TextType") .IsTranslatable}}
				var {{.GetName}} map[string]string
				err := json.Unmarshal([]byte(scanProperties[i].(*sql.NullString).String), &{{.GetName}})
				if err != nil {
					return nil, errors.Wrap(err, "Couldn't unmarshal translations")
				}
				{{$.Table.Name}}.{{.Title}} = {{.GetName}}
				{{else if eq .Type "FloatType"}}
				{{.GetName}} := scanProperties[i].(*float64)
				{{$.Table.Name}}.{{.Title}} = *{{.GetName}}
				{{else}}
				//{{.GetName}} := scanProperties[i].(*string)
				{{if eq .GoType "int32"}}
				{{.GetName}} := scanProperties[i].(*int64)
				{{$.Table.Name}}.{{.Title}} = int32(*{{.Name}})
				{{else if eq .GoType "bool"}}
				{{.GetName}} := scanProperties[i].(*bool)
				{{$.Table.Name}}.{{.Title}} = *{{.GetName}}
				{{else if eq .Type "DatetimeType"}}
				{{.GetName}} := scanProperties[i].(*time.Time)
				{{$.Table.Name}}.{{.Title}} = libutils.ConvertTimestampToProto(*{{.GetName}})
				{{else}}
					{{if eq .GoType "float32"}}
					{{$.Table.Name}}.{{.Title}} = float32({{.Name}}.{{.DBType.Value}})
					{{else}}
						{{if eq .GoType "[]byte"}}
							{{$.Table.Name}}.{{.Title}} = []byte({{.Name}}.{{.DBType.Value}})
						{{else}}
							{{if eq .Type "Many2oneType"}}
								// {{$.Table.Name}}.{{.Title}} = &pb.{{.TitleWithoutID}}{ID: {{.GetName}}.{{.DBType.Value}} }
								{{.GetName}} := scanProperties[i].(*string)
								{{$.Table.Name}}.{{.Title}} = *{{.GetName}}
							{{else if eq .Type "IDType"}}
								// {{$.Table.Name}}.{{.Title}} = &pb.{{.TitleWithoutID}}{ID: {{.GetName}}.{{.DBType.Value}} }
								{{.GetName}} := scanProperties[i].(*string)
								{{$.Table.Name}}.{{.Title}} = *{{.GetName}}
							{{else}}
								{{if eq .GoType "[]string"}}
									{{$.Table.Name}}.{{.Title}} = []string{}
								{{else}}
									{{.GetName}} := scanProperties[i].(*string)
									{{$.Table.Name}}.{{.Title}} = *{{.GetName}}
								{{end}}
							{{end}}
						{{end}}
					{{end}}
				{{end}}
			{{end}}
			{{end}}
			{{- else}}
			{{- end}}
			{{if not .Datasource}}
			{{if not .Table.GetDisableDatetime}}
			case "created_at":
				createdAt := scanProperties[i].(*timestamp.Timestamp)
				{{.Table.Name}}.CreatedAt = createdAt
			case "updated_at":
				updatedAt := scanProperties[i].(*timestamp.Timestamp)
				{{.Table.Name}}.UpdatedAt = updatedAt
			{{end}}
			{{if $.Table.IsEntity}}
			case "external_module":
				externalModule := scanProperties[i].(*sql.NullString)
				{{.Table.Name}}.ExternalModule = externalModule.String
			case "external_id":
				externalID := scanProperties[i].(*sql.NullString)
				{{.Table.Name}}.ExternalID = externalID.String
			{{end}}
			{{end}}
			}
		}

		records = append(records, {{.Table.Name}})
		fmt.Printf("record %+v\n", {{.Table.Name}})

	}
	if err := scanner.Err(); err != nil {
		panic(err)
	}
	return records, nil
}
`))

func buildFilters(
	wk *libapplication.ApplicationContext, model libpersistence.TableInterface,
	filters []*libpersistence.Filter,
) (string, []interface{}, error) {

	query := ""
	args := []interface{}{}
	// // fmt.Printf("tx %+v\n", tx)
	if model.UseTenants() { //model.Cluster.MultiTenant && model != libdata.TenantModel {
		filters = append(filters, &libpersistence.Filter{
			Field:    "tenant_id",
			Operator: &libpersistence.IsEqualTo{Value: string(wk.Workflow().TenantID)},
		})
	}
	for _, w := range filters {
		if query != "" {
			query = fmt.Sprintf("%s AND ", query)
		}

		fmt.Printf("w %T\n", w.Operator)

		switch f := w.Operator.(type) {
		case *libpersistence.IsEqualTo:
			query = fmt.Sprintf("%s%s = %s", query, w.Field, f.Value)
			// args = append(args, f.Value)
		case *libpersistence.IsIn:
			query = fmt.Sprintf("%s%s in (%s)", query, w.Field, strings.Join(f.Values, ","))
			// args = append(args, f.Values)
			// args[w.Field] = "{" + strings.Join(f.Values, ",") + "}"
		case *libpersistence.IsNull:
			query = fmt.Sprintf("%s%s IS NULL", query, w.Field)
		case *libpersistence.IsNotNull:
			query = fmt.Sprintf("%s%s IS NOT NULL", query, w.Field)
		case *libpersistence.IsTrue:
			query = fmt.Sprintf("%s%s = True", query, w.Field)
		case *libpersistence.IsFalse:
			query = fmt.Sprintf("%s%s = False", query, w.Field)
		case *libpersistence.IsBefore:
			query = fmt.Sprintf("%s%s < ?", query, w.Field)
			args = append(args, f.Value)
		case *libpersistence.IsAfter:
			query = fmt.Sprintf("%s%s < ?", query, w.Field)
			args = append(args, f.Value)
			// default:
			// 	return "", nil, &DBUnrecognizedOperatorError{O: fmt.Sprintln(w.Operator)}
		}

	}

	if query != "" {
		// nolint: gas
		query = fmt.Sprintf("WHERE %s", query)
	}
	return query, args, nil
}
