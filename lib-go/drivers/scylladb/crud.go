package scylladb

import (
	"fmt"
	"strconv"
	"strings"

	"github.com/gocql/gocql"
	"github.com/pkg/errors"
	"gitlab.com/empowerlab/stack/lib-go/libdomain/libapplication"
	"gitlab.com/empowerlab/stack/lib-go/libinfrastructure/libpersistence"
)

func (d *Client) Insert(
	t libpersistence.TableInterface, wk *libapplication.ApplicationContext, records []interface{},
	fields []string, args map[string]interface{},
	properties []string,
) (interface{}, error) {

	fmt.Printf("fields %+v\n", fields)
	fmt.Printf("args %+v\n", args)

	/* span, _ := opentracing.StartSpanFromContext(wk.Workflow().Context, "postgres-"+t.GetName()+"-insert")
	defer span.Finish()
	wk.Workflow().Context = opentracing.ContextWithSpan(wk.Workflow().Context, span) */

	// fmt.Printf("%+v", tx.Tenant)
	// fields, args := insertBuildArgs(records)
	var values []string
	var argsSlice []interface{}
	var ids []gocql.UUID
	for i := range records {
		var recordValues []string

		if t.UseTenants() { //tx.Cluster.MultiTenant {
			// tenantName := fmt.Sprintf(":%v_tenant_id", i)
			recordValues = append([]string{"?"}, recordValues...)
			argsSlice = append([]interface{}{string(wk.Workflow().TenantID)}, argsSlice...)
		}
		// recordValues = append(recordValues, "?")
		// argsSlice = append(argsSlice, args[fmt.Sprintf("%d_id", i)])
		// fmt.Println(fmt.Sprintf("%d_id", i))
		id, err := gocql.ParseUUID(args[fmt.Sprintf("%d_id", i)].(string))
		if err != nil {
			return nil, errors.Wrap(err, "failed to parse uuid")
		}
		ids = append(ids, id)
		for _, field := range fields {
			recordValues = append(recordValues, "?")
			argsSlice = append(argsSlice, args[fmt.Sprintf("%d_%s", i, field)])
		}
		values = append(values, strings.Join(recordValues, ","))
	}

	if t.UseTenants() {
		fields = append([]string{"tenant_id"}, fields...)
	}
	// fields = append([]string{"id"}, fields...)

	fmt.Printf("fields %s\n", fields)
	fmt.Printf("args %+v\n", argsSlice)
	fmt.Printf("properties %+v\n", properties)

	//nolint:gas
	q := fmt.Sprintf(
		"INSERT INTO \"%s\" (%s) VALUES (%s)",
		t.Snake(), strings.Join(fields, ","), strings.Join(values, "),("))
	/*span.LogFields(
		otlog.String("query", q),
		otlog.Object("args", args),
	)*/
	fmt.Println(q)

	// rows, err := wk.Transaction.(*Tx).QueryRows(q, args)
	// if err != nil {
	// 	return nil, errors.Wrap(err, "Couldn't insert record")
	// }
	// rows, err := libapplication.ExecuteActivity(
	// 	wk.OrchestratorClient, wk, &InsertActivity{
	// 		Workflow: wk,
	// 		Query:    q,
	// 		Args:     args,
	// 	}, d)

	scanner := d.DBExecute(wk.Workflow().Context, q, argsSlice)
	if err := scanner.Err(); err != nil {
		panic(err)
		return nil, err
	}

	q = fmt.Sprintf(
		"SELECT %s FROM \"%s\" WHERE id in ?", strings.Join(properties, ","), t.Snake())
	/*span.LogFields(
		otlog.String("query", q),
		otlog.Object("args", args),
	)*/
	fmt.Println(q)

	// rows, err := wk.Transaction.(*Tx).QueryRows(q, args)
	// if err != nil {
	// 	return nil, errors.Wrap(err, "Couldn't insert record")
	// }
	// rows, err := libapplication.ExecuteActivity(
	// 	wk.OrchestratorClient, wk, &InsertActivity{
	// 		Workflow: wk,
	// 		Query:    q,
	// 		Args:     args,
	// 	}, d)

	scanner = d.DBExecute(wk.Workflow().Context, q, []interface{}{ids})
	// if err := scanner.Err(); err != nil {
	// 	return nil, err
	// }

	// rows, err := wk.Transaction.(*Tx).QueryRows(q, args)
	// if err != nil {
	// 	return nil, errors.Wrap(err, "Couldn't update record")
	// }
	// if err != nil {
	// 	return nil, err
	// }

	return scanner, nil
}

// Update will update the data in the filtered records.
func (d *Client) Update(
	t libpersistence.TableInterface, wk *libapplication.ApplicationContext,
	filters []*libpersistence.Filter, record interface{}, set []string, args map[string]interface{},
	properties []string,
) (interface{}, error) {
	/*span, _ := opentracing.StartSpanFromContext(wk.Workflow().Context, "postgres-"+t.GetName()+"-update")
	defer span.Finish()
	wk.Workflow().Context = opentracing.ContextWithSpan(wk.Workflow().Context, span) */

	// set, args := updateBuildArgs(record, updateMask)

	fmt.Printf("set %+v\n", set)
	fmt.Printf("args %+v\n", args)

	var setFinal []string
	var argsFinal []interface{}
	for _, s := range set {
		key := strings.Split(s, "=")[0]
		setFinal = append(setFinal, fmt.Sprintf("%s = ?", key))
		argsFinal = append(argsFinal, args[key])
	}

	filterQuery, filterArgs, err := buildFilters(wk, t, filters)
	if err != nil {
		return nil, errors.Wrap(err, "Couldn't build where query")
	}
	for _, arg := range filterArgs {
		argsFinal = append(argsFinal, arg)
	}

	//nolint:gas
	q := fmt.Sprintf(
		"UPDATE \"%s\" SET %s %s",
		t.Snake(), strings.Join(setFinal, ","), filterQuery)
	/*span.LogFields(
		otlog.String("query", q),
		otlog.Object("args", args),
	)*/
	scanner := d.DBExecute(wk.Workflow().Context, q, argsFinal)
	if err := scanner.Err(); err != nil {
		fmt.Println(err)
		return nil, err
	}

	q = fmt.Sprintf(
		"SELECT %s FROM \"%s\" %s", strings.Join(properties, ","), t.Snake(), filterQuery)
	/*span.LogFields(
		otlog.String("query", q),
		otlog.Object("args", args),
	)*/
	fmt.Println(q)

	// rows, err := wk.Transaction.(*Tx).QueryRows(q, args)
	// if err != nil {
	// 	return nil, errors.Wrap(err, "Couldn't insert record")
	// }
	// rows, err := libapplication.ExecuteActivity(
	// 	wk.OrchestratorClient, wk, &InsertActivity{
	// 		Workflow: wk,
	// 		Query:    q,
	// 		Args:     args,
	// 	}, d)

	argsFinal = []interface{}{}
	for _, arg := range filterArgs {
		argsFinal = append(argsFinal, arg)
	}

	scanner = d.DBExecute(wk.Workflow().Context, q, argsFinal)

	return scanner, nil

}

// Select will return the filtered records.
func (d *Client) Get(
	t *libpersistence.TableDefinition,
	wk *libapplication.ApplicationContext, id string,
) (interface{}, error) {
	return nil, nil
}

// Select will return the filtered records.
func (d *Client) Select(
	model libpersistence.TableInterface, wk *libapplication.ApplicationContext,
	filters []*libpersistence.Filter, properties []string, options *libpersistence.OptionsListQuery,
) (interface{}, error) {
	/* span, _ := opentracing.StartSpanFromContext(wk.Workflow().Context, "postgres-"+model.GetName()+"-select")
	defer span.Finish()
	wk.Workflow().Context = opentracing.ContextWithSpan(wk.Workflow().Context, span) */

	filterQuery, args, err := buildFilters(wk, model, filters)
	if err != nil {
		return nil, errors.Wrap(err, "Couldn't build where query")
	}
	limitQuery := ""
	orderByQuery := ""
	allowFiltering := ""
	if options != nil {
		if options.Limit != 0 {
			limitQuery = fmt.Sprintf("LIMIT %s", strconv.Itoa(int(options.Limit)))
		}
		if options.OrderBy != "" {
			// for _, o := range query.OrderBy {
			// 	if orderByQuery != "" {
			// 		orderByQuery = fmt.Sprintf("%s, ", orderByQuery)
			// 	}
			// 	orderByQuery = fmt.Sprintf("%s %s", orderByQuery, o.Field)
			// 	if o.Desc {
			// 		orderByQuery = fmt.Sprintf("%s %s", orderByQuery, "DESC")
			// 	}
			// }
			orderByQuery = fmt.Sprintf("ORDER BY %s", options.OrderBy)
		}
		if options.AllowFiltering {
			allowFiltering = "ALLOW FILTERING"
		}

	}

	//nolint:gas
	q := fmt.Sprintf("SELECT %s from \"%s\" %s %s %s %s",
		strings.Join(properties, ","), model.Snake(), filterQuery, orderByQuery, limitQuery, allowFiltering)
	/*span.LogFields(
		otlog.String("query", q),
		otlog.Object("args", args),
	)*/
	// rows, err := wk.Transaction.(*Tx).QueryRows(q, args)
	// if err != nil {
	// 	return nil, errors.Wrap(err, "Couldn't get record")
	// }
	scanner := d.DBExecute(wk.Workflow().Context, q, args)
	return scanner, nil

}

// Delete will delete the filtered records.
func (d *Client) Delete(
	model libpersistence.TableInterface,
	wk *libapplication.ApplicationContext, filters []*libpersistence.Filter,
) error {
	/* span, _ := opentracing.StartSpanFromContext(wk.Workflow().Context, "postgres-"+model.GetName()+"-delete")
	defer span.Finish()
	wk.Workflow().Context = opentracing.ContextWithSpan(wk.Workflow().Context, span) */

	// filterQuery, args, err := buildFilters(wk, model, filters)
	// if err != nil {
	// 	return errors.Wrap(err, "Couldn't build where query")
	// }

	// q := fmt.Sprintf(
	// 	"DELETE FROM \"%s\" %s", model.Snake(), filterQuery)
	// /*span.LogFields(
	// 	otlog.String("query", q),
	// 	otlog.Object("args", args),
	// )*/
	// // nolint: gas
	// err = wk.Transaction.(*Tx).Exec(q, args)
	// if err != nil {
	// 	return errors.Wrap(err, "Couldn't delete record")
	// }
	// return nil

	return nil
}
