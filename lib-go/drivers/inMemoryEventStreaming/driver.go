package inMemoryEventStreaming

import (
	"context"
	"fmt"
	"strings"
	"time"

	"github.com/nats-io/nats.go"
	"github.com/thanhpk/randstr"
	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain"
	// "gitlab.com/empowerlab/stack/lib-go/libdata"
)

type ClientInfo struct {
	URL string
}

func NewClient() (*Client, error) {

	return &Client{
		subscribers: map[string][]Subscriber{},
	}, nil
}

// Driver contains all the function needed to be recognized as a libdata driver
type Client struct {
	subscribers map[string][]Subscriber
}

func (c *Client) Connection() *nats.Conn {
	// return c.nc
	return nil
}

func (c *Client) DispatchEvent(ctx context.Context, aggregate string, payload []byte) error {

	fmt.Println("!!!!!!!!!")
	fmt.Println("!!!!!!!!!")
	fmt.Println("!!!!!!!!!")
	fmt.Printf("aggregate = %+v\n", aggregate)
	fmt.Printf("subscribers = %+v\n", c.subscribers)

	topics := []string{aggregate, strings.Split(aggregate, ".")[0] + ".*"}
	for _, topic := range topics {
		fmt.Printf("topic = %+v\n", topic)
		if _, ok := c.subscribers[topic]; ok {
			for _, subscriber := range c.subscribers[topic] {
				subscriber.HandleEvent(payload)
			}
		}
	}

	return nil

}

func (c *Client) Subscribe(ctx context.Context, consumer string, stream string, subject string, function func([]byte) error) (interface{}, error) {

	subscriber := Subscriber{
		function: function,
	}
	if _, ok := c.subscribers[subject]; !ok {
		c.subscribers[subject] = []Subscriber{}
	}
	c.subscribers[subject] = append(c.subscribers[subject], subscriber)
	return subscriber, nil
}

func (c *Client) SubscribeSync(stream string) (libdomain.EventDispatcherSubscription, error) {

	subscriber := &Subscriber{}
	return subscriber, nil
}

func (c *Client) Publish(uniqueReplyTo string, message []byte) error {
	// err := c.nc.Publish(uniqueReplyTo, message)
	// if err != nil {
	// 	return err
	// }

	return nil
}

func (c *Client) NewInbox() string {

	return randstr.String(32)

}

type Subscriber struct {
	function func([]byte) error
}

func (s *Subscriber) HandleEvent(event []byte) {
	fmt.Printf("Received a JetStream message: %s\n", string(event))
	s.function(event)
}

func (s *Subscriber) NextMsg(time.Duration) ([]byte, error) {
	return nil, nil
}
