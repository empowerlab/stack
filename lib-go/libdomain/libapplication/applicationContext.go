package libapplication

import (
	"context"
	"encoding/json"
	"fmt"
	"math"
	"os"
	"strings"
	"time"

	"github.com/golang-jwt/jwt"
	"github.com/google/uuid"
	"go.opentelemetry.io/otel"
	grpcMetadata "google.golang.org/grpc/metadata"
	"google.golang.org/protobuf/proto"

	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain"
)

type ApplicationContext struct {
	workflow                     *libdomain.Workflow
	TransactionClient            TransactionClient
	Transaction                  interface{}
	CacheClient                  CacheClient
	cache                        map[string]map[string]*WorkflowCache
	domainEventPublisherInstance *libdomain.DomainEventPublisherInstance
	eventsForEventStore          []*Event
	deterministicTestIds		 map[string]int
	fromDomain                   bool
	// uniqueReplyTo                string
	timestamp int64
	sudo bool
}

func NewApplicationContext(ctx context.Context, transactionClient TransactionClient, cacheClient CacheClient, domainEventPublisher *libdomain.DomainEventPublisher, fromDomain bool) *ApplicationContext {

	// var uniqueReplyTo string
	// if ctx.Value("uniqueReplyTo") != nil {
	// 	uniqueReplyTo = ctx.Value("uniqueReplyTo").(string)
	// }

	var domainEventPublisherInstance *libdomain.DomainEventPublisherInstance
	if domainEventPublisher != nil {
		domainEventPublisherInstance = domainEventPublisher.Instance()
	}

	tracer := otel.GetTracerProvider().Tracer(os.Getenv("OTEL_SERVICE_NAME"))

	return &ApplicationContext{
		workflow: &libdomain.Workflow{
			Context: ctx,
			Tracer: tracer,
		},
		TransactionClient:            transactionClient,
		CacheClient:                  cacheClient,
		cache:                        map[string]map[string]*WorkflowCache{},
		domainEventPublisherInstance: domainEventPublisherInstance,
		fromDomain:                   fromDomain,
		// uniqueReplyTo:                uniqueReplyTo,
		deterministicTestIds: map[string]int{},
	}
}

func NewApplicationContextFromService(c context.Context, tenantID string, locale string, s ApplicationService) (*ApplicationContext, error) {
	ctx := NewApplicationContext(c, s.TransactionClient(), s.CacheClient(), s.DomainEventPublisher(), true)
	err := s.TransactionClient().BeginTransaction(ctx, true)
	if err != nil {
		return nil, err
	}
	ctx.Workflow().TenantID = libdomain.TenantID(tenantID)
	ctx.Workflow().Locale = locale
	err = ctx.Authenticate(s)
	if err != nil {
		return nil, err
	}

	return ctx, nil
}

func (a *ApplicationContext) FromDomain() bool {
	return a.fromDomain
}

func (a *ApplicationContext) Copy() *ApplicationContext {

	return &ApplicationContext{
		TransactionClient:            a.TransactionClient,
		Transaction: 				  a.Transaction,
		CacheClient:                  a.CacheClient,
		cache:                        a.cache,
		domainEventPublisherInstance: a.domainEventPublisherInstance,
		eventsForEventStore: 		a.eventsForEventStore,
		fromDomain:                   a.fromDomain,
		// uniqueReplyTo:                uniqueReplyTo,
		deterministicTestIds: a.deterministicTestIds,
		timestamp: 				  a.timestamp,
		sudo: 					  a.sudo,
	}
}

func (a *ApplicationContext) WithContext(ctx context.Context) *ApplicationContext {

	applicationContext := a.Copy()
	applicationContext.workflow = a.Workflow().WithContext(ctx)

	return applicationContext
}

func (a *ApplicationContext) WithTenantID(tenantID libdomain.TenantID) *ApplicationContext {

	applicationContext := a.Copy()
	applicationContext.workflow = a.Workflow().WithTenantID(tenantID)

	return applicationContext
}

func (a *ApplicationContext) WithSudo() *ApplicationContext {

	applicationContext := a.Copy()
	applicationContext.workflow = a.Workflow().Copy()
	applicationContext.sudo = true

	return applicationContext
}

func (a *ApplicationContext) AppendEventStore(aggregate libdomain.Aggregate) {
	i := aggregate.UnmutatedVersion()
	for _, event := range aggregate.MutatingEvents() {

		newUUID, err := uuid.NewV7()
		if err != nil {
			panic(err)
		}

		i = i + 1
		a.eventsForEventStore = append(a.eventsForEventStore, &Event{
			Aggregate: aggregate.Definition(),
			Stream:    event.GetStreamToDispatch(),
			Data: &EventData{
				ID:               newUUID.String(),
				TenantID:         string(aggregate.IdentityInterface().TenantID()),
				AggregateName:    aggregate.Definition().AggregateRoot.Name,
				AggregateID:      aggregate.IdentityInterface().GetIDAsString(),
				AggregateVersion: i,
				Event:            string(event.Type()),
				EventVersion:     event.TypeVersion(),
				Current:          aggregate.Map(),
				Diff:             event.Diff().Map(),
				Payload:          event.Map(),
				// UniqueReplyTo:    a.uniqueReplyTo,
				OccurredAt: event.OccurredAt(),
				OccurredBy: event.OccurredBy(),
			},
		})
	}
	aggregate.Mutated()
}

func (a *ApplicationContext) Authenticate(authService AuthService) error {

	token := ""
	value, _ := grpcMetadata.FromIncomingContext(a.workflow.Context)
	authorization := value.Get("Authorization")
	fmt.Printf("authorization: %v\n", authorization)
	if len(authorization) > 0 {
		token = strings.Replace(authorization[0], "Bearer ", "", -1)
	}

	signedJWT := ""
	value, _ = grpcMetadata.FromIncomingContext(a.workflow.Context)
	fmt.Println(value)
	jwtValue := value.Get("JWT")
	if len(jwtValue) > 0 {
		signedJWT = jwtValue[0]
	}

	fmt.Println("JWT")
	fmt.Println(signedJWT)
	fmt.Println(a.Workflow().TenantID)

	if token != "" && signedJWT == "" && a.Workflow().TenantID != "" {

		userID, email, groupIds, err := authService.Authenticate(a, token)
		if err != nil {
			return err
		}

		privateKey := []byte(os.Getenv("JWT_PRIVATE_KEY"))

		key, err := jwt.ParseRSAPrivateKeyFromPEM(privateKey)
		if err != nil {
			return fmt.Errorf("create: parse key: %w", err)
		}

		signedJWT, err = jwt.NewWithClaims(jwt.SigningMethodRS256, &libdomain.JWTClaims{
			StandardClaims: &jwt.StandardClaims{
				// set the expire time
				// see http://tools.ietf.org/html/draft-ietf-oauth-json-web-token-20#section-4.1.4
				ExpiresAt: time.Now().Add(time.Minute * 1).Unix(),
			},
			UserID:    userID,
			UserEmail: email,
			Groups:    groupIds,
		}).SignedString(key)
		if err != nil {
			return fmt.Errorf("create: sign token: %w", err)
		}

	}

	fmt.Printf("signedJWT: %s\n", signedJWT)

	decryptedJWT := &libdomain.JWTClaims{}
	if signedJWT != "" {

		publicKey := []byte(os.Getenv("JWT_PUBLIC_KEY"))

		key, err := jwt.ParseRSAPublicKeyFromPEM(publicKey)
		if err != nil {
			return fmt.Errorf("validate: parse key: %w", err)
		}

		// Parse the token
		tok, err := jwt.Parse(signedJWT, func(jwtToken *jwt.Token) (interface{}, error) {
			// since we only use the one private key to sign the tokens,
			// we also only use its public counter part to verify
			if _, ok := jwtToken.Method.(*jwt.SigningMethodRSA); !ok {
				fmt.Println("hum")
				return nil, fmt.Errorf("unexpected method: %s", jwtToken.Header["alg"])
			}
			return key, nil
		})
		if err != nil {
			return err
		}

		claims, ok := tok.Claims.(jwt.MapClaims)
		if !ok || !tok.Valid {
			fmt.Println("hum3")
			return fmt.Errorf("validate: invalid")
		}

		// var groups []interface{}
		// if claims["Groups"] != nil {
		// 	for _, groupInterface := range claims["Groups"].([]interface{}) {
		// 		group := groupInterface.(map[string]interface{})
		// 		groups = append(groups, &basepb.GetByExternalIDQuery{
		// 			Module: group["module"].(string),
		// 			ID:     group["id"].(string),
		// 		})
		// 	}
		// }

		groups := map[string]bool{}
		if claims["Groups"] != nil {
			for group, value := range claims["Groups"].(map[string]interface{}) {
				groups[group] = value.(bool)
			}
		}

		decryptedJWT = &libdomain.JWTClaims{
			StandardClaims: &jwt.StandardClaims{
				// set the expire time
				// see http://tools.ietf.org/html/draft-ietf-oauth-json-web-token-20#section-4.1.4
				ExpiresAt: int64(claims["exp"].(float64)),
			},
			UserID:    claims["UserID"].(string),
			UserEmail: claims["UserEmail"].(string),
			TenantID:  libdomain.TenantID(claims["TenantID"].(string)),
			Groups:    groups,
		}
	}

	a.workflow.JWT = signedJWT
	a.workflow.JWTDecrypted = decryptedJWT

	return nil
}

func (a *ApplicationContext) DomainEventPublisherInstance() *libdomain.DomainEventPublisherInstance {
	return a.domainEventPublisherInstance
}

func (w *ApplicationContext) Workflow() *libdomain.Workflow {
	return w.workflow
}

func (r *ApplicationContext) Commit(err error) error {

	if err != nil {
		r.TransactionClient.RollbackTransaction(r)
		return err
	}

	if r.fromDomain {
		if len(r.eventsForEventStore) > 0 {
			err := r.TransactionClient.RegisterEvents(
				r,
				nil,
				// a.Aggregate,
				r.eventsForEventStore,
			)
			if err != nil {
				r.TransactionClient.RollbackTransaction(r)
				panic(err)
			}
		}
	}

	r.TransactionClient.CommitTransaction(r)

	// for _, event := range r.Workflow().GetEvents() {

	// 	// applicationAggregate := MappingAggregate[event.Aggregate.Definition()]

	// 	// if applicationAggregate.EventStoreClient() != nil {

	// 	// 	eventResult, err := ExecuteActivity(
	// 	// 		r.client, r.context, &RegisterEventActivity{
	// 	// 			Workflow:         r.context,
	// 	// 			EventStoreClient: applicationAggregate.EventStoreClient(),
	// 	// 			Aggregate:        event.Aggregate,
	// 	// 			AggregateID:      event.AggregateID,
	// 	// 			Event:            event.Name,
	// 	// 			Payload:          event.Payload,
	// 	// 		}, r.context.TransactionClient)
	// 	// 	if err != nil {
	// 	// 		return nil, err
	// 	// 	}
	// 	// eventResult, err := activityRun.Get(r.context, nil)
	// 	// if err != nil {
	// 	// 	return nil, err
	// 	// }

	// 	// event = eventResult.(*libdomain.Event)

	for _, cache := range r.cache {
		for _, cacheItem := range cache {

			// fmt.Printf("Cache item: %+v\n", cacheItem)
			if cacheItem.CacheClient != nil {

				if cacheItem.ToInvalidate {
					fmt.Printf("Cache invalidated for %s\n", cacheItem.Key)
					cacheItem.CacheClient.Invalidate(r.workflow.Context, cacheItem.Key)
					continue
				}

				if cacheItem.Item != nil {
					cacheItem.PurgeOutOfCacheFunction(cacheItem.Item)
					item, err := proto.Marshal(cacheItem.Item)
					if err != nil {
						fmt.Println("Error while marshalling cache", err)
					}
					cacheItem.CacheClient.Set(r.workflow.Context, cacheItem.Key, item, 0)
					if err != nil {
						fmt.Println("Error while setting cache", err)
					// } else {
					// 	fmt.Printf("Cache set for %s: %+v\n", cacheItem.Key, (cacheItem.Item))
					}
				}
			}
		}
	}

	if r.fromDomain {
		for _, event := range r.eventsForEventStore {

			if event.Stream != nil {

				payload, err := json.Marshal(event.Data)
				if err != nil {
					fmt.Printf("error marshaling event json: %s", err.Error())
					continue
				}

				fmt.Println("Dispatching event to stream", event.Stream.StreamName())

				err = event.Stream.EventDispatcher().DispatchEvent(r.workflow.Context, event.Stream.StreamName(), payload)
				// _, err := ExecuteActivity(
				// 	r.client, r.context, &DispatchEventActivity{
				// 		Workflow:            r.context,
				// 		EventDispatchClient: applicationAggregate.EventDispatchClient(),
				// 		Event:               event,
				// 	}, r.context.TransactionClient)
				if err != nil {
					fmt.Printf("error dispatching event: %s %s", err.Error(), payload)
					break
				}
				// _, err = activityRun.Get(r.context, nil)
				// if err != nil {
				// 	return nil, err
				// }

			}

			err := r.TransactionClient.MarkDispatchedEvent(r, event)
			if err != nil {
				fmt.Printf("error marking event as dispatched: %s", err.Error())
				break
			}
		}
	}

	return nil
}

func (w *ApplicationContext) GetCache() map[string]map[string]*WorkflowCache {
	return w.cache
}

func (w *ApplicationContext) SetWorkflow(workflow *libdomain.Workflow) {
	w.workflow = workflow
	w.cache = map[string]map[string]*WorkflowCache{}
}

func (w *ApplicationContext) GetTimestamp() int64 {
	timestamp := int64(math.Round(float64(time.Now().UnixNano()) / 1000))
	fmt.Println("Current timestamp: ", w.timestamp, " - Proposed timestamp: ", timestamp)
	if timestamp < w.timestamp {
		w.timestamp = w.timestamp + 1
	} else {
		w.timestamp = timestamp
	}
	fmt.Println("Returned timestamp: ", w.timestamp)
	return w.timestamp
}

func (w *ApplicationContext) Sudo() libdomain.ApplicationContext {
	return &ApplicationContext{
		workflow:                     w.workflow,
		TransactionClient:            w.TransactionClient,
		CacheClient:                  w.CacheClient,
		cache:                        w.cache,
		domainEventPublisherInstance: w.domainEventPublisherInstance,
		eventsForEventStore:          w.eventsForEventStore,
		fromDomain:                   w.fromDomain,
		// uniqueReplyTo:                w.uniqueReplyTo,
		timestamp:                    w.timestamp,
		sudo: 					   true,
	}
}
func (w *ApplicationContext) IsSudo() bool {
	return w.sudo
}
func (w *ApplicationContext) GetDeterministicTestIds(entityName string) int {
	if _, ok := w.deterministicTestIds[entityName]; !ok {
		return 0
	}
	return w.deterministicTestIds[entityName]
}
func (w *ApplicationContext) SetDeterministicTestIds(entityName string, value int) {
	w.deterministicTestIds[entityName] = value
}

type WorkflowCache struct {
	CacheClient
	Key          string
	ToInvalidate bool
	Item         proto.Message
	PurgeOutOfCacheFunction func(proto.Message)
}

type ProtobufInterface interface {
	// Unmarshal([]byte) error
	// Marshal() ([]byte, error)
}

type EntityInterface interface {
	GetID() string
	GetExternalID() string
}

type CacheClient interface {
	Set(context.Context, string, []byte, time.Duration) error
	Get(context.Context, string) ([]byte, error)
	Invalidate(context.Context, string) error
}

type TransactionClient interface {
	BeginTransaction(*ApplicationContext, bool) error
	RollbackTransaction(*ApplicationContext) error
	CommitTransaction(*ApplicationContext) error
	RegisterEvents(*ApplicationContext, interface{}, []*Event) error
	MarkDispatchedEvent(*ApplicationContext, *Event) error
	GetAggregateEvents(*ApplicationContext, *libdomain.AggregateDefinition, string) ([]*Event, error)
	GetAggregateHistory(*ApplicationContext, *libdomain.AggregateDefinition, string) ([]*libdomain.EventHistory, error)
}

type AuthService interface {
	Authenticate(*ApplicationContext, string) (string, string, map[string]bool, error)
}
type ApplicationService interface {
	AuthService
	TransactionClient() TransactionClient
	CacheClient() CacheClient
	DomainEventPublisher() *libdomain.DomainEventPublisher
}

type Event struct {
	Aggregate *libdomain.AggregateDefinition
	Stream    *libdomain.Stream
	Data      *EventData
}
type EventData struct {
	ID               string                 `json:"id"`
	TenantID         string                 `json:"tenantID"`
	AggregateName    string                 `json:"aggregateName"`
	AggregateID      string                 `json:"aggregateID"`
	AggregateVersion int                    `json:"aggregateVersion"`
	Event            string                 `json:"event"`
	EventVersion     int                    `json:"eventVersion"`
	Current          map[string]interface{} `json:"current"`
	Diff             map[string]interface{} `json:"diff"`
	Payload          map[string]interface{} `json:"payload"`
	// UniqueReplyTo    string                 `json:"uniqueReplyTo"`
	OccurredAt time.Time `json:"occurredAt"`
	OccurredBy string    `json:"occurredBy"`
}
