package libdomain

import (
	"errors"
	"fmt"
	"time"
)

type DomainEventPublisher struct {
	subscribers []*DomainEventSubscriber
}

func NewDomainEventPublisher() *DomainEventPublisher {
	return &DomainEventPublisher{}
}

func (d *DomainEventPublisher) Instance() *DomainEventPublisherInstance {
	return &DomainEventPublisherInstance{
		publisher: d,
	}
}

func (d *DomainEventPublisher) Subscribe(aSubscriber *DomainEventSubscriber) error {

	d.subscribers = append(d.subscribers, aSubscriber)

	return nil
}

type DomainEventPublisherInstance struct {
	publisher   *DomainEventPublisher
	publishing  bool
	subscribers []*DomainEventSubscriber
}

func (d *DomainEventPublisherInstance) Publish(ctx ApplicationContext, aggregate interface{}, event DomainEvent) error {

	if d.publishing {
		return errors.New("can't publish while publishing")
	}

	d.publishing = true
	defer func() {
		d.publishing = false
	}()

	fmt.Printf("Publishing event: %+v\n", event)
	fmt.Printf("subscribers: %+v\n", d.publisher.subscribers)

	var subscribers []*DomainEventSubscriber
	subscribers = append(subscribers, d.publisher.subscribers...)
	subscribers = append(subscribers, d.subscribers...)

	for _, subscriber := range subscribers {
		if subscriber.SubscribedToEventType() == event.Type() {
			fmt.Printf("handling subscriber: %+v\n", subscriber)
			err := subscriber.HandleEvent(ctx, aggregate, event)
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func (d *DomainEventPublisherInstance) Subscribe(aSubscriber *DomainEventSubscriber) error {
	if d.publishing {
		return errors.New("can't subscribe while publishing")
	}

	d.subscribers = append(d.subscribers, aSubscriber)

	return nil
}

type DomainEventSubscriber struct {
	handleEvent           func(ctx ApplicationContext, aggregate interface{}, event interface{}) error
	subscribedToEventType func() EventType
}

func NewDomainEventSubscriber(handleEvent func(ctx ApplicationContext, aggregate interface{}, event interface{}) error, subscribedToEventType func() EventType) *DomainEventSubscriber {
	return &DomainEventSubscriber{
		handleEvent:           handleEvent,
		subscribedToEventType: subscribedToEventType,
	}
}

func (d *DomainEventSubscriber) HandleEvent(ctx ApplicationContext, aggregate interface{}, event interface{}) error {
	return d.handleEvent(ctx, aggregate, event)
}

func (d *DomainEventSubscriber) SubscribedToEventType() EventType {
	return d.subscribedToEventType()
}

type DomainEventDiff interface {
	Map() map[string]interface{}
}

type DomainEvent interface {
	Type() EventType
	TypeVersion() int
	OccurredAt() time.Time
	OccurredBy() string
	Diff() DomainEventDiff
	Map() map[string]interface{}
	// DispatchToStream(ApplicationContext, Aggregate) *Stream
	SetStreamToDispatch(*Stream)
	GetStreamToDispatch() *Stream
}
type EventType string

type AggregateIdentity interface {
	GetIDAsString() string
	TenantID() TenantID
}

type Stream struct {
	eventDispatcher EventDispatcher
	streamName      string
}

func NewStream(eventDispatcher EventDispatcher, streamName string) *Stream {

	if eventDispatcher == nil {
		panic("eventDispatcher is nil")
	}

	if streamName == "" {
		panic("streamName is empty")
	}

	return &Stream{
		eventDispatcher: eventDispatcher,
		streamName:      streamName,
	}
}

func (s *Stream) EventDispatcher() EventDispatcher {
	return s.eventDispatcher
}

func (s *Stream) StreamName() string {
	return s.streamName
}
