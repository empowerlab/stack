package libdomain

import (
	"context"

	"github.com/golang-jwt/jwt"
	"go.opentelemetry.io/otel/trace"
)

type Workflow struct {
	Context context.Context
	// WorkflowID string
	// WorkflowStep int
	TenantID     TenantID
	Locale       string
	JWT          string
	JWTDecrypted *JWTClaims
	// UseCache    bool
	// Transaction interface{}
	// TransactionClient       TransactionClient
	// TransactionAlwaysCommit bool
	// TenantClient            DatasourceClient
	// OrchestratorClient OrchestratorClient
	Tracer  trace.Tracer
	events *[]*DomainEvent
}

func NewWorkflow(ctx context.Context, tenantID TenantID, locale string, jwt string, jwtDecrypted *JWTClaims, tracer trace.Tracer) *Workflow {
	return &Workflow{
		Context:      ctx,
		TenantID:     tenantID,
		Locale:       locale,
		JWT:          jwt,
		JWTDecrypted: jwtDecrypted,
		Tracer:       tracer,
		events:       &[]*DomainEvent{},
	}
}

func (w *Workflow) AddEvent(event *DomainEvent) {
	*w.events = append(*w.events, event)
}
func (w *Workflow) GetEvents() []*DomainEvent {
	return *w.events
}

func (w *Workflow) Copy() *Workflow {

	return &Workflow{
		Context:      w.Context,
		TenantID:    w.TenantID,
		Locale:       w.Locale,
		JWT:          w.JWT,
		JWTDecrypted: w.JWTDecrypted,
		Tracer:       w.Tracer,
		events:       w.events,
	}
}

func (w *Workflow) WithContext(ctx context.Context) *Workflow {

	workflow := w.Copy()
	workflow.Context = ctx

	return workflow
}

func (w *Workflow) WithTenantID(tenantID TenantID) *Workflow {

	workflow := w.Copy()
	workflow.TenantID = tenantID

	return workflow
}

type EventOld struct {
	ID          string
	Aggregate   interface{}
	AggregateID string
	Name        string
	Payload     map[string]interface{}
}

type ApplicationContext interface {
	Workflow() *Workflow
	Commit(error) error
	DomainEventPublisherInstance() *DomainEventPublisherInstance
	Sudo() ApplicationContext
	IsSudo() bool
}

type JWTClaims struct {
	*jwt.StandardClaims
	TenantID  TenantID
	UserID    string
	UserEmail string
	Groups    map[string]bool
}

func (j *JWTClaims) HasGroup(groups []string) bool {
	for _, group := range groups {
		if _, ok := j.Groups[group]; ok {
			return true
		}
	}
	return false
}

type TenantID IDType
