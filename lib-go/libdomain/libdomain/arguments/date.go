package arguments

import (
	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain"
	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain/properties"
)

const DateArgumentType = libdomain.ArgumentType("DateType")


type DateDefinition struct {
	*properties.DateDefinition
}

func Date(name string, required bool, options *properties.DateOptions) *DateDefinition {
	return &DateDefinition{
		DateDefinition: properties.Date(name, required, 0, 0, options),
	}
}

// Type return the type of the field.
func (f *DateDefinition) Type() libdomain.ArgumentType {
	return DateArgumentType
}

func (f *DateDefinition) GetProperty() libdomain.PropertyDefinition {
	return f.DateDefinition
}