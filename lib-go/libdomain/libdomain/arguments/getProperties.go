package arguments

import (
	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain"
	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain/properties"
)

const GetPropertiesArgumentType = libdomain.ArgumentType("GetPropertiesType")


type GetPropertiesDefinition struct {
	*properties.GetPropertiesDefinition
}

func GetProperties(referenceName string, referenceDefinition libdomain.TableInterface) *GetPropertiesDefinition {
	return &GetPropertiesDefinition{
		GetPropertiesDefinition: properties.GetProperties(referenceName, referenceDefinition),
	}
}

// Type return the type of the field.
func (f *GetPropertiesDefinition) Type() libdomain.ArgumentType {
	return GetPropertiesArgumentType
}

func (f *GetPropertiesDefinition) GetProperty() libdomain.PropertyDefinition {
	return f.GetPropertiesDefinition
}