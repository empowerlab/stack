package arguments

import (
	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain"
	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain/properties"
)

const BinaryArgumentType = libdomain.ArgumentType("BinaryType")


type BinaryDefinition struct {
	*properties.BinaryDefinition
}

func Binary(name string, required bool, options *properties.BinaryOptions) *BinaryDefinition {
	return &BinaryDefinition{
		BinaryDefinition: properties.Binary(name, required, 0, 0, options),
	}
}

// Type return the type of the field.
func (f *BinaryDefinition) Type() libdomain.ArgumentType {
	return BinaryArgumentType
}

func (f *BinaryDefinition) GetProperty() libdomain.PropertyDefinition {
	return f.BinaryDefinition
}