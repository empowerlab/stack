package arguments

import (
	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain"
	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain/properties"
)

const SelectionArgumentType = libdomain.ArgumentType("SelectionType")


type SelectionDefinition struct {
	*properties.SelectionDefinition
}

func Selection(name string, required bool, enumName string, enumDefinition *libdomain.EnumDefinition, options *properties.SelectionOptions) *SelectionDefinition {
	return &SelectionDefinition{
		SelectionDefinition: properties.Selection(name, required, enumName, enumDefinition, 0, 0, options),
	}
}

// Type return the type of the field.
func (f *SelectionDefinition) Type() libdomain.ArgumentType {
	return SelectionArgumentType
}

func (f *SelectionDefinition) GetProperty() libdomain.PropertyDefinition {
	return f.SelectionDefinition
}