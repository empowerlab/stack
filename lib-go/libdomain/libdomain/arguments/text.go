package arguments

import (
	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain"
	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain/properties"
)

const TextArgumentType = libdomain.ArgumentType("TextType")


type TextDefinition struct {
	*properties.TextDefinition
}

func Text(name string, required bool, options *properties.TextOptions) *TextDefinition {
	return &TextDefinition{
		TextDefinition: properties.Text(name, required, 0, 0, options),
	}
}
func TextTranslatable(name string, required bool, options *properties.TextOptions) *TextDefinition {
	return &TextDefinition{
		TextDefinition: properties.TextTranslatable(name, required, 0, 0, options),
	}
}
func TextPatch(name string, required bool, options *properties.TextOptions) *TextDefinition {
	return &TextDefinition{
		TextDefinition: properties.TextPatch(name, required, 0, 0, options),
	}
}
func TextWYSIWYG(name string, required bool, patch bool, options *properties.TextOptions) *TextDefinition {
	return &TextDefinition{
		TextDefinition: properties.TextWYSIWYG(name, required, patch, 0, 0, 0, 0, options),
	}
}

// Type return the type of the field.
func (f *TextDefinition) Type() libdomain.ArgumentType {
	return TextArgumentType
}

func (f *TextDefinition) GetProperty() libdomain.PropertyDefinition {
	return f.TextDefinition
}