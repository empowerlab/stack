package arguments

import (
	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain"
	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain/properties"
)

const ValueObjectArgumentType = libdomain.ArgumentType("ValueObjectType")

const ValueObjectCreateInputType libdomain.ObjectInputType = "CreateInput"
const ValueObjectUpdateInputType libdomain.ObjectInputType = "UpdateInput"

type ValueObjectDefinition struct {
	*properties.ValueObjectDefinition
	inputType libdomain.ObjectInputType 
}

func ValueObject(name string, required bool, referenceName string, referenceDefinition libdomain.TableInterface, options *properties.ValueObjectOptions) *ValueObjectDefinition {
	return &ValueObjectDefinition{
		ValueObjectDefinition: properties.ValueObject(name, required, referenceName, referenceDefinition, options),
	}
}
func ValueObjectCreateInput(name string, required bool, referenceName string, referenceDefinition libdomain.TableInterface, options *properties.ValueObjectOptions) *ValueObjectDefinition {
	return &ValueObjectDefinition{
		ValueObjectDefinition: properties.ValueObject(name, required, referenceName, referenceDefinition, options),
		inputType: ValueObjectCreateInputType,
	}
}
func ValueObjectUpdateInput(name string, required bool, referenceName string, referenceDefinition libdomain.TableInterface, options *properties.ValueObjectOptions) *ValueObjectDefinition {
	return &ValueObjectDefinition{
		ValueObjectDefinition: properties.ValueObject(name, required, referenceName, referenceDefinition, options),
		inputType: ValueObjectUpdateInputType,
	}
}

// Type return the type of the field.
func (f *ValueObjectDefinition) Type() libdomain.ArgumentType {
	return ValueObjectArgumentType
}

func (f *ValueObjectDefinition) GetProperty() libdomain.PropertyDefinition {
	return f.ValueObjectDefinition
}

func (f *ValueObjectDefinition) InputType() libdomain.ObjectInputType {
	return f.inputType
}