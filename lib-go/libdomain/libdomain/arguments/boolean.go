package arguments

import (
	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain"
	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain/properties"
)

const BooleanArgumentType = libdomain.ArgumentType("BooleanType")


type BooleanDefinition struct {
	*properties.BooleanDefinition
}

func Boolean(name string, required bool, options *properties.BooleanOptions) *BooleanDefinition {
	return &BooleanDefinition{
		BooleanDefinition: properties.Boolean(name, required, 0, 0, options),
	}
}

// Type return the type of the field.
func (f *BooleanDefinition) Type() libdomain.ArgumentType {
	return BooleanArgumentType
}

func (f *BooleanDefinition) GetProperty() libdomain.PropertyDefinition {
	return f.BooleanDefinition
}