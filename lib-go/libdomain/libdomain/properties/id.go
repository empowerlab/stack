package properties

import (
	// "strings"

	"fmt"
	"strings"
	"unicode"

	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain"
)

// IDPropertyType contains the field type for ID
const IDPropertyType = libdomain.PropertyType("IDType")

/*ID is the field type you can use in definition to declare an ID field.
 */
type IDDefinition struct {
	Common	
	Reference              string
	ReferenceDefinition    libdomain.TableInterface
	ReferencePrefix        string
	ReferencePath          string
	OnDelete               libdomain.OnDeleteValue
	WithEntity 				bool
	LoadedPositionEntity      int
	PositionEntity            int
	Options *IDOptions
}
type IDOptions struct {
	Unique bool
	PrimaryKey bool	
	ReferencePrefix        string
	ReferencePath          string
	NotInInput bool
}

func ID(name string, required bool, referenceName string, referenceDefinition libdomain.TableInterface, onDelete libdomain.OnDeleteValue, position int, loadedPosition int, options *IDOptions) *IDDefinition {

	if !unicode.IsLower(rune(name[0])) {
		panic(fmt.Sprintf("Binary field name %s must start with a lowercase character", name))
	}

	if referenceDefinition != nil {
		if referenceName != "" {
			panic(fmt.Sprintf("You can't set a reference name and a reference definition: %s", name))
		}
		referenceName = referenceDefinition.GetName()
	}

	result := &IDDefinition{
		Common: Common{
			Name:           name,
			Required:       required,
			Position: 	 position,
			LoadedPosition: loadedPosition,
		},
		Reference: referenceName,
		ReferenceDefinition: referenceDefinition,	
		OnDelete: onDelete,
		PositionEntity: position,
		LoadedPositionEntity: loadedPosition,
		Options: options,
	}
	if options != nil {
		result.Common.Unique = options.Unique
		result.Common.PrimaryKey = options.PrimaryKey
		result.ReferencePrefix = options.ReferencePrefix
		result.ReferencePath = options.ReferencePath
	}

	return result
}
func IDWithEntity(name string, required bool, referenceName string, referenceDefinition libdomain.TableInterface, onDelete libdomain.OnDeleteValue, position int, loadedPosition int, positionEntity int, loadedPositionEntity int, options *IDOptions) *IDDefinition {

	result := ID(name, required, referenceName, referenceDefinition, onDelete, position, loadedPosition, options)
	result.WithEntity = true
	result.PositionEntity = positionEntity
	result.LoadedPositionEntity = loadedPositionEntity
	return result
}

// Title return the title of the field.
func (f *IDDefinition) NameWithoutID() string {
	return strings.Replace(strings.Replace(f.Name, "ID", "", -1), "Ids", "s", -1)
}

// Title return the title of the field.
func (f *IDDefinition) TitleWithoutID() string {
	return strings.Replace(strings.Replace(f.Title(), "ID", "", -1), "Ids", "s", -1)
}

// Type return the type of the field.
func (f *IDDefinition) Type() libdomain.PropertyType {
	return IDPropertyType
}

// Type return the type of the field.
func (f *IDDefinition) GoType() string {
	return f.ReferenceDefinition.DomainPackage() + "identities." + f.ReferenceDefinition.Title() + "ID"
}

// Type return the type of the field.
func (f *IDDefinition) GoTypeWithPackage() string {
	return f.GoType()
}

func (f *IDDefinition) GoTypeBase() string {
	return "string"
}

// Type return the type of the field.
func (f *IDDefinition) GoTypeID() string {
	return f.GoType()
}

// Type return the type of the field.
func (f *IDDefinition) GoTypeIDWithPackage() string {
	return f.GoTypeID()
}

// Type return the type of the field.
func (f *IDDefinition) GoNil() string {
	return "\"\""
}

// Type return the type of the field.
func (f *IDDefinition) JSONType() string {
	return f.GoType()
}

// ProtoType return the protobuf type for this field.
func (f *IDDefinition) ProtoType() string {
	return "string"
}

func (f *IDDefinition) ProtoTypeArg() string {
	return f.ProtoType()
}

// ProtoType return the protobuf type for this field.
func (f *IDDefinition) ProtoTypeOptional() string {
	return f.ProtoType()
}

// ProtoType return the protobuf type for this field.
func (f *IDDefinition) DBType() *libdomain.DBType {
	return &libdomain.DBType{
		Type:  "sql.NullString",
		Value: "String",
	}
}

// Type return the type of the field.
func (f *IDDefinition) DiffType(pack string) string {
	return "*" + f.GoType() + "Diff"
}

// Type return the type of the field.
func (f *IDDefinition) DiffTypeNew(pack string) string {
	return f.ReferenceDefinition.DomainPackage() + "identities.New" + f.ReferenceDefinition.Title() + "IDDiff"
}

// Type return the type of the field.
func (f *IDDefinition) GraphqlType() string {
	return "graphql.ID"
}

// Type return the type of the field.
func (f *IDDefinition) GraphqlSchemaType() string {
	return "ID"
}

// GetReference return the name of referenced model, if this field is linked to another model.
func (f *IDDefinition) GetReferenceName() string {
	return f.Reference
}

// GetReferenceDefinition return the definition of referenced model,
// if this field is linked to another model.
func (f *IDDefinition) GetReference() libdomain.TableInterface {
	return f.ReferenceDefinition
}

// SetReferenceDefinition set the definition of referenced model,
// if this field is linked to another model.
func (f *IDDefinition) SetReference(d libdomain.TableInterface) {
	f.ReferenceDefinition = d
}

func (f *IDDefinition) GetWithEntity() bool {
	return f.WithEntity
}

func (r *IDDefinition) GetLoadedPositionEntity() int {
	return r.LoadedPositionEntity
}
func (r *IDDefinition) GetPositionEntity() int {
	return r.PositionEntity
}
func (r *IDDefinition) GetOptions() interface{} {
	return r.Options
}