package properties

import (
	"fmt"
	"unicode"

	"github.com/iancoleman/strcase"

	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain"
)

func init() {
	libdomain.SetAggregateIDProperty = func(referenceName string) libdomain.PropertyDefinition {
		return Entity(
			"aggregateID", true, referenceName, nil, 3, 4, &EntityOptions{
				PrimaryKey: true, OnDelete: libdomain.OnDeleteCascade})
	}
}

// EntityPropertyType contains the field type for Entity
const EntityPropertyType = libdomain.PropertyType("EntityType")

/*Entity is the field type you can use in definition to declare a Entity field.
 */
type EntityDefinition struct {
	Common	
	Reference              string
	ReferenceDefinition    libdomain.TableInterface
	Input                   bool
	ReferencePrefix        string
	ReferencePath          string
	OnDelete               libdomain.OnDeleteValue
	ReturnDetailsInTests   bool
	FromID bool
}
type EntityOptions struct {
	Unique bool
	PrimaryKey bool	
	ReferencePrefix        string
	ReferencePath          string
	OnDelete               libdomain.OnDeleteValue
	ReturnDetailsInTests   bool
	FromID bool
}

func Entity(name string, required bool, referenceName string, referenceDefinition libdomain.TableInterface, position int, loadedPosition int, options *EntityOptions) *EntityDefinition {

	if !unicode.IsLower(rune(name[0])) {
		panic(fmt.Sprintf("Binary field name %s must start with a lowercase character", name))
	}

	if referenceDefinition != nil {
		if referenceName != "" {
			panic(fmt.Sprintf("You can't set a reference name and a reference definition: %s", name))
		}
		referenceName = referenceDefinition.GetName()
	}

	result := &EntityDefinition{
		Common: Common{
			Name:           name,
			Required:       required,
			Position: 	 position,
			LoadedPosition: loadedPosition,
		},
		Reference: referenceName,
		ReferenceDefinition: referenceDefinition,	
	}
	if options != nil {
		result.Common.Unique = options.Unique
		result.Common.PrimaryKey = options.PrimaryKey
		result.ReferencePrefix = options.ReferencePrefix
		result.ReferencePath = options.ReferencePath
		result.OnDelete = options.OnDelete
		result.ReturnDetailsInTests = options.ReturnDetailsInTests
		result.FromID = options.FromID
	}

	return result
}
// func EntityInput(name string, required bool, referenceName string, referenceDefinition libdomain.TableInterface, position int, loadedPosition int, options *EntityOptions) *EntityDefinition {
// 	result :=  Entity(name, required, referenceName, referenceDefinition,  position, loadedPosition, options)
// 	result.Input = true
// 	return result
// }

// Type return the type of the field.
func (f *EntityDefinition) Type() libdomain.PropertyType {
	return EntityPropertyType
}

// Type return the type of the field.
func (f *EntityDefinition) GoType() string {
	data := ""
	// if f.Input {
	// 	data = "Input"
	// }
	return "*" + strcase.ToCamel(f.Reference) + data
	// return libdomain.STRING
}

// Type return the type of the field.
func (f *EntityDefinition) GoTypeWithPackage() string {
	data := ""
	// if f.Input {
	// 	data = "Input"
	// }
	return "*" + f.ReferenceDefinition.DomainPackage() + "." + strcase.ToCamel(f.Reference) + data
}

func (f *EntityDefinition) GoTypeBase() string {
	return f.GoType()
}

// Type return the type of the field.
func (f *EntityDefinition) GoTypeID() string {
	return f.ReferenceDefinition.DomainPackage() + "identities." + f.ReferenceDefinition.Title() + "ID"
	// return libdomain.STRING
}

// Type return the type of the field.
func (f *EntityDefinition) GoTypeIDWithPackage() string {
	return f.GoTypeID()
}

// Type return the type of the field.
func (f *EntityDefinition) GoTypeWithPath() string {
	prefix := ""
	if f.ReferencePrefix != "" {
		prefix = f.ReferencePrefix + "."
	}
	return prefix + strcase.ToCamel(f.Reference)
	// return libdomain.STRING
}

// Type return the type of the field.
func (f *EntityDefinition) GoNil() string {
	return "nil"
}

// Type return the type of the field.
func (f *EntityDefinition) JSONType() string {
	return libdomain.STRING
}

// ProtoType return the protobuf type for this field.
func (f *EntityDefinition) ProtoType() string {
	return strcase.ToCamel(f.Reference)
}

func (f *EntityDefinition) ProtoTypeArg() string {
	return f.ProtoType()
}

// ProtoType return the protobuf type for this field.
func (f *EntityDefinition) ProtoTypeOptional() string {
	return f.ProtoType()
}

// ProtoType return the protobuf type for this field.
func (f *EntityDefinition) ProtoTypeOptionalArg() string {
	return "string"
}

// ProtoType return the protobuf type for this field.
func (f *EntityDefinition) DBType() *libdomain.DBType {
	return &libdomain.DBType{
		Type:  "sql.NullString",
		Value: "String",
	}
}

// Type return the type of the field.
func (f *EntityDefinition) DiffType(pack string) string {
	return f.GoTypeWithPackage() + "Diff"
}

// Type return the type of the field.
func (f *EntityDefinition) DiffTypeNew(pack string) string {
	return f.ReferenceDefinition.DomainPackage() + ".New" + strcase.ToCamel(f.Reference) + "Diff"
}

// Type return the type of the field.
func (f *EntityDefinition) GraphqlType() string {
	return "graphql.ID"
}

// Type return the type of the field.
func (f *EntityDefinition) GraphqlSchemaType() string {
	return "ID"
}

// GetReference return the name of referenced model, if this field is linked to another model.
func (f *EntityDefinition) GetReferenceName() string {
	return f.Reference
}

// GetReferenceDefinition return the definition of referenced model,
// if this field is linked to another model.
func (f *EntityDefinition) GetReference() libdomain.TableInterface {
	return f.ReferenceDefinition
}

// SetReferenceDefinition set the definition of referenced model,
// if this field is linked to another model.
func (f *EntityDefinition) SetReference(d libdomain.TableInterface) {
	f.ReferenceDefinition = d
}

func (r *EntityDefinition) GetInput() bool {
	return r.Input
}

func (r *EntityDefinition) GetReturnDetailsInTests() bool {
	return r.ReturnDetailsInTests
}
