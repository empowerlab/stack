package properties

import (
	// "strings"

	"strings"

	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain"
)

// TextPropertyType contains the field type for Text
const GetPropertiesPropertyType = libdomain.PropertyType("GetPropertiesType")

/*Text is the field type you can use in definition to declare a Text field.
 */
type GetPropertiesDefinition struct {
	Common	
	Reference              string
	ReferenceDefinition    libdomain.TableInterface
	Data                   bool
	ReferencePrefix        string
	ReferencePath          string
	OnDelete               libdomain.OnDeleteValue
	ReturnDetailsInTests   bool
	FromID bool
}
type GetPropertiesOptions struct {
	Unique bool
	PrimaryKey bool	
	ReferencePrefix        string
	ReferencePath          string
	OnDelete               libdomain.OnDeleteValue
	ReturnDetailsInTests   bool
	FromID bool
}

func GetProperties(referenceName string, referenceDefinition libdomain.TableInterface) *GetPropertiesDefinition {

	if referenceDefinition != nil {
		referenceName = referenceDefinition.GetName()
	}

	result := &GetPropertiesDefinition{
		Common: Common{
			Name: "get" + strings.Title(referenceName) + "Properties",
		},
		Reference: referenceName,
		ReferenceDefinition: referenceDefinition,	
	}

	return result
}

// Type return the type of the field.
func (f *GetPropertiesDefinition) Type() libdomain.PropertyType {
	return GetPropertiesPropertyType
}

// Type return the type of the field.
func (f *GetPropertiesDefinition) GoType() string {
	return "*" + f.Title()
}

// Type return the type of the field.
func (f *GetPropertiesDefinition) GoTypeWithPackage() string {
	return "*" + f.ReferenceDefinition.DomainPackage() + "." + f.Title()
}

// Type return the type of the field.
func (f *GetPropertiesDefinition) GoTypeID() string {
	return f.GoType()
}

// Type return the type of the field.
func (f *GetPropertiesDefinition) GoTypeIDWithPackage() string {
	return "*" + f.ReferenceDefinition.DomainPackage() + "." + f.Title()
}

func (f *GetPropertiesDefinition) GoTypeBase() string {
	return f.GoType()
}

// Type return the type of the field.
func (f *GetPropertiesDefinition) GoNil() string {
	return "\"\""
}

// Type return the type of the field.
func (f *GetPropertiesDefinition) JSONType() string {
	return f.GoType()
}

// ProtoType return the protobuf type for this field.
func (f *GetPropertiesDefinition) ProtoType() string {
	return f.GoType()
}

func (f *GetPropertiesDefinition) ProtoTypeArg() string {
	return f.ProtoType()
}

// ProtoType return the protobuf type for this field.
func (f *GetPropertiesDefinition) ProtoTypeOptional() string {
	return f.ProtoType()
}

// ProtoType return the protobuf type for this field.
func (f *GetPropertiesDefinition) DBType() *libdomain.DBType {
	return &libdomain.DBType{
		Type:  "sql.NullString",
		Value: "String",
	}
}

// Type return the type of the field.
func (f *GetPropertiesDefinition) DiffType(pack string) string {
	return "*libdomain.TextDiff"
}

// Type return the type of the field.
func (f *GetPropertiesDefinition) DiffTypeNew(pack string) string {
	return "libdomain.NewTextDiff"
}

// Type return the type of the field.
func (f *GetPropertiesDefinition) GraphqlType() string {
	return f.GoType()
}

// Type return the type of the field.
func (f *GetPropertiesDefinition) GraphqlSchemaType() string {
	return "String"
}

// GetReference return the name of referenced model, if this field is linked to another model.
func (f *GetPropertiesDefinition) GetReferenceName() string {
	return f.Reference
}

// GetReferenceDefinition return the definition of referenced model,
// if this field is linked to another model.
func (f *GetPropertiesDefinition) GetReference() libdomain.TableInterface {
	return f.ReferenceDefinition
}

// SetReferenceDefinition set the definition of referenced model,
// if this field is linked to another model.
func (f *GetPropertiesDefinition) SetReference(entity libdomain.TableInterface) {
	f.ReferenceDefinition = entity
}
