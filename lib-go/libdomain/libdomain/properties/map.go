package properties

import (
	// "strings"

	"fmt"
	"unicode"

	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain"
)

// MapPropertyType contains the field type for Map
const MapPropertyType = libdomain.PropertyType("MapType")

/*Map is the field type you can use in definition to declare a Map field.
 */
type MapDefinition struct {
	Common
}
type MapOptions struct {
	Unique bool
	PrimaryKey bool
}

func Map(name string, required bool, position int, loadedPosition int, options *MapOptions) *MapDefinition {

	if !unicode.IsLower(rune(name[0])) {
		panic(fmt.Sprintf("Binary field name %s must start with a lowercase character", name))
	}

	common := Common{
		Name:           name,
		Required:       required,
		Position: 	 position,
		LoadedPosition: loadedPosition,
	}
	if options != nil {
		common.Unique = options.Unique
		common.PrimaryKey = options.PrimaryKey
	}

	return &MapDefinition{
		Common: common,
	}
}

// Type return the type of the field.
func (f *MapDefinition) Type() libdomain.PropertyType {
	return MapPropertyType
}

// Type return the type of the field.
func (f *MapDefinition) GoType() string {
	return "map[string]interface{}"
}

// Type return the type of the field.
func (f *MapDefinition) GoTypeWithPackage() string {
	return f.GoType()
}

// Type return the type of the field.
func (f *MapDefinition) GoTypeID() string {
	return f.GoType()
}

// Type return the type of the field.
func (f *MapDefinition) GoTypeIDWithPackage() string {
	return f.GoTypeID()
}

func (f *MapDefinition) GoTypeBase() string {
	return f.GoType()
}

// Type return the type of the field.
func (f *MapDefinition) GoNil() string {
	return "nil"
}

// Type return the type of the field.
func (f *MapDefinition) JSONType() string {
	return f.GoType()
}

// ProtoType return the protobuf type for this field.
func (f *MapDefinition) ProtoType() string {
	return "bytes"
}

func (f *MapDefinition) ProtoTypeArg() string {
	return f.ProtoType()
}

// ProtoType return the protobuf type for this field.
func (f *MapDefinition) ProtoTypeOptional() string {
	return f.ProtoType()
}

// ProtoType return the protobuf type for this field.
func (f *MapDefinition) DBType() *libdomain.DBType {
	return &libdomain.DBType{
		Type:  "sql.NullString",
		Value: "String",
	}
}

// Type return the type of the field.
func (f *MapDefinition) DiffType(pack string) string {
	return "*libdomain.TextDiff"
}

// Type return the type of the field.
func (f *MapDefinition) DiffTypeNew(pack string) string {
	return "libdomain.NewTextDiff"
}

// Type return the type of the field.
func (f *MapDefinition) GraphqlType() string {
	return f.GoType()
}

// Type return the type of the field.
func (f *MapDefinition) GraphqlSchemaType() string {
	return "String"
}
