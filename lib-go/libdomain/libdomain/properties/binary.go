package properties

import (
	// "strings"

	"fmt"
	"unicode"

	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain"
)

// JSONPropertyType contains the field type for JSON
const BinaryPropertyType = libdomain.PropertyType("BinaryType")

/*JSON is the field type you can use in definition to declare a JSON field.
 */
type BinaryDefinition struct {
	Common
}
type BinaryOptions struct {
	Unique bool
	PrimaryKey bool
}

func Binary(name string, required bool, position int, loadedPosition int, options *BinaryOptions) *BinaryDefinition {

	if !unicode.IsLower(rune(name[0])) {
		panic(fmt.Sprintf("Binary field name %s must start with a lowercase character", name))
	}

	common := Common{
		Name:           name,
		Required:       required,
		Position: 	 position,
		LoadedPosition: loadedPosition,
	}
	if options != nil {
		common.Unique = options.Unique
		common.PrimaryKey = options.PrimaryKey
	}

	return &BinaryDefinition{
		Common: common,
	}
}

// Type return the type of the field.
func (f *BinaryDefinition) Type() libdomain.PropertyType {
	return BinaryPropertyType
}

// Type return the type of the field.
func (f *BinaryDefinition) GoType() string {
	return "*libdomain.File"
}

// Type return the type of the field.
func (f *BinaryDefinition) GoTypeWithPackage() string {
	return f.GoType()
}

func (f *BinaryDefinition) GoTypeBase() string {
	return f.GoType()
}

// Type return the type of the field.
func (f *BinaryDefinition) GoTypeID() string {
	return f.GoType()
}

// Type return the type of the field.
func (f *BinaryDefinition) GoTypeIDWithPackage() string {
	return f.GoTypeID()
}

// Type return the type of the field.
func (f *BinaryDefinition) GoNil() string {
	return "nil"
}

// Type return the type of the field.
func (f *BinaryDefinition) JSONType() string {
	return f.GoType()
}

// ProtoType return the protobuf type for this field.
func (f *BinaryDefinition) ProtoType() string {
	return "google.protobuf.File"
}

func (f *BinaryDefinition) ProtoTypeArg() string {
	return f.ProtoType()
}

// ProtoType return the protobuf type for this field.
func (f *BinaryDefinition) ProtoTypeOptional() string {
	return f.ProtoType()
}

// ProtoType return the protobuf type for this field.
func (f *BinaryDefinition) DBType() *libdomain.DBType {
	return &libdomain.DBType{
		Type:  "sql.NullString",
		Value: "String",
	}
}

// Type return the type of the field.
func (f *BinaryDefinition) DiffType(pack string) string {
	return "*libdomain.TextDiff"
}

// Type return the type of the field.
func (f *BinaryDefinition) DiffTypeNew(pack string) string {
	return "libdomain.NewTextDiff"
}

// Type return the type of the field.
func (f *BinaryDefinition) GraphqlType() string {
	return "graphql.Upload"
}

// Type return the type of the field.
func (f *BinaryDefinition) GraphqlSchemaType() string {
	return "Upload"
}
