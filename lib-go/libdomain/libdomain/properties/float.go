package properties

import (
	// "strings"

	"fmt"
	"unicode"

	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain"
)

// FloatPropertyType contains the field type for Float
const FloatPropertyType = libdomain.PropertyType("FloatType")

/*Float is the field type you can use in definition to declare a Float field.
 */
type FloatDefinition struct {
	Common
}
type FloatOptions struct {
	Unique bool
	PrimaryKey bool
}

func Float(name string, required bool, position int, loadedPosition int, options *FloatOptions) *FloatDefinition {

	if !unicode.IsLower(rune(name[0])) {
		panic(fmt.Sprintf("Binary field name %s must start with a lowercase character", name))
	}

	common := Common{
		Name:           name,
		Required:       required,
		Position: 	 position,
		LoadedPosition: loadedPosition,
	}
	if options != nil {
		common.Unique = options.Unique
		common.PrimaryKey = options.PrimaryKey
	}

	return &FloatDefinition{
		Common: common,
	}
}

// Type return the type of the field.
func (f *FloatDefinition) Type() libdomain.PropertyType {
	return FloatPropertyType
}

// Type return the type of the field.
func (f *FloatDefinition) GoType() string {
	return "float64"
}

// Type return the type of the field.
func (f *FloatDefinition) GoTypeWithPackage() string {
	return f.GoType()
}

// Type return the type of the field.
func (f *FloatDefinition) GoTypeID() string {
	return f.GoType()
}

// Type return the type of the field.
func (f *FloatDefinition) GoTypeIDWithPackage() string {
	return f.GoTypeID()
}

func (f *FloatDefinition) GoTypeBase() string {
	return f.GoType()
}

// Type return the type of the field.
func (f *FloatDefinition) GoNil() string {
	return "0.0"
}

// Type return the type of the field.
func (f *FloatDefinition) JSONType() string {
	return f.GoType()
}

// ProtoType return the protobuf type for this field.
func (f *FloatDefinition) ProtoType() string {
	return "double"
}

func (f *FloatDefinition) ProtoTypeArg() string {
	return f.ProtoType()
}

// ProtoType return the protobuf type for this field.
func (f *FloatDefinition) ProtoTypeOptional() string {
	return f.ProtoType()
}

// ProtoType return the protobuf type for this field.
func (f *FloatDefinition) DBType() *libdomain.DBType {
	return &libdomain.DBType{
		Type:  "sql.NullFloat64",
		Value: "Float64",
	}
}

// Type return the type of the field.
func (f *FloatDefinition) DiffType(pack string) string {
	return "*libdomain.FloatDiff"
}

// Type return the type of the field.
func (f *FloatDefinition) DiffTypeNew(pack string) string {
	return "libdomain.NewFloatDiff"
}

// Type return the type of the field.
func (f *FloatDefinition) GraphqlType() string {
	return f.GoType()
}

// Type return the type of the field.
func (f *FloatDefinition) GraphqlSchemaType() string {
	return "Float"
}
