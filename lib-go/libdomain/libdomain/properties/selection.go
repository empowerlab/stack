package properties

import (
	// "strings"

	"fmt"
	"strings"
	"unicode"

	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain"
)

// TextPropertyType contains the field type for Text
const SelectionPropertyType = libdomain.PropertyType("SelectionType")

/*Text is the field type you can use in definition to declare a Text field.
 */
type SelectionDefinition struct {
	Common
	Enum           string
	EnumDefinition *libdomain.EnumDefinition
}
type SelectionOptions struct {
	Unique bool
	PrimaryKey bool
}

func Selection(name string, required bool, enumName string, enumDefinition *libdomain.EnumDefinition, position int, loadedPosition int, options *SelectionOptions) *SelectionDefinition {
	
	if !unicode.IsLower(rune(name[0])) {
		panic(fmt.Sprintf("Selection field name %s must start with a lowercase character", name))
	}

	if enumDefinition != nil {
		if enumName != "" {
			panic(fmt.Sprintf("You can't set an enum name and an enum definition: %s", name))
		}
		enumName = enumDefinition.GetName()
	}

	common := Common{
		Name:           name,
		Required:       required,
		Position:       position,
		LoadedPosition: loadedPosition,
	}
	if options != nil {
		common.Unique = options.Unique
		common.PrimaryKey = options.PrimaryKey
	}

	return &SelectionDefinition{
		Common: common,
		Enum:   enumName,
		EnumDefinition: enumDefinition,
	}
}


// Type return the type of the field.
func (f *SelectionDefinition) Type() libdomain.PropertyType {
	return SelectionPropertyType
}

// Type return the type of the field.
func (f *SelectionDefinition) GoType() string {
	return libdomain.STRING
}

// Type return the type of the field.
func (f *SelectionDefinition) GoTypeWithPackage() string {
	return f.EnumDefinition.DomainPackage() + "." + f.EnumDefinition.Title() + "Type"
}

// Type return the type of the field.
func (f *SelectionDefinition) GoTypeID() string {
	return f.GoType()
}

// Type return the type of the field.
func (f *SelectionDefinition) GoTypeIDWithPackage() string {
	return f.GoTypeID()
}

func (f *SelectionDefinition) GoTypeBase() string {
	return f.GoType()
}

// Type return the type of the field.
func (f *SelectionDefinition) GoNil() string {
	return "\"\""
}

// Type return the type of the field.
func (f *SelectionDefinition) JSONType() string {
	return f.GoType()
}

// ProtoType return the protobuf type for this field.
func (f *SelectionDefinition) ProtoType() string {
	return f.GoType()
}

func (f *SelectionDefinition) ProtoTypeArg() string {
	return f.ProtoType()
}

// ProtoType return the protobuf type for this field.
func (f *SelectionDefinition) ProtoTypeOptional() string {
	return f.ProtoType()
}

// ProtoType return the protobuf type for this field.
func (f *SelectionDefinition) DBType() *libdomain.DBType {
	return &libdomain.DBType{
		Type:  "sql.NullString",
		Value: "String",
	}
}

// Type return the type of the field.
func (f *SelectionDefinition) DiffType(pack string) string {
	if f.EnumDefinition.DomainPackage() != pack {
		return "*" + f.EnumDefinition.DomainPackage() + "." + f.EnumDefinition.Title() + "TypeDiff"
	}
	return "*" + f.EnumDefinition.Title() + "TypeDiff"

}

// Type return the type of the field.
func (f *SelectionDefinition) DiffTypeNew(pack string) string {
	if f.EnumDefinition.DomainPackage() != pack {
		return f.EnumDefinition.DomainPackage() + ".New" + f.EnumDefinition.Title() + "TypeDiff"
	}
	return "New" + f.EnumDefinition.Title() + "TypeDiff"
}

// Type return the type of the field.
func (f *SelectionDefinition) GraphqlType() string {
	return f.GoType()
}

// Type return the type of the field.
func (f *SelectionDefinition) GraphqlSchemaType() string {
	return "String"
}

func (f *SelectionDefinition) EnumTitle() string {
	return strings.Title(f.Enum)
}

// GetReference return the name of referenced model, if this field is linked to another model.
func (f *SelectionDefinition) GetReferenceName() string {
	return f.Enum
}

// GetReferenceDefinition return the definition of referenced model,
// if this field is linked to another model.
func (f *SelectionDefinition) GetReference() libdomain.TableInterface {
	if f.EnumDefinition != nil {
		return f.EnumDefinition
	}
	return nil
}

// SetReferenceDefinition set the definition of referenced model,
// if this field is linked to another model.
func (f *SelectionDefinition) SetReference(ref libdomain.TableInterface) {
	f.EnumDefinition = ref.(*libdomain.EnumDefinition)
}
