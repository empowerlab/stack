package properties

import (
	"fmt"
	"unicode"

	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain"
)

// TextPropertyType contains the field type for Text
const TextPropertyType = libdomain.PropertyType("TextType")


/*Text is the field type you can use in definition to declare a Text field.
 */
type TextDefinition struct {
	Common
	Translatable        libdomain.TranslatableType
	TranslatableOptions *TranslatableOptions
	Patch               bool
	PositionTerms            int
	LoadedPositionTerms      int
}
type TextOptions struct {
	Unique bool
	PrimaryKey bool
}

func Text(name string, required bool, position int, loadedPosition int, options *TextOptions) *TextDefinition {

	if !unicode.IsLower(rune(name[0])) {
		panic(fmt.Sprintf("Text field name %s must start with a lowercase character", name))
	}

	common := Common{
		Name:           name,
		Required:       required,
		Position: 	 position,
		LoadedPosition: loadedPosition,
	}
	if options != nil {
		common.Unique = options.Unique
		common.PrimaryKey = options.PrimaryKey
	}

	return &TextDefinition{
		Common: common,
	}
}
func TextTranslatable(name string, required bool, position int, loadedPosition int, options *TextOptions) *TextDefinition {
	result := Text(name, required, position, loadedPosition, options)
	result.Translatable = TranslatableSimple
	return result
}
func TextPatch(name string, required bool, position int, loadedPosition int, options *TextOptions) *TextDefinition {
	result := Text(name, required, position, loadedPosition, options)
	result.Patch = true
	return result
}
func TextWYSIWYG(name string, required bool, patch bool, position int, loadedPosition int, positionTerms int, loadedPositionTerms int, options *TextOptions) *TextDefinition {
	result := Text(name, required, position, loadedPosition, options)
	result.Translatable = TranslatableWYSIWYG
	result.Patch = patch
	result.PositionTerms = positionTerms
	result.LoadedPositionTerms = loadedPositionTerms
	return result
}


type TranslatableOptions struct {
	LoadedPosition int
	Position       int
}

const TranslatableSimple libdomain.TranslatableType = "TranslatableSimple"
const TranslatableWYSIWYG libdomain.TranslatableType = "TranslatableWYSIWYG"

// Type return the type of the field.
func (f *TextDefinition) Type() libdomain.PropertyType {
	return TextPropertyType
}

// Type return the type of the field.
func (f *TextDefinition) GoType() string {
	if f.Translatable == TranslatableSimple {
		return "map[string]string"
	}
	return libdomain.STRING
}

// Type return the type of the field.
func (f *TextDefinition) GoTypeWithPackage() string {
	return f.GoType()
}

// Type return the type of the field.
func (f *TextDefinition) GoTypeID() string {
	return f.GoType()
}

// Type return the type of the field.
func (f *TextDefinition) GoTypeIDWithPackage() string {
	return f.GoTypeID()
}

func (f *TextDefinition) GoTypeBase() string {
	return f.GoType()
}

// Type return the type of the field.
func (f *TextDefinition) GoNil() string {
	if f.Translatable == TranslatableSimple {
		return "nil"
	}
	return "\"\""
}

// Type return the type of the field.
func (f *TextDefinition) JSONType() string {
	return f.GoType()
}

// ProtoType return the protobuf type for this field.
func (f *TextDefinition) ProtoType() string {
	return "string"
}

func (f *TextDefinition) ProtoTypeArg() string {
	return f.ProtoType()
}

// ProtoType return the protobuf type for this field.
func (f *TextDefinition) ProtoTypeOptional() string {
	return f.ProtoType()
}

// ProtoType return the protobuf type for this field.
func (f *TextDefinition) DBType() *libdomain.DBType {
	return &libdomain.DBType{
		Type:  "sql.NullString",
		Value: "String",
	}
}

// Type return the type of the field.
func (f *TextDefinition) DiffType(pack string) string {
	if f.Translatable == TranslatableSimple {
		return "map[string]*libdomain.TextDiff"
	}
	if f.Patch {
		return "*libdomain.TextPatchDiff"
	}
	return "*libdomain.TextDiff"
}

// Type return the type of the field.
func (f *TextDefinition) DiffTypeNew(pack string) string {
	return "libdomain.NewTextDiff"
}

// Type return the type of the field.
func (f *TextDefinition) GraphqlType() string {
	return f.GoType()
}

// Type return the type of the field.
func (f *TextDefinition) GraphqlSchemaType() string {
	return "String"
}

func (r *TextDefinition) IsTranslatable() bool {
	return r.Translatable == TranslatableSimple
}

func (r *TextDefinition) GetTranslatable() libdomain.TranslatableType {
	return r.Translatable
}

func (f *TextDefinition) GetPatch() bool {
	return f.Patch
}