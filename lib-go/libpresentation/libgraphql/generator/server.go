package generator

import (
	"bytes"
	"embed"
	"errors"
	"fmt"
	"go/format"
	"io/ioutil"
	"os"
	"strings"
	"text/template"

	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain"
	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain/arguments"
	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain/properties"
	"gitlab.com/empowerlab/stack/lib-go/libpresentation/libgraphql"
	"gitlab.com/empowerlab/stack/lib-go/libpresentation/libtranslator"
)

//go:embed *.tmpl
var fserver embed.FS

const graphqlID = "graphql.ID"

// Gen will generate the graphql files.
func Server(defs *libgraphql.Definitions) {

	var err error
	file, _ := fserver.ReadFile("schema.graphqls.tmpl")
	schemaTemplate := template.Must(schemaFuncs.Parse(string(file)))

	file, _ = fserver.ReadFile("gqlgen.yml.tmpl")
	gqlgenTemplate := template.Must(template.New("").Parse(string(file)))

	file, _ = fserver.ReadFile("init.go.tmpl")
	initTemplate := template.Must(template.New("").Parse(string(file)))

	file, _ = fserver.ReadFile("auth.go.tmpl")
	authTemplate := template.Must(template.New("").Parse(string(file)))

	file, _ = fserver.ReadFile("aggregate.go.tmpl")
	restTemplate := template.Must(restFuncs.Parse(string(file)))

	requestsFile, _ := fserver.ReadFile("service-requests.go.tmpl")
	requestsTemplate := template.Must(requestsFuncs.Parse(string(requestsFile)))

	err = os.MkdirAll("../gen", os.ModePerm)
	if err != nil {
		fmt.Println(err)
	}

	event := false
	for _, def := range defs.Slice() {
		if def.Event {
			event = true
		}
	}

	d := struct {
		Repository  string
		Title       string
		Definitions []*libgraphql.Definition
		Event       bool
		Auth        bool
	}{
		Repository:  defs.Repository,
		Title:       defs.GetTitle(),
		Definitions: defs.Slice(),
		Event:       event,
		Auth:        defs.Auth,
	}

	buf := &bytes.Buffer{}
	err = schemaTemplate.Execute(buf, d)
	if err != nil {
		fmt.Println(err)
	}
	err = ioutil.WriteFile(
		"../gen/schema.graphqls",
		buf.Bytes(), 0644)
	if err != nil {
		fmt.Println(err)
	}

	buf = &bytes.Buffer{}
	err = gqlgenTemplate.Execute(buf, d)
	if err != nil {
		fmt.Println(err)
	}
	err = ioutil.WriteFile(
		"../gen/gqlgen.yml",
		buf.Bytes(), 0644)
	if err != nil {
		fmt.Println(err)
	}

	// file := fmt.Sprintf("grpc/gen/%s.go", d.Name)
	buf = &bytes.Buffer{}
	err = initTemplate.Execute(buf, d)
	if err != nil {
		fmt.Println(err)
	}
	content, err := format.Source(buf.Bytes())
	if err != nil {
		fmt.Println(buf.String())
		fmt.Println("init rest ", err)
		content = buf.Bytes()
	}
	err = ioutil.WriteFile(
		"../gen/init.gen.go",
		content, 0644)
	if err != nil {
		fmt.Println(err)
	}

	if defs.Auth {
		buf := &bytes.Buffer{}
		err = authTemplate.Execute(buf, d)
		if err != nil {
			fmt.Println(err)
		}
		content, err := format.Source(buf.Bytes())
		if err != nil {
			fmt.Println(buf.String())
			fmt.Println("auth rest ", err)
			content = buf.Bytes()
		}
		err = ioutil.WriteFile(
			"../gen/auth.gen.go",
			content, 0644)
		if err != nil {
			fmt.Println(err)
		}
	}

	fmt.Println(defs.Slice())
	for _, definition := range defs.Slice() {

		var apiTenantPrefix string
		if defs.Tenant {
			apiTenantPrefix = "tenant/{tenantID}/"
		}

		d := struct {
			Repository      string
			Tenant          bool
			Event           bool
			APITenantPrefix string
			ServiceTitle    string
			GenDomain       string
			Service         *libtranslator.ServiceDefinition
			Create          *libgraphql.RequestConfig
			CustomFuncs     []*libtranslator.CustomRequest
			DisableSelect   bool
			DisableCreate   bool
			DisableUpdate   bool
			DisableDelete   bool
		}{
			Repository:      defs.Repository,
			Tenant:          defs.Tenant,
			Event:           definition.Event,
			APITenantPrefix: apiTenantPrefix,
			ServiceTitle:    defs.GetTitle(),
			GenDomain:       defs.TranslatorPath,
			Service:         definition.Translator,
			Create:          definition.Create,
			CustomFuncs:     definition.CustomRequests(),
			DisableSelect:   definition.DisableSelect,
			DisableCreate:   definition.DisableCreate,
			DisableUpdate:   definition.DisableUpdate,
			DisableDelete:   definition.DisableDelete,
		}

		if len(d.Service.Objects()) > 0 {

			buf = &bytes.Buffer{}
			err = restTemplate.Execute(buf, d)
			if err != nil {
				fmt.Println(err)
			}
			content, err := format.Source(buf.Bytes())
			if err != nil {
				// fmt.Println(buf.String())
				fmt.Println("graphql ", d.Service.Definition().Title(), err)
				content = buf.Bytes()
			}
			err = ioutil.WriteFile(
				fmt.Sprintf("../gen/%s-objects.gen.go", d.Service.Definition().Snake()),
				content, 0644)
			if err != nil {
				fmt.Println(err)
			}

		}

		if len(d.Service.Requests()) > 0 {

			buf = &bytes.Buffer{}
			err = requestsTemplate.Execute(buf, d)
			if err != nil {
				fmt.Println(err)
			}
			content, err = format.Source(buf.Bytes())
			if err != nil {
				// fmt.Println(buf.String())
				fmt.Println("graphql ", d.Service.Definition().Title(), err)
				content = buf.Bytes()
			}
			err = ioutil.WriteFile(
				fmt.Sprintf("../gen/%s-requests.gen.go", d.Service.Definition().Snake()),
				content, 0644)
			if err != nil {
				fmt.Println(err)
			}
		}

	}
}

var schemaFuncs = template.New("").Funcs(template.FuncMap{
	"makeSlice": func(args ...string) []string {
		return args
	},
	"getType": func(property libtranslator.PropertyDefinition) string {

		field := property.Definition()

		fieldType := field.GraphqlSchemaType()
		if field.Type() == properties.SelectionPropertyType {
			fieldType = property.EnumTitle() + "Type"
		}
		if field.Type() == properties.EntityPropertyType {
			fieldType = field.GetReference().Title()
		}
		if field.Type() == properties.ValueObjectPropertyType {
			fieldType = field.GetReference().Title()
		}

		content := `{{.Type}}{{if .Required}}!{{end}}`

		buf := &bytes.Buffer{}
		err := template.Must(template.New("").Parse(content)).Execute(buf, struct {
			Type     string
			Required bool
			Nested   bool
		}{
			Type: fieldType,
			Required: field.GetRequired(),
		})
		if err != nil {
			fmt.Println(err)
		}
		return buf.String()
	},
	"buildFunc": func(request *libtranslator.RequestDefinition) string {

		var args []string
		if request.Definition.UseTenants() {
			args = append(args, "tenantID: ID!")
		}
		for _, arg := range request.Args {
			required := ""
			if arg.Definition().GetRequired() {
				required = "!"
			}
			schemaType := arg.Definition().GraphqlSchemaType()
			if arg.Definition().Type() == arguments.SelectionArgumentType {
				schemaType = arg.EnumTitle() + "Type"
			}
			inputType := string(arg.Definition().InputType())
			if inputType == "" {
				inputType = "Input"
			}
			if arg.Definition().CollectionType() == properties.OrderedMapPropertyType {
				if arg.Definition().Type() == arguments.EntityArgumentType {
					if arg.Definition().InputType() == arguments.EntityUpdateInputType {
						schemaType = arg.Definition().GetReference().Title() + "OrderedMapUpdateInput"
					} else {
						schemaType = "[" + arg.Definition().GetReference().Title() + inputType + "!]"
					}
				}
			} else if arg.Definition().CollectionType() == properties.SlicePropertyType {
				if arg.Definition().Type() == arguments.EntityArgumentType {
					schemaType = "[" + arg.Definition().GetReference().Title() + inputType + "!]"
				}
				if arg.Definition().Type() == arguments.ValueObjectArgumentType {
					schemaType = "[" + arg.Definition().GetReference().Title() + inputType + "!]"
				}
			} else {
				if arg.Definition().Type() == arguments.EntityArgumentType {
					schemaType = arg.Definition().GetReference().Title() + inputType
				}
				if arg.Definition().Type() == arguments.ValueObjectArgumentType {
					schemaType = arg.Definition().GetReference().Title() + inputType
				}
			}
			args = append(args, fmt.Sprintf("%s: %s%s", arg.Definition().GetName(), schemaType, required))

			if arg.Definition().IsTranslatable() {
				args = append(args, fmt.Sprintf("%sTranslations: [TranslationInput!]", arg.Definition().GetName()))
			}
			if arg.Definition().GetTranslatable() == properties.TranslatableWYSIWYG {
				args = append(args, fmt.Sprintf("%sWYSIWYG: WYSIWYGInput", arg.Definition().GetName()))
			}
		}

		results := request.Definition.Title() + "Results"
		if len(request.GetResults()) == 0 {
			results = "Boolean"
		}

		content := `{{.Name}}({{.Args}}): {{.Results}}!`
		if !request.Definition.UseTenants() && len(request.Args) == 0 {
			content = `{{.Name}}: {{.Results}}!`
		}

		buf := &bytes.Buffer{}
		err := template.Must(template.New("").Parse(content)).Execute(buf, struct {
			Name    string
			Results string
			Args    string
		}{
			Name:    request.Definition.GetName(),
			Results: results,
			Args:    strings.Join(args, ","),
		})
		fmt.Println("test- ", buf.String(), err)
		return buf.String()
	},
	"getResultType": func(arg libdomain.ArgumentDefinition) string {

		result := arg.GraphqlSchemaType()
		if arg.Type() == arguments.EntityArgumentType {
			result = arg.GetReference().Title()
			if arg.CollectionType() == "Slice" {
				result = "[" + result + "!]"
			}
		}
		return result

	},
})

var restFuncs = template.New("").Funcs(template.FuncMap{
	"makeSlice": func(args ...string) []string {
		return args
	},
	"prepareNullableID": func(field libtranslator.Property, update bool) string {

		var content string
		var updateMask string
		if update || !field.GetRequired() {

			if field.GraphqlType() == graphqlID {
				content = `
				var {{.Field.Name}} string
				if args.Data.{{.Field.Title}} != nil {
					{{.Field.Name}} = string(*args.Data.{{.Field.Title}}){{.UpdateMask}}
				}
				`
			} else if field.IsNested() {
				if !update {
					content = `
					{{.Field.Name}} := &mpb.Create{{.Field.GetReference.Title}}ManyRequest{}
					if args.Data.{{.Field.Title}}.Create != nil {
						for _, source := range *args.Data.{{.Field.Title}}.Create {
							{{.Field.Name}}.Create = append(
								{{.Field.Name}}.Create,
								&mpb.Create{{.Field.GetReference.Title}}Request{
									{{.Field.GetReference.Title}}: &mpb.{{.Field.GetReference.Title}}{
										{{.Field.GetReference.SourceFields}}
									},
								},
							)
						}
					}
					`
				} else {
					content = `var {{.Field.Name}} *mpb.Update{{.Field.GetReference.Title}}ManyRequest`
				}
			} else {
				content = `
				var {{.Field.Name}} {{.Field.GoType}}
				if args.Data.{{.Field.Title}} != nil {
					{{.Field.Name}} = *args.Data.{{.Field.Title}}{{.UpdateMask}}
				}
				`
			}
			if update {
				updateMask = `
				updateMask = append(updateMask, "` + field.GetName() + `")`
			}

		}

		buf := &bytes.Buffer{}
		err := template.Must(template.New("").Parse(content)).Execute(buf, &struct {
			Field      libtranslator.Property
			UpdateMask string
		}{
			Field:      field,
			UpdateMask: updateMask,
		})
		if err != nil {
			fmt.Println(err)
		}
		return buf.String()
	},
	"testStructInput": func(entity libtranslator.ObjectDefinition) string {

		content := `
		{{- range .Entity.StoredProperties}}
		{{ if .IsRepeated }}
		{{ else }}

		{{if eq .Type "TextType"}}
		{{.Title}}    *gclient.String
		{{end}}

		{{if eq .Type "FloatType"}}
		
		{{end}}

		{{ end }}
		{{- end}}

		{{- range .Entity.NestedProperties}}
		{{if eq .Type "One2manyType"}}
		{{else}}
		{{.TitleWithoutID}} *[]gclient.ID
		{{end}}
		{{- end}}
		`

		buf := &bytes.Buffer{}
		err := template.Must(template.New("").Parse(content)).Execute(buf, &struct {
			Entity libtranslator.ObjectDefinition
		}{
			Entity: entity,
		})
		if err != nil {
			fmt.Println(err)
		}
		return buf.String()
	},
	"testPrepareArgs": func(entity libtranslator.ObjectDefinition) string {

		content := `
		{{- range .Entity.StoredProperties}}
		{{ if .IsRepeated }}
		{{ else }}

		{{if eq .Type "TextType"}}
		if field == "{{.Name}}" {
			// data["description"] = gclient.String(request.Data.Properties.Description)
			{{.Name}} := gclient.String(request.Data.Properties.{{.Title}})
			data.{{.Title}} = &{{.Name}}
		}
		{{end}}

		{{if eq .Type "FloatType"}}
		
		{{end}}

		{{ end }}
		{{- end}}

		{{- range .Entity.NestedProperties}}
		{{if eq .Type "One2manyType"}}
		{{else}}
		if field == "{{.TitleWithoutID}}" {
			// data["descriptionTranslations"] = request.Data.Properties.DescriptionTranslations
			var {{.NameWithoutID}} []gclient.ID
			for _, t := range request.Data.{{.TitleWithoutID}}.Set {
				{{.NameWithoutID}} = append({{.NameWithoutID}}, gclient.ID(t.ID))
			}
			data.{{.TitleWithoutID}} = &{{.NameWithoutID}}
		}
		{{end}}
		{{- end}}
		`

		buf := &bytes.Buffer{}
		err := template.Must(template.New("").Parse(content)).Execute(buf, &struct {
			Entity libtranslator.ObjectDefinition
		}{
			Entity: entity,
		})
		if err != nil {
			fmt.Println(err)
		}
		return buf.String()
	},
	"testQueryProperties": func(entity libtranslator.TableInterface) string {

		content := `

		{{define "testQueryProperties"}}

			{{- range .StoredProperties}}
			{{ if .IsRepeated }}

			{{ if .GetReturnDetailsInTests }}
			{{.TitleWithoutID}} []struct {
				ID gclient.ID

				{{template "testQueryProperties" .GetReference}}
			}
			{{ else }}
			{{.Title}} []struct {
				ID gclient.ID
			}
			{{ end }}

			{{ else }}

			{{if eq .Type "TextType"}}
			{{.Title}} gclient.String
			{{end}}

			{{if eq .Type "IntegerType"}}
			{{.Title}} gclient.Int
			{{end}}

			{{if eq .Type "FloatType"}}
			{{.Title}} gclient.Float
			{{end}}

			{{if eq .Type "BooleanType"}}
			{{.Title}} gclient.Boolean
			{{end}}

			{{if eq .Type "Many2oneType"}}
			{{ if .ReturnDetailsInTests }}
			{{.TitleWithoutID}} struct {
				ID gclient.ID

				{{template "testQueryProperties" .GetReference}}
			}
			{{ else }}
			{{.Title}} gclient.ID ` + "`graphql:\"{{.Name}}\"`" + `
			{{end}}
			{{end}}

			{{ end }}
			{{- end}}

			{{- range .NestedProperties}}
			{{if eq .Type "One2manyType"}}
			{{.TitleWithoutID}} []struct {
				ID gclient.ID

				{{template "testQueryProperties" .GetReference}}
			}
			{{else}}
			{{.TitleWithoutID}} []gclient.String
			{{end}}
			{{- end}}

		{{end}}

		{{template "testQueryProperties" .Entity}}
		
		`

		buf := &bytes.Buffer{}
		err := template.Must(template.New("").Parse(content)).Execute(buf, &struct {
			Entity libtranslator.TableInterface
		}{
			Entity: entity,
		})
		if err != nil {
			fmt.Println(err)
		}
		return buf.String()
	},
	"testConvertResults": func(results []libtranslator.ArgsInterface) string {
		return testConvertResults(results)
	},
})

var requestsFuncs = template.New("").Funcs(template.FuncMap{})

var testConvertResults = func(results []libtranslator.ArgsInterface) string {

	content := `
	
	{{define "testConvertResults"}}

		{{- range .Properties}}
			{{ if .IsRepeated }}

				{{ if .GetReturnDetailsInTests }}
					var new{{.TitleWithoutID}} []*mpb.{{.GetReference.Title}}
					for _, t := range source{{$.TitleWithoutID}}.{{.TitleWithoutID}} {
						var source{{.TitleWithoutID}} = t.{{.TitleWithoutID}}
						var {{.NameWithoutID}} *mpb.{{.GetReference.Title}}
						{{.NameWithoutID}}.ID = string(source{{.TitleWithoutID}}.ID)
						{{template "testConvertResults" . }}
						new{{.TitleWithoutID}} = append(new{{.TitleWithoutID}}, {{.NameWithoutID}})
					}
					{{$.NameWithoutID}}.{{.TitleWithoutID}} = new{{.TitleWithoutID}}
				{{ else }}
					var {{.NameWithoutID}} []string
					for _, t := range source{{$.TitleWithoutID}}.{{.TitleWithoutID}} {
						{{.NameWithoutID}} = append({{.NameWithoutID}}, string(t))
					}
					{{$.NameWithoutID}}.{{.TitleWithoutID}} = {{.NameWithoutID}}
				{{ end }}

			{{ else }}

				{{if eq .Type "TextType"}}
					{{$.NameWithoutID}}.{{.Title}} = string(source{{$.TitleWithoutID}}.{{.Title}})
				{{end}}

				{{if eq .Type "IntegerType"}}
					{{$.NameWithoutID}}.{{.Title}} = int32(source{{$.TitleWithoutID}}.{{.Title}})
				{{end}}

				{{if eq .Type "FloatType"}}
					{{$.NameWithoutID}}.{{.Title}} = float64(source{{$.TitleWithoutID}}.{{.Title}})
				{{end}}

				{{if eq .Type "BooleanType"}}
					{{$.NameWithoutID}}.{{.Title}} = bool(source{{$.TitleWithoutID}}.{{.Title}})
				{{end}}

				{{if eq .Type "Many2oneType"}}
					{{ if .ReturnDetailsInTests }}
						var source{{.TitleWithoutID}} = t.{{.TitleWithoutID}}
						var {{.NameWithoutID}} *mpb.{{.GetReference.Title}}
						{{.NameWithoutID}}.ID = string(source{{.TitleWithoutID}}.ID)
						{{ template "testConvertResults" . }}
						{{$.NameWithoutID}}.{{.TitleWithoutID}} = {{.NameWithoutID}}
					{{ else }}
						{{$.NameWithoutID}}.{{.Title}} = string(source{{$.TitleWithoutID}}.{{.Title}})
					{{end}}
				{{end}}


				{{if eq .Type "One2manyType"}}
					var new{{.TitleWithoutID}} []*mpb.{{.GetReference.Title}}
					for _, t := range source{{$.TitleWithoutID}}.{{.TitleWithoutID}} {
						var source{{.TitleWithoutID}} = t.{{.TitleWithoutID}}
						var {{.NameWithoutID}} *mpb.{{.GetReference.Title}}
						{{.NameWithoutID}}.ID = string(source{{.TitleWithoutID}}.ID)
						{{ template "testConvertResults" dict "Properties" GetReference.Properties "ParentProperty" . }}
						new{{.TitleWithoutID}} = append(new{{.TitleWithoutID}}, {{.NameWithoutID}})
					}
					{{$.NameWithoutID}}.{{.TitleWithoutID}} = new{{.TitleWithoutID}}
				{{end}}
				{{if eq .Type "Many2manyType"}}
					var {{.NameWithoutID}} []string
					for _, t := range source{{$.TitleWithoutID}}.{{.TitleWithoutID}} {
						{{.NameWithoutID}} = append({{.NameWithoutID}}, string(t))
					}
					{{$.NameWithoutID}}.{{.TitleWithoutID}} = {{.NameWithoutID}}
				{{end}}
			{{end}}
		{{- end}}

	{{end}}

	{{ template "testConvertResults" dict "Properties" .Properties "ParentProperty" .ParentProperty }}
	`

	buf := &bytes.Buffer{}
	err := template.Must(template.New("").Funcs(template.FuncMap{
		"dict": dictTemplate,
	}).Parse(content)).Execute(buf, &struct {
		Properties     []libtranslator.ArgsInterface
		ParentProperty libtranslator.Property
	}{
		Properties:     results,
		ParentProperty: nil,
	})
	if err != nil {
		fmt.Println(err)
	}
	return buf.String()
}

var dictTemplate = func(values ...interface{}) (map[string]interface{}, error) {
	if len(values)%2 != 0 {
		return nil, errors.New("invalid dict call")
	}
	dict := make(map[string]interface{}, len(values)/2)
	for i := 0; i < len(values); i += 2 {
		key, ok := values[i].(string)
		if !ok {
			return nil, errors.New("dict keys must be strings")
		}
		dict[key] = values[i+1]
	}
	return dict, nil
}
