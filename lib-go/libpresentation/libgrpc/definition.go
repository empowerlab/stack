package libgrpc

import (
	"fmt"
	"strings"

	gclient "github.com/hasura/go-graphql-client"

	"gitlab.com/empowerlab/stack/lib-go/libpresentation/libtranslator"
)

// Definitions contains all the graphql definitions in the service.
type Definitions struct {
	Prefix     string
	Name       string
	Repository string
	TranslatorPath string
	Tenant     bool
	Auth       bool
	slice      []*Definition
	byIds      map[string]*Definition
}

// GetTitle is used to return the name of the definition with InitCap
func (ds *Definitions) GetTitle() string {
	return strings.Title(ds.Name)
}

// Register is used to register a new definition into the service.
func (ds *Definitions) Register(d *Definition) {

	if ds.byIds == nil {
		ds.byIds = map[string]*Definition{}
	}

	// d.aggregate = d.Translator.Definition()

	ds.slice = append(ds.slice, d)
	ds.byIds[d.Translator.Definition().Name] = d
}

// Slice return the definitions as a slice.
func (ds *Definitions) Slice() []*Definition {
	return ds.slice
}

// GetByID return the specified definition by its ID.
func (ds *Definitions) GetByID(id string) *Definition {
	d := ds.byIds[id]
	if d == nil {
		panic(fmt.Sprintf("The graphql definition %s doesn't exist", id))
	}
	return d
}

type RequestConfig struct{}

// Definition is used to declare the information of a model, so it can generate its code.
type Definition struct {
	Translator     *libtranslator.ServiceDefinition
	GenDomain      string
	Create         *RequestConfig
	Update         *RequestConfig
	Delete         *RequestConfig
	CustomQueries  []*libtranslator.CustomRequest
	CustomCommands []*libtranslator.CustomRequest
	DisableSelect  bool
	DisableCreate  bool
	DisableUpdate  bool
	DisableDelete  bool
	Event          bool
	aggregate      *libtranslator.AggregateDefinition
}

func (d *Definition) Aggregate() *libtranslator.AggregateDefinition {
	return d.aggregate
}
func (d *Definition) CustomRequests() []*libtranslator.CustomRequest {
	return append(d.CustomQueries, d.CustomCommands...)
}

// GetCustomFuncsData return the custom function information for collections and pools,
// in a format usable by templates.
// func (d *Definition) GetCustomFuncsData() []*liborm.CustomFuncData {
// 	var result []*liborm.CustomFuncData
// 	for _, c := range d.CustomFuncs {
// 		result = append(result, c.GetCustomFuncData())
// 	}
// 	return result
// }

type TranslationInput struct {
	Key         gclient.String
	Translation gclient.String
}

type Translation struct {
	Key         gclient.String
	Translation gclient.String
}
