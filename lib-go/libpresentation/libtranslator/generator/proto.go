package generator

import (
	"bytes"
	"fmt"
	"strings"

	"io/ioutil"
	"os"

	"embed"

	"text/template"

	// "go/format"

	// "github.com/dave/jennifer/jen"

	// "gitlab.com/empowerlab/stack/lib-go/libdata"
	// "gitlab.com/empowerlab/stack/lib-go/libdata/fields"
	// "gitlab.com/empowerlab/stack/lib-go/libdomain"
	"github.com/iancoleman/strcase"
	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain/properties"
	"gitlab.com/empowerlab/stack/lib-go/libpresentation/libtranslator"

	// "gitlab.com/empowerlab/stack/lib-go/libpresentation/libtranslator/properties"
	"gitlab.com/empowerlab/stack/lib-go/libutils"
	// "gitlab.com/empowerlab/stack/lib-go/libdomain/templates"
)

//go:generate protoc -I=. -I=$GOPATH/src --go_out=$GOPATH/src basepb/main.proto

//go:embed proto.go.tmpl
var fproto embed.FS

// Protos will generate the protobuf files for the models.
func Protos(defs *libtranslator.Definitions) {

	var err error
	file, _ := fproto.ReadFile("proto.go.tmpl")
	protoTemplate := template.Must(protoFuncs.Parse(string(file)))

	d := struct {
		Repository string
		// UseTenants      bool
		Services        []*libtranslator.ServiceDefinition
		EntityPositions map[string]int
		Import          string
	}{
		Repository: defs.Repository,
		// UseTenants: defs.UseTenants,
	}

	imports := map[string]string{}
	for _, definition := range defs.Slice() {

		d.Services = append(d.Services, definition)

		// for _, model := range d.Models {
		// 	for _, field := range model.Fields {
		// 		if field.Type() == fields.DateFieldType {
		// 			field.Type = "Datetime"
		// 		}
		// 		if field.Type() == fields.DatetimeFieldType {
		// 			field.Type = "Datetime"
		// 		}
		// 	}
		// }

		// for _, entity := range definition.GetEntities() {
		// 	for _, property := range entity.Properties {
		// 		// fmt.Printf("%s %T\n", property.GetName(), property)
		// 		switch v := property.(type) {
		// 		case *properties.Many2one:
		// 			if v.GetReference().DomainPath() != defs.Repository {
		// 				path := "import \"" + v.GetReference().DomainPath() + "/gen/pb/main.proto\";"
		// 				imports[path] = path
		// 			}
		// 		}
		// 	}
		// }
		// for _, request := range definition.GetCustomRequests() {
		// 	for _, result := range request.Results {
		// 		if result.GetReference() != nil {
		// 			if result.GetReference().DomainPath() != defs.Repository {
		// 				path := "import \"" + result.GetReference().DomainPath() + "/gen/pb/main.proto\";"
		// 				imports[path] = path
		// 			}
		// 		}
		// 	}
		// }

	}

	d.Import = strings.Join(libutils.MapKeys(imports), "\n")

	entityPositions := map[string]int{}
	// for _, a := range d.Services {
	// 	for _, entity := range a.GetEntities() {
	// 		position := 3
	// 		for _, property := range entity.StoredProperties() {
	// 			// property.SetPosition(position)

	// 			position = position + 1
	// 			if property.Type() == properties.Many2onePropertyType {
	// 				position = position + 1
	// 			}
	// 			if property.IsTranslatable() {
	// 				position = position + 1
	// 			}
	// 		}

	// 		entityPositions[entity.Name] = position
	// 	}
	// 	for _, valueObject := range a.ValueObjects {
	// 		position := 3
	// 		for _, property := range valueObject.StoredProperties() {
	// 			// property.SetPosition(position)

	// 			position = position + 1
	// 			if property.Type() == properties.Many2onePropertyType {
	// 				position = position + 1
	// 			}
	// 			if property.IsTranslatable() {
	// 				position = position + 1
	// 			}
	// 		}
	// 		for _, property := range valueObject.NestedProperties() {
	// 			// property.SetPosition(position)

	// 			position = position + 1
	// 			if property.Type() == properties.Many2onePropertyType {
	// 				position = position + 1
	// 			}
	// 			if property.IsTranslatable() {
	// 				position = position + 1
	// 			}
	// 		}

	// 		entityPositions[valueObject.Name] = position
	// 	}
	// }
	d.EntityPositions = entityPositions

	err = os.MkdirAll("./gen/pb", os.ModePerm)
	if err != nil {
		fmt.Println(err)
	}

	buf := &bytes.Buffer{}
	err = protoTemplate.Execute(buf, d)
	if err != nil {
		fmt.Println(buf)
		fmt.Println("model proto", err)
	}
	err = ioutil.WriteFile(
		"./gen/pb/main.proto",
		buf.Bytes(), 0644)
	if err != nil {
		fmt.Println(err)
	}

}

// nolint:lll
var protoFuncs = template.New("").Funcs(template.FuncMap{
	"add": func(i int, j int) int {
		return i + j
	},
	"upper": func(s string) string {
		return strings.ToUpper(strcase.ToSnake(s))
	},
	"position": func(entityPositions map[string]int, entity libtranslator.TableInterface, j int) int {
		return entityPositions[entity.GetName()] + j
	},
	"makeSlice": func(args ...string) []string {
		return args
	},
	"getProperty": func(table libtranslator.ObjectDefinitionInterface, property *libtranslator.PropertyDefinition, i int, j int, data string) string {

		var content string

		// if property.GetRequired() {
		content = `{{.Property.Definition.ProtoTypeArg}} {{.Property.Definition.GetName}} = {{.Position}};`
		// } else {
		// 	if table.GetUseOneOf() {
		// 		content = `{ {oneof has{{.Property.Title}} { {{.Property.ProtoType}} {{.Property.GetName}} = {{.Position}};}`
		// 	} else {
		// 		content = `{{.Property.ProtoTypeArg}} {{.Property.GetName}} = {{.Position}} [(gogoproto.nullable) = true, (gogoproto.moretags) = "db:\"{{.Property.Snake}}\""];`
		// 	}
		// }
		if property.Definition().Type() == properties.EntityPropertyType {
			content = `{{.Property.Definition.ProtoTypeArg}}{{.Data}} {{.Property.Definition.GetName}} = {{.Position}};`
		}
		if property.Definition().Type() == properties.ValueObjectPropertyType {
			content = `{{.Property.Definition.ProtoTypeArg}}{{.Data}} {{.Property.Definition.GetName}} = {{.Position}};`
		}
		if property.Definition().Type() == properties.SelectionPropertyType {
			content = `{{.Property.EnumTitle}}Properties {{.Property.Definition.GetName}} = {{.Position}};`
		}
		if property.Definition().CollectionType() == properties.OrderedMapPropertyType && data == "UpdateInput" {
			content = `{{.Property.Definition.GetReference.Title}}OrderedMapUpdateInput {{.Property.Definition.GetName}} = {{.Position}};`
		}
		if property.Definition().CollectionType() == properties.SlicePropertyType && property.Definition().Type() == properties.IDPropertyType && data == "UpdateInput" {
			content = `{{.Property.Definition.GetReference.Title}}SliceUpdateInput {{.Property.Definition.GetName}} = {{.Position}};`
		}

		positionDefault := property.Position() + 1

		// if property.Definition().Type() == properties.EntityPropertyType {

		// 	var contentMany2one string
		// 	content = content + "\n" + `    bool {{.Property.NameWithoutID}}Loaded = {{.LoadedPositionMany2one}} [(gogoproto.moretags) = "db:\"{{.Property.Snake}}\""];`
		// 	if property.IsRepeated() {
		// 		contentMany2one = `    repeated`
		// 	}
		// 	if property.GetRequired() {
		// 		contentMany2one = contentMany2one + `    {{.Property.GetReference.Title}} {{.Property.NameWithoutID}} = {{.PositionMany2one}} [(gogoproto.moretags) = "db:\"{{.Property.Snake}}\""];`
		// 	} else {
		// 		contentMany2one = contentMany2one + `    {{.Property.GetReference.Title}} {{.Property.NameWithoutID}} = {{.PositionMany2one}} [(gogoproto.nullable) = true, (gogoproto.moretags) = "db:\"{{.Property.Snake}}\""];`
		// 	}

		// 	content = content + "\n" + contentMany2one

		// 	positionDefault = property.GetPosition() + 2
		// }

		// if property.Type() == properties.Many2manyPropertyType {

		// 	many2manyProperty := property.(*properties.Many2many)

		// 	var contentMany2many string
		// 	content = content + "\n" + `    bool {{.Property.NameWithoutID}}Loaded = ` + fmt.Sprint(many2manyProperty.LoadedPositionMany2many) + ` [(gogoproto.moretags) = "db:\"{{.Property.Snake}}\""];`
		// 	if property.GetRequired() {
		// 		contentMany2many = contentMany2many + `    repeated {{.Property.GetReference.Title}} {{.Property.NameWithoutID}} = ` + fmt.Sprint(many2manyProperty.PositionMany2many) + ` [(gogoproto.moretags) = "db:\"{{.Property.Snake}}\""];`
		// 	} else {
		// 		contentMany2many = contentMany2many + `    repeated {{.Property.GetReference.Title}} {{.Property.NameWithoutID}} = ` + fmt.Sprint(many2manyProperty.PositionMany2many) + ` [(gogoproto.nullable) = true, (gogoproto.moretags) = "db:\"{{.Property.Snake}}\""];`
		// 	}

		// 	content = content + "\n" + contentMany2many

		// 	positionDefault = property.GetPosition() + 2
		// }

		if property.Definition().IsTranslatable() {
			// textProperty := property.Definition().(*properties.Text)
			content = content + "\n" + `    map<string, string> {{.Property.Definition.NameWithoutID}}Translations = ` + fmt.Sprint(property.PositionTranslations()) + `;`
			if data == "" {
				content = content + "\n" + `    bool {{.Property.Definition.NameWithoutID}}TranslationsLoaded = ` + fmt.Sprint(property.PositionTranslationsLoaded()) + `;`
			}
			if data == "UpdateInput" {
				content = content + "\n" + `    bool {{.Property.Definition.NameWithoutID}}TranslationsIsNull = ` + fmt.Sprint(property.PositionTranslationsLoaded()) + `;`
			}
		}

		buf := &bytes.Buffer{}
		err := template.Must(template.New("").Parse(content)).Execute(buf, struct {
			Table    libtranslator.ObjectDefinitionInterface
			Property *libtranslator.PropertyDefinition
			Position int
			// PositionMany2one       int
			// LoadedPositionMany2one int
			PositionDefault int
			Data string
		}{
			Table:    table,
			Property: property,
			Position: property.Position(),
			// PositionMany2one:       property.GetPositionMany2one(),
			// LoadedPositionMany2one: property.GetLoadedPositionMany2one(),
			PositionDefault: positionDefault,
			Data: data,
		})
		if err != nil {
			fmt.Println(err)
		}
		return buf.String()
	},
})
