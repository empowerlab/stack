package properties

import (
	// "strings"

	"strings"

	"github.com/iancoleman/strcase"

	"gitlab.com/empowerlab/stack/lib-go/libpresentation/libtranslator"
)

// JSONPropertyType contains the field type for JSON
const JSONPropertyType = libtranslator.PropertyType("JSONType")

/*JSON is the field type you can use in definition to declare a JSON field.
 */
type JSON struct {
	Name           string
	String         string
	Required       bool
	Unique         bool
	PrimaryKey     bool
	Store          bool
	TitleName      string
	DBName         string
	LoadedPosition int
	Position       int
}

// GetName return the name of the field.
func (f *JSON) GetName() string {
	return f.Name
}

// Title return the title of the field.
func (f *JSON) Title() string {
	titleName := strcase.ToCamel(f.Name)
	if f.TitleName != "" {
		titleName = f.TitleName
	}
	return titleName
}

// Title return the title of the field.
func (f *JSON) NameWithoutID() string {
	return f.Name
}

// Title return the title of the field.
func (f *JSON) TitleWithoutID() string {
	return f.Title()
}

// Snake return the name of the field, in snake_case. This is essentially used for database.
func (f *JSON) Snake() string {
	dbName := strcase.ToSnake(f.Name)
	if f.DBName != "" {
		dbName = f.DBName
	}
	return dbName
}

func (f *JSON) Upper() string {
	return strings.ToUpper(f.Snake())
}

// Type return the type of the field.
func (f *JSON) Type() libtranslator.PropertyType {
	return JSONPropertyType
}

// Type return the type of the field.
func (f *JSON) GoType() string {
	return "[]byte"
}

// Type return the type of the field.
func (f *JSON) GoTypeID() string {
	return f.GoType()
}

// Type return the type of the field.
func (f *JSON) GoNil() string {
	return "nil"
}

// Type return the type of the field.
func (f *JSON) JSONType() string {
	return f.GoType()
}

// ProtoType return the protobuf type for this field.
func (f *JSON) ProtoType() string {
	return "bytes"
}

func (f *JSON) ProtoTypeArg() string {
	return f.ProtoType()
}

// ProtoType return the protobuf type for this field.
func (f *JSON) ProtoTypeOptional() string {
	return f.ProtoType()
}

// ProtoType return the protobuf type for this field.
func (f *JSON) DBType() *libtranslator.DBType {
	return &libtranslator.DBType{
		Type:  "sql.NullString",
		Value: "String",
	}
}

// Type return the type of the field.
func (f *JSON) GraphqlType() string {
	return f.GoType()
}

// Type return the type of the field.
func (f *JSON) GraphqlSchemaType() string {
	return "String"
}

// GetReference return the name of referenced model, if this field is linked to another model.
func (f *JSON) GetReferenceName() string {
	return ""
}

// GetReferenceDefinition return the definition of referenced model,
// if this field is linked to another model.
func (f *JSON) GetReference() libtranslator.TableInterface {
	return nil
}

// SetReferenceDefinition set the definition of referenced model,
// if this field is linked to another model.
func (f *JSON) SetReference(libtranslator.TableInterface) {
}

// GetInverseProperty return the inverse field, if applicable.
func (f *JSON) GetInverseProperty() string {
	return ""
}

// GetRequired return if this field is required or not.
func (f *JSON) GetRequired() bool {
	return f.Required
}

// GetPrimaryKey return if this field is a primary key or not.
func (f *JSON) GetPrimaryKey() bool {
	return f.PrimaryKey
}

func (f *JSON) IsStored() bool {
	return true
}

func (f *JSON) IsNested() bool {
	return false
}

//GetFieldData return the field information in a format exploimodel by templates.
// func (f *JSON) GetFieldData() *libtranslator.FieldData {
// 	return &libtranslator.FieldData{
// 		Name:            f.Name,
// 		NameWithoutID: f.Name,
// 		// Type:            JSONPropertyType.GoType(),
// 		// GoType:          JSONPropertyType.GoType(),
// 		// ProtoType:       JSONPropertyType.ProtoType(),
// 		DBType: &libtranslator.DBType{
// 			Type:  "sql.NullString",
// 			Value: "String",
// 		},
// 		Title:            f.Title(),
// 		TitleWithoutID: f.Title(),
// 		LesserTitle:      strings.Title(f.GetName()),
// 		Snake:            f.Snake(),
// 		Required:         f.Required,
// 		Nested:           false,
// 		Reference:        nil,
// 		InverseProperty:     "",
// 		// Definition:       f,
// 	}
// }

func (f *JSON) SetPosition(position int) {
	f.Position = position
}

func (f *JSON) GetPosition() int {
	return f.Position
}

func (r *JSON) IsRepeated() bool {
	return false
}

func (r *JSON) IsTranslatable() bool {
	return false
}

func (r *JSON) GetReturnDetailsInTests() bool {
	return false
}

func (r *JSON) GetLoadedPosition() int {
	return r.LoadedPosition
}
func (r *JSON) GetLoadedPositionMany2one() int {
	return 0
}
func (r *JSON) GetPositionMany2one() int {
	return 0
}
