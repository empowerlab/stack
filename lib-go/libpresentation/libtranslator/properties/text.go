package properties

import (
	// "strings"

	"strings"

	"github.com/iancoleman/strcase"

	"gitlab.com/empowerlab/stack/lib-go/libpresentation/libtranslator"
)

// TextPropertyType contains the field type for Text
const TextPropertyType = libtranslator.PropertyType("TextType")

type TranslatableOptions struct {
	LoadedPosition int
	Position       int
}

/*Text is the field type you can use in definition to declare a Text field.
 */
type Text struct {
	Name                string
	String              string
	Required            bool
	Translatable        bool
	TranslatableOptions *TranslatableOptions
	Unique              bool
	PrimaryKey          bool
	Store               bool
	TitleName           string
	DBName              string
	LoadedPosition      int
	Position            int
}

// GetName return the name of the field.
func (f *Text) GetName() string {
	return f.Name
}

// Title return the title of the field.
func (f *Text) Title() string {
	titleName := strcase.ToCamel(f.Name)
	if f.TitleName != "" {
		titleName = f.TitleName
	}
	return titleName
}

// Title return the title of the field.
func (f *Text) NameWithoutID() string {
	return f.Name
}

// Title return the title of the field.
func (f *Text) TitleWithoutID() string {
	return f.Title()
}

// Snake return the name of the field, in snake_case. This is essentially used for database.
func (f *Text) Snake() string {
	dbName := strcase.ToSnake(f.Name)
	if f.DBName != "" {
		dbName = f.DBName
	}
	return dbName
}

func (f *Text) Upper() string {
	return strings.ToUpper(f.Snake())
}

// Type return the type of the field.
func (f *Text) Type() libtranslator.PropertyType {
	return TextPropertyType
}

// Type return the type of the field.
func (f *Text) GoType() string {
	return libtranslator.STRING
}

// Type return the type of the field.
func (f *Text) GoTypeID() string {
	return f.GoType()
}

// Type return the type of the field.
func (f *Text) GoNil() string {
	return "\"\""
}

// Type return the type of the field.
func (f *Text) JSONType() string {
	return f.GoType()
}

// ProtoType return the protobuf type for this field.
func (f *Text) ProtoType() string {
	return f.GoType()
}

func (f *Text) ProtoTypeArg() string {
	return f.ProtoType()
}

// ProtoType return the protobuf type for this field.
func (f *Text) ProtoTypeOptional() string {
	return f.ProtoType()
}

// ProtoType return the protobuf type for this field.
func (f *Text) DBType() *libtranslator.DBType {
	return &libtranslator.DBType{
		Type:  "sql.NullString",
		Value: "String",
	}
}

// Type return the type of the field.
func (f *Text) GraphqlType() string {
	return f.GoType()
}

// Type return the type of the field.
func (f *Text) GraphqlSchemaType() string {
	return "String"
}

// GetReference return the name of referenced model, if this field is linked to another model.
func (f *Text) GetReferenceName() string {
	return ""
}

// GetReferenceDefinition return the definition of referenced model,
// if this field is linked to another model.
func (f *Text) GetReference() libtranslator.TableInterface {
	return nil
}

// SetReferenceDefinition set the definition of referenced model,
// if this field is linked to another model.
func (f *Text) SetReference(libtranslator.TableInterface) {
}

// GetInverseProperty return the inverse field, if applicable.
func (f *Text) GetInverseProperty() string {
	return ""
}

// GetRequired return if this field is required or not.
func (f *Text) GetRequired() bool {
	return f.Required
}

// GetPrimaryKey return if this field is a primary key or not.
func (f *Text) GetPrimaryKey() bool {
	return f.PrimaryKey
}

func (f *Text) IsStored() bool {
	return true
}

func (f *Text) IsNested() bool {
	return false
}

//GetFieldData return the field information in a format exploimodel by templates.
// func (f *Text) GetFieldData() *libtranslator.FieldData {
// 	return &libtranslator.FieldData{
// 		Name:            f.Name,
// 		NameWithoutID: f.Name,
// 		// Type:            TextPropertyType.GoType(),
// 		// GoType:          TextPropertyType.GoType(),
// 		// ProtoType:       TextPropertyType.ProtoType(),
// 		DBType: &libtranslator.DBType{
// 			Type:  "sql.NullString",
// 			Value: "String",
// 		},
// 		Title:            f.Title(),
// 		TitleWithoutID: f.Title(),
// 		LesserTitle:      strings.Title(f.GetName()),
// 		Snake:            f.Snake(),
// 		Required:         f.Required,
// 		Nested:           false,
// 		Reference:        nil,
// 		InverseProperty:     "",
// 		// Definition:       f,
// 	}
// }

func (f *Text) SetPosition(position int) {
	f.Position = position
}

func (f *Text) GetPosition() int {
	return f.Position
}

func (r *Text) IsRepeated() bool {
	return false
}

func (r *Text) IsTranslatable() bool {
	return r.Translatable
}

func (r *Text) GetReturnDetailsInTests() bool {
	return false
}

func (r *Text) GetLoadedPosition() int {
	return r.LoadedPosition
}
func (r *Text) GetLoadedPositionMany2one() int {
	return 0
}
func (r *Text) GetPositionMany2one() int {
	return 0
}
