package properties

import (
	// "strings"

	"strings"

	"github.com/iancoleman/strcase"

	"gitlab.com/empowerlab/stack/lib-go/libpresentation/libtranslator"
)

// DatetimePropertyType contains the field type for Datetime
const DatetimePropertyType = libtranslator.PropertyType("DatetimeType")

/*Datetime is the field type you can use in definition to declare a Datetime field.
 */
type Datetime struct {
	Name           string
	String         string
	Required       bool
	Unique         bool
	PrimaryKey     bool
	Store          bool
	TitleName      string
	DBName         string
	LoadedPosition int
	Position       int
}

// GetName return the name of the field.
func (f *Datetime) GetName() string {
	return f.Name
}

// Title return the title of the field.
func (f *Datetime) Title() string {
	titleName := strcase.ToCamel(f.Name)
	if f.TitleName != "" {
		titleName = f.TitleName
	}
	return titleName
}

// Title return the title of the field.
func (f *Datetime) NameWithoutID() string {
	return f.Name
}

// Title return the title of the field.
func (f *Datetime) TitleWithoutID() string {
	return f.Title()
}

// Snake return the name of the field, in snake_case. This is essentially used for database.
func (f *Datetime) Snake() string {
	dbName := strcase.ToSnake(f.Name)
	if f.DBName != "" {
		dbName = f.DBName
	}
	return dbName
}

func (f *Datetime) Upper() string {
	return strings.ToUpper(f.Snake())
}

// Type return the type of the field.
func (f *Datetime) Type() libtranslator.PropertyType {
	return DatetimePropertyType
}

// Type return the type of the field.
func (f *Datetime) GoType() string {
	return "*ptypes.Timestamp"
}

// Type return the type of the field.
func (f *Datetime) GoTypeID() string {
	return f.GoType()
}

// Type return the type of the field.
func (f *Datetime) GoNil() string {
	return "nil"
}

// Type return the type of the field.
func (f *Datetime) JSONType() string {
	return f.GoType()
}

// ProtoType return the protobuf type for this field.
func (f *Datetime) ProtoType() string {
	return "google.protobuf.Timestamp"
}

func (f *Datetime) ProtoTypeArg() string {
	return f.ProtoType()
}

// ProtoType return the protobuf type for this field.
func (f *Datetime) ProtoTypeOptional() string {
	return f.ProtoType()
}

// ProtoType return the protobuf type for this field.
func (f *Datetime) DBType() *libtranslator.DBType {
	return &libtranslator.DBType{
		Type:  "ptypes.Timestamp",
		Value: "",
	}
}

// Type return the type of the field.
func (f *Datetime) GraphqlType() string {
	return f.GoType()
}

// Type return the type of the field.
func (f *Datetime) GraphqlSchemaType() string {
	return "Time"
}

// GetReference return the name of referenced model, if this field is linked to another model.
func (f *Datetime) GetReferenceName() string {
	return ""
}

// GetReferenceDefinition return the definition of referenced model,
// if this field is linked to another model.
func (f *Datetime) GetReference() libtranslator.TableInterface {
	return nil
}

// SetReferenceDefinition set the definition of referenced model,
// if this field is linked to another model.
func (f *Datetime) SetReference(libtranslator.TableInterface) {
}

// GetInverseProperty return the inverse field, if applicable.
func (f *Datetime) GetInverseProperty() string {
	return ""
}

// GetRequired return if this field is required or not.
func (f *Datetime) GetRequired() bool {
	return f.Required
}

// GetPrimaryKey return if this field is a primary key or not.
func (f *Datetime) GetPrimaryKey() bool {
	return f.PrimaryKey
}

func (f *Datetime) IsStored() bool {
	return true
}

func (f *Datetime) IsNested() bool {
	return false
}

// //GetFieldData return the field information in a format exploimodel by templates.
// func (f *Datetime) GetFieldData() *libtranslator.FieldData {
// 	return &libtranslator.FieldData{
// 		Name:            f.Name,
// 		NameWithoutID: f.Name,
// 		// Type:            DatetimePropertyType.GoType(),
// 		// GoType:          DatetimePropertyType.GoType(),
// 		// ProtoType:       DatetimePropertyType.ProtoType(),
// 		DBType: &libtranslator.DBType{
// 			Type:  "ptypes.Timestamp",
// 			Value: "",
// 		},
// 		Title:            f.Title(),
// 		TitleWithoutID: f.Title(),
// 		LesserTitle:      strings.Title(f.GetName()),
// 		Snake:            f.Snake(),
// 		Required:         f.Required,
// 		Nested:           false,
// 		Reference:        nil,
// 		InverseProperty:     "",
// 		// Definition:       f,
// 	}
// }

func (f *Datetime) SetPosition(position int) {
	f.Position = position
}

func (f *Datetime) GetPosition() int {
	return f.Position
}

func (r *Datetime) IsRepeated() bool {
	return false
}

func (r *Datetime) IsTranslatable() bool {
	return false
}

func (r *Datetime) GetReturnDetailsInTests() bool {
	return false
}
func (r *Datetime) GetLoadedPosition() int {
	return r.LoadedPosition
}
func (r *Datetime) GetLoadedPositionMany2one() int {
	return 0
}
func (r *Datetime) GetPositionMany2one() int {
	return 0
}
