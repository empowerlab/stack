package properties

import (
	// "strings"

	"strings"

	"github.com/iancoleman/strcase"

	"gitlab.com/empowerlab/stack/lib-go/libpresentation/libtranslator"
)

// IntegerPropertyType contains the field type for Interger
const IntegerPropertyType = libtranslator.PropertyType("IntegerType")

/*Integer is the field type you can use in definition to declare an Integer field.
 */
type Integer struct {
	Name           string
	String         string
	Required       bool
	Unique         bool
	PrimaryKey     bool
	Store          bool
	TitleName      string
	DBName         string
	LoadedPosition int
	Position       int
}

// GetName return the name of the field.
func (f *Integer) GetName() string {
	return f.Name
}

// Title return the title of the field.
func (f *Integer) Title() string {
	titleName := strcase.ToCamel(f.Name)
	if f.TitleName != "" {
		titleName = f.TitleName
	}
	return titleName
}

// Title return the title of the field.
func (f *Integer) NameWithoutID() string {
	return f.Name
}

// Title return the title of the field.
func (f *Integer) TitleWithoutID() string {
	return f.Title()
}

// Snake return the name of the field, in snake_case. This is essentially used for database.
func (f *Integer) Snake() string {
	dbName := strcase.ToSnake(f.Name)
	if f.DBName != "" {
		dbName = f.DBName
	}
	return dbName
}

func (f *Integer) Upper() string {
	return strings.ToUpper(f.Snake())
}

// Type return the type of the field.
func (f *Integer) Type() libtranslator.PropertyType {
	return IntegerPropertyType
}

// Type return the type of the field.
func (f *Integer) GoType() string {
	return "int32"
}

// Type return the type of the field.
func (f *Integer) GoTypeID() string {
	return f.GoType()
}

// Type return the type of the field.
func (f *Integer) GoNil() string {
	return "nil"
}

// Type return the type of the field.
func (f *Integer) JSONType() string {
	return "float64"
}

// ProtoType return the protobuf type for this field.
func (f *Integer) ProtoType() string {
	return f.GoType()
}

func (f *Integer) ProtoTypeArg() string {
	return f.ProtoType()
}

// ProtoType return the protobuf type for this field.
func (f *Integer) ProtoTypeOptional() string {
	return f.ProtoType()
}

// ProtoType return the protobuf type for this field.
func (f *Integer) DBType() *libtranslator.DBType {
	return &libtranslator.DBType{
		Type:  "sql.NullInt64",
		Value: "Int64",
	}
}

// Type return the type of the field.
func (f *Integer) GraphqlType() string {
	return f.GoType()
}

// Type return the type of the field.
func (f *Integer) GraphqlSchemaType() string {
	return "Int"
}

// GetReference return the name of referenced model, if this field is linked to another model.
func (f *Integer) GetReferenceName() string {
	return ""
}

// GetReferenceDefinition return the definition of referenced model,
// if this field is linked to another model.
func (f *Integer) GetReference() libtranslator.TableInterface {
	return nil
}

// SetReferenceDefinition set the definition of referenced model,
// if this field is linked to another model.
func (f *Integer) SetReference(libtranslator.TableInterface) {
}

// GetInverseProperty return the inverse field, if applicable.
func (f *Integer) GetInverseProperty() string {
	return ""
}

// GetRequired return if this field is required or not.
func (f *Integer) GetRequired() bool {
	return f.Required
}

// GetPrimaryKey return if this field is a primary key or not.
func (f *Integer) GetPrimaryKey() bool {
	return f.PrimaryKey
}

func (f *Integer) IsStored() bool {
	return true
}

func (f *Integer) IsNested() bool {
	return false
}

//GetFieldData return the field information in a format exploimodel by templates.
// func (f *Integer) GetFieldData() *libtranslator.FieldData {
// 	return &libtranslator.FieldData{
// 		Name:            f.Name,
// 		NameWithoutID: f.Name,
// 		// Type:            IntegerPropertyType.GoType(),
// 		// GoType:          IntegerPropertyType.GoType(),
// 		// ProtoType:       IntegerPropertyType.ProtoType(),
// 		DBType: &libtranslator.DBType{
// 			Type:  "sql.NullInt64",
// 			Value: "Int64",
// 		},
// 		Title:            f.Title(),
// 		TitleWithoutID: f.Title(),
// 		LesserTitle:      strings.Title(f.GetName()),
// 		Snake:            f.Snake(),
// 		Required:         f.Required,
// 		Nested:           false,
// 		Reference:        nil,
// 		InverseProperty:     "",
// 		// Definition:       f,
// 	}
// }

func (f *Integer) SetPosition(position int) {
	f.Position = position
}

func (f *Integer) GetPosition() int {
	return f.Position
}

func (r *Integer) IsRepeated() bool {
	return false
}

func (r *Integer) IsTranslatable() bool {
	return false
}

func (r *Integer) GetReturnDetailsInTests() bool {
	return false
}

func (r *Integer) GetLoadedPosition() int {
	return r.LoadedPosition
}
func (r *Integer) GetLoadedPositionMany2one() int {
	return 0
}
func (r *Integer) GetPositionMany2one() int {
	return 0
}
