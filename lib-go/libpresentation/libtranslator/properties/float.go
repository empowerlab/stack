package properties

import (
	// "strings"

	"strings"

	"github.com/iancoleman/strcase"

	"gitlab.com/empowerlab/stack/lib-go/libpresentation/libtranslator"
)

// FloatPropertyType contains the field type for Float
const FloatPropertyType = libtranslator.PropertyType("FloatType")

/*Float is the field type you can use in definition to declare a Float field.
 */
type Float struct {
	Name           string
	String         string
	Required       bool
	Unique         bool
	PrimaryKey     bool
	Store          bool
	TitleName      string
	DBName         string
	LoadedPosition int
	Position       int
}

// GetName return the name of the field.
func (f *Float) GetName() string {
	return f.Name
}

// Title return the title of the field.
func (f *Float) Title() string {
	titleName := strcase.ToCamel(f.Name)
	if f.TitleName != "" {
		titleName = f.TitleName
	}
	return titleName
}

// Title return the title of the field.
func (f *Float) NameWithoutID() string {
	return f.Name
}

// Title return the title of the field.
func (f *Float) TitleWithoutID() string {
	return f.Title()
}

// Snake return the name of the field, in snake_case. This is essentially used for database.
func (f *Float) Snake() string {
	dbName := strcase.ToSnake(f.Name)
	if f.DBName != "" {
		dbName = f.DBName
	}
	return dbName
}

func (f *Float) Upper() string {
	return strings.ToUpper(f.Snake())
}

// Type return the type of the field.
func (f *Float) Type() libtranslator.PropertyType {
	return FloatPropertyType
}

// Type return the type of the field.
func (f *Float) GoType() string {
	return "float64"
}

// Type return the type of the field.
func (f *Float) GoTypeID() string {
	return f.GoType()
}

// Type return the type of the field.
func (f *Float) GoNil() string {
	return "0.0"
}

// Type return the type of the field.
func (f *Float) JSONType() string {
	return f.GoType()
}

// ProtoType return the protobuf type for this field.
func (f *Float) ProtoType() string {
	return "double"
}

func (f *Float) ProtoTypeArg() string {
	return f.ProtoType()
}

// ProtoType return the protobuf type for this field.
func (f *Float) ProtoTypeOptional() string {
	return f.ProtoType()
}

// ProtoType return the protobuf type for this field.
func (f *Float) DBType() *libtranslator.DBType {
	return &libtranslator.DBType{
		Type:  "sql.NullFloat64",
		Value: "Float64",
	}
}

// Type return the type of the field.
func (f *Float) GraphqlType() string {
	return f.GoType()
}

// Type return the type of the field.
func (f *Float) GraphqlSchemaType() string {
	return "Float"
}

// GetReference return the name of referenced model, if this field is linked to another model.
func (f *Float) GetReferenceName() string {
	return ""
}

// GetReferenceDefinition return the definition of referenced model,
// if this field is linked to another model.
func (f *Float) GetReference() libtranslator.TableInterface {
	return nil
}

// SetReferenceDefinition set the definition of referenced model,
// if this field is linked to another model.
func (f *Float) SetReference(libtranslator.TableInterface) {
}

// GetInverseProperty return the inverse field, if applicable.
func (f *Float) GetInverseProperty() string {
	return ""
}

// GetRequired return if this field is required or not.
func (f *Float) GetRequired() bool {
	return f.Required
}

// GetPrimaryKey return if this field is a primary key or not.
func (f *Float) GetPrimaryKey() bool {
	return f.PrimaryKey
}

func (f *Float) IsStored() bool {
	return true
}

func (f *Float) IsNested() bool {
	return false
}

//GetFieldData return the field information in a format exploimodel by templates.
// func (f *Float) GetFieldData() *libtranslator.FieldData {
// 	return &libtranslator.FieldData{
// 		Name:            f.Name,
// 		NameWithoutID: f.Name,
// 		// Type:            FloatPropertyType.GoType(),
// 		// GoType:          FloatPropertyType.GoType(),
// 		// ProtoType:       FloatPropertyType.ProtoType(),
// 		DBType: &libtranslator.DBType{
// 			Type:  "sql.NullFloat64",
// 			Value: "Float64",
// 		},
// 		Title:            f.Title(),
// 		TitleWithoutID: f.Title(),
// 		LesserTitle:      strings.Title(f.GetName()),
// 		Snake:            f.Snake(),
// 		Required:         f.Required,
// 		Nested:           false,
// 		Reference:        nil,
// 		InverseProperty:     "",
// 		// Definition:       f,
// 	}
// }

func (f *Float) SetPosition(position int) {
	f.Position = position
}

func (f *Float) GetPosition() int {
	return f.Position
}

func (r *Float) IsRepeated() bool {
	return false
}

func (r *Float) IsTranslatable() bool {
	return false
}

func (r *Float) GetReturnDetailsInTests() bool {
	return false
}
func (r *Float) GetLoadedPosition() int {
	return r.LoadedPosition
}
func (r *Float) GetLoadedPositionMany2one() int {
	return 0
}
func (r *Float) GetPositionMany2one() int {
	return 0
}
