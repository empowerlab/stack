package libutils

import (
	"fmt"
	"reflect"
	"runtime/debug"
)

// InterfaceSlice todo
func InterfaceSlice(slice interface{}) []interface{} {
	s := reflect.ValueOf(slice)
	if s.Kind() != reflect.Slice {
		panic("InterfaceSlice() given a non-slice type")
	}

	ret := make([]interface{}, s.Len())

	for i := 0; i < s.Len(); i++ {
		ret[i] = s.Index(i).Interface()
	}

	return ret
}

func PrintStack() {
	defer func() {
		if r := recover(); r != nil {
			fmt.Printf("%s %s\n", r, debug.Stack())
		}
	}()
	panic("stack trace")
}