package libutils

import (
	"reflect"
)

// MapKeys todo
func MapKeys(m interface{}) []string {
	keys := reflect.ValueOf(m).MapKeys()
	strKeys := make([]string, len(keys))
	for i := 0; i < len(keys); i++ {
		strKeys[i] = keys[i].String()
	}
	return strKeys
}
