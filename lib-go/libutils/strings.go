package libutils

import (
	"fmt"
	"math/rand"
	"time"
)

// StringInSlice todo
func StringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}

// FilterStringsInSlice todo
func FilterStringsInSlice(testList []string, compareList []string) []string {
	var result []string
	for _, t := range testList {
		for _, c := range compareList {
			if t == c {
				result = append(result, t)
			}
		}
	}
	return result
}

// RemoveStringsInSlice todo
func RemoveStringsInSlice(s []string, r []string) []string {
	var result []string
	for _, sv := range s {
		if !StringInSlice(sv, r) {
			result = append(result, sv)
		}
	}

	// for _, rv := range r {
	// 	for si, sv := range result {
	// 		if sv == rv {
	// 			result = append(result[:si], result[si+1:]...)
	// 		}
	// 	}
	// }
	fmt.Printf("TEST %s %s\n", result, r)
	return result
}

var src = rand.NewSource(time.Now().UnixNano())

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
const (
	letterIdxBits = 6                    // 6 bits to represent a letter index
	letterIdxMask = 1<<letterIdxBits - 1 // All 1-bits, as many as letterIdxBits
	letterIdxMax  = 63 / letterIdxBits   // # of letter indices fitting in 63 bits
)

// GenerateRandomString todo
func GenerateRandomString(n int) string {
	b := make([]byte, n)
	// A src.Int63() generates 63 random bits, enough for letterIdxMax characters!
	for i, cache, remain := n-1, src.Int63(), letterIdxMax; i >= 0; {
		if remain == 0 {
			cache, remain = src.Int63(), letterIdxMax
		}
		if idx := int(cache & letterIdxMask); idx < len(letterBytes) {
			b[i] = letterBytes[idx]
			i--
		}
		cache >>= letterIdxBits
		remain--
	}

	return string(b)
}

type DummyType struct{}