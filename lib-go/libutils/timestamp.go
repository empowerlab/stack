package libutils

import (
	"time"

	ptypes "github.com/golang/protobuf/ptypes/timestamp"
	// ptypes "gitlab.com/empowerlab/stack/lib-go/ptypes/timestamp"
)

func ConvertTimestampFromProto(timestamp *ptypes.Timestamp) time.Time {

	if timestamp == nil {
		return time.Time{}
	}

	return time.Unix(timestamp.Seconds, int64(timestamp.Nanos)).UTC()
}

func ConvertTimestampToProto(timestamp time.Time) *ptypes.Timestamp {

	return &ptypes.Timestamp{Seconds: timestamp.Unix(), Nanos: int32(timestamp.Nanosecond())}
}
