package generator

import (
	"bytes"
	"embed"
	"fmt"
	"go/format"
	"io/ioutil"
	"os"
	"strings"
	"text/template"
	"time"

	"github.com/iancoleman/strcase"

	"gitlab.com/empowerlab/stack/lib-go/libinfrastructure/libpersistence"
	"gitlab.com/empowerlab/stack/lib-go/libinfrastructure/libpersistence/properties"
	"gitlab.com/empowerlab/stack/lib-go/libutils"
)

//go:embed *.go.tmpl
var fdomain embed.FS

// Domain will generate the models files.
func Domains(defs *libpersistence.Definitions) {

	var err error
	fileInit, _ := fdomain.ReadFile("domain-init.go.tmpl")
	initTemplate := template.Must(initFuncs.Parse(string(fileInit)))

	fileExternal, _ := fdomain.ReadFile("domain-external.go.tmpl")
	externalTemplate := template.Must(externalFuncs.Parse(string(fileExternal)))

	file, _ := fdomain.ReadFile("domain-main.go.tmpl")
	domainTemplate := template.Must(aggregateFuncs.Parse(string(file)))

	persistenceFile, _ := fdomain.ReadFile("persistence.go.tmpl")
	persistenceTemplate := template.Must(persistenceFuncs.Parse(string(persistenceFile)))

	event := false
	// if defs.EventClient != nil {
	// 	event = true
	// }

	err = os.MkdirAll("./gen", os.ModePerm)
	if err != nil {
		fmt.Println(err)
	}

	schemas := map[libpersistence.DatasourceClient][]*libpersistence.TableDefinition{
		defs.DatasourceClient: {},
	}
	importsMap := map[string]struct {
		DomainName string
		DomainPath string
		Aggregate  libpersistence.TableInterface
	}{}
	var aggregates []*libpersistence.AggregateDefinition
	for _, definition := range defs.Slice() {

		for _, entity := range definition.GetEntities() {
			if entity.ExternalDatasource != nil {
				if _, ok := schemas[entity.ExternalDatasource]; !ok {
					schemas[entity.ExternalDatasource] = []*libpersistence.TableDefinition{}
				}
				schemas[entity.ExternalDatasource] = append(schemas[entity.ExternalDatasource], entity)
			} else {
				schemas[defs.DatasourceClient] = append(schemas[defs.DatasourceClient], entity)
			}
		}

		d := struct {
			GitRepository    string
			Repository       string
			Event            bool
			DatasourceClient libpersistence.DatasourceClient
			Aggregate        *libpersistence.AggregateDefinition
			CustomCommandsAggregate  []*libpersistence.CustomRequest
			CustomCommandsCollection []*libpersistence.CustomRequest
			Import                   string
		}{
			GitRepository:    defs.GitRepository,
			Repository:       defs.Repository,
			Event:            event,
			DatasourceClient: defs.DatasourceClient,
			Aggregate:        definition,
			CustomCommandsAggregate:  definition.GetCustomRequests(),
			CustomCommandsCollection: definition.AggregateRoot.Commands.CustomCommands,
		}

		imports := map[string]string{}
		for _, entity := range definition.GetEntities() {
			for _, property := range entity.Properties {
				switch v := property.(type) {
				case *properties.Many2oneDefinition:
					if v.ReferencePath != "" {
						path := v.ReferencePrefix + " \"" + v.ReferencePath + "\""
						imports[path] = path
					}
				}
			}
		}
		d.Import = strings.Join(libutils.MapKeys(imports), "\n")

		aggregates = append(aggregates, definition)

		buf := &bytes.Buffer{}
		err := domainTemplate.Execute(buf, d)
		if err != nil {
			fmt.Println(buf)
			fmt.Println("execute ", err)
		}
		content, err := format.Source(buf.Bytes())
		if err != nil {
			fmt.Println("model ", err)
			content = buf.Bytes()
		}
		err = ioutil.WriteFile(
			fmt.Sprintf("./gen/%s.gen.go", strcase.ToSnake(definition.AggregateRoot.Name)),
			content, 0644)
		if err != nil {
			fmt.Println(err)
		}

		buf = &bytes.Buffer{}
		err = persistenceTemplate.Execute(buf, d)
		if err != nil {
			fmt.Println(buf)
			fmt.Println("execute ", err)
		}
		content, err = format.Source(buf.Bytes())
		if err != nil {
			fmt.Println("model ", err)
			content = buf.Bytes()
		}
		err = ioutil.WriteFile(
			fmt.Sprintf("./gen/%s-persistence.gen.go", strcase.ToSnake(definition.AggregateRoot.Name)),
			content, 0644)
		if err != nil {
			fmt.Println(err)
		}

		for _, table := range definition.GetTables() {
			for _, field := range table.StoredProperties() {
				if field.GetReference() != nil {
					if field.GetReference().DomainPath() != defs.Repository {
						importsMap[field.GetReference().GetName()] = struct {
							DomainName string
							DomainPath string
							Aggregate  libpersistence.TableInterface
						}{
							DomainName: field.GetReference().GetName(),
							DomainPath: field.GetReference().DomainPath(),
							Aggregate:  field.GetReference(),
						}
					}
				}
			}
			for _, request := range definition.GetCustomRequests() {
				for _, result := range request.Results {
					if result.GetReference() != nil {
						if result.GetReference().DomainPath() != defs.Repository {
							importsMap[result.GetReference().GetName()] = struct {
								DomainName string
								DomainPath string
								Aggregate  libpersistence.TableInterface
							}{
								DomainName: result.GetReference().GetName(),
								DomainPath: result.GetReference().DomainPath(),
								Aggregate:  result.GetReference(),
							}
						}
					}
				}
			}
		}

	}

	for _, library := range defs.Libraries {
		for _, definition := range library.Slice() {

			for _, entity := range definition.GetEntities() {
				if entity.ExternalDatasource != nil {
					if _, ok := schemas[entity.ExternalDatasource]; !ok {
						schemas[entity.ExternalDatasource] = []*libpersistence.TableDefinition{}
					}
					schemas[entity.ExternalDatasource] = append(schemas[entity.ExternalDatasource], entity)
				} else {
					schemas[defs.DatasourceClient] = append(schemas[defs.DatasourceClient], entity)
				}
			}
		}
	}

	d := struct {
		Repository string
		Aggregates []*libpersistence.AggregateDefinition
	}{
		Repository: defs.Repository,
		Aggregates: aggregates,
	}
	buf := &bytes.Buffer{}
	err = initTemplate.Execute(buf, d)
	if err != nil {
		fmt.Println(buf)
		fmt.Println("execute ", err)
	}
	content, err := format.Source(buf.Bytes())
	if err != nil {
		fmt.Println("model ", err)
		content = buf.Bytes()
	}
	err = ioutil.WriteFile(
		fmt.Sprintf("./gen/init.gen.go"),
		content, 0644)
	if err != nil {
		fmt.Println(err)
	}

	externalAggregates := []struct {
		DomainName string
		DomainPath string
		Aggregate  libpersistence.TableInterface
	}{}
	for _, value := range importsMap {
		externalAggregates = append(externalAggregates, value)
	}
	externalData := struct {
		ExternalAggregates []struct {
			DomainName string
			DomainPath string
			Aggregate  libpersistence.TableInterface
		}
	}{
		ExternalAggregates: externalAggregates,
	}
	buf = &bytes.Buffer{}
	err = externalTemplate.Execute(buf, externalData)
	if err != nil {
		fmt.Println(buf)
		fmt.Println("execute ", err)
	}
	content, err = format.Source(buf.Bytes())
	if err != nil {
		fmt.Println("model ", err)
		content = buf.Bytes()
	}
	err = ioutil.WriteFile(
		"./gen/external.gen.go",
		content, 0644)
	if err != nil {
		fmt.Println(err)
	}

	for datasourceClient, tables := range schemas {
		content := datasourceClient.GenerateSchema(tables)
		if content != "" {
			err = os.MkdirAll(fmt.Sprintf("./gen/%s", strcase.ToLowerCamel(datasourceClient.GetDriverName())), os.ModePerm)
			if err != nil {
				fmt.Println(err)
			}
			err = ioutil.WriteFile(
				fmt.Sprintf("./gen/%s/schema.hcl", strcase.ToLowerCamel(datasourceClient.GetDriverName())),
				[]byte(content), 0644)
			if err != nil {
				fmt.Println(err)
			}
		}
	}
}

var initFuncs = template.New("").Funcs(template.FuncMap{
	"timestamp": func() time.Time {
		return time.Now()
	},
})

var externalFuncs = template.New("").Funcs(template.FuncMap{
	"timestamp": func() time.Time {
		return time.Now()
	},
})

// nolint:lll
var aggregateFuncs = template.New("").Funcs(template.FuncMap{
	"getCustomCommandSingleton": func(aggregate *libpersistence.AggregateDefinition, table libpersistence.TableInterface, customCommand *libpersistence.CustomRequest) string {

		content := `/* {{.Title}} todo
		*/
	   	func (t *{{.ModelTitle}}) {{.Title}}({{.ArgsPrototype}}) ({{.ResultsPrototype}}) {
	   
		   collection := &{{.ModelTitle}}Collection{}
		   collection.Init([]libpersistence.DomainObjectInterface{t})
		   
		   {{.Results}} := {{.ModelTitle}}Aggregate.{{.Title}}(collection, {{.Aggregate.AggregateRoot.Name}}InternalLibrary, {{.Args}})
		   if err != nil {
			   return {{.ReturnKO}}
		   }
		   return {{.ReturnOK}}
	   
	   	}`

		var argsPrototype []string
		var args []string
		for _, arg := range customCommand.Args {
			argsPrototype = append(argsPrototype, arg.GetName()+" "+string(arg.GoType()))
			args = append(args, arg.GetName())
		}

		var resultsPrototype []string
		var results []string
		var returnKO []string
		var returnOK []string
		for _, result := range customCommand.Results {
			resultsPrototype = append(resultsPrototype, string(result.Type()))
			results = append(results, result.GetName())
			returnKO = append(returnKO, "nil")
			returnOK = append(returnOK, result.GetName())
		}
		resultsPrototype = append(resultsPrototype, "error")
		results = append(results, "err")
		returnKO = append(returnKO, "err")
		returnOK = append(returnOK, "nil")

		buf := &bytes.Buffer{}
		err := template.Must(template.New("").Parse(content)).Execute(buf, struct {
			Aggregate        *libpersistence.AggregateDefinition
			ModelTitle       string
			Title            string
			ArgsPrototype    string
			Args             string
			ResultsPrototype string
			Results          string
			ReturnKO         string
			ReturnOK         string
		}{
			Aggregate:        aggregate,
			ModelTitle:       table.Title(),
			Title:            customCommand.Title(),
			ArgsPrototype:    strings.Join(argsPrototype, ","),
			Args:             strings.Join(args, ","),
			ResultsPrototype: strings.Join(resultsPrototype, ","),
			Results:          strings.Join(results, ","),
			ReturnKO:         strings.Join(returnKO, ","),
			ReturnOK:         strings.Join(returnOK, ","),
		})
		if err != nil {
			fmt.Println(err)
		}
		return buf.String()
	},
	"getCustomCommandPrototype": func(customFunc *libpersistence.CustomRequest) string {

		content := "{{.Title}}({{.Args}}) ({{.Results}})"

		var args []string
		for _, arg := range customFunc.Args {
			args = append(args, string(arg.GoType()))
		}

		var results []string
		for _, result := range customFunc.Results {
			results = append(results, string(result.GoType()))
		}
		results = append(results, "error")

		buf := &bytes.Buffer{}
		err := template.Must(template.New("").Parse(content)).Execute(buf, struct {
			Title   string
			Args    string
			Results string
		}{
			Title:   customFunc.Title(),
			Args:    strings.Join(args, ","),
			Results: strings.Join(results, ","),
		})
		if err != nil {
			fmt.Println(err)
		}
		return buf.String()
	},
	"getCustomCommandCollection": func(aggregate *libpersistence.AggregateDefinition, table libpersistence.TableInterface, customCommand *libpersistence.CustomRequest) string {

		content := `/* {{.Title}} todo
		*/
	   	func (c *{{.ModelTitle}}Collection) {{.Title}}({{.ArgsPrototype}}) ({{.ResultsPrototype}}) {
	   
		   {{.Results}} := {{.ModelTitle}}Aggregate.{{.Title}}(c, {{.Aggregate.AggregateRoot.Name}}InternalLibrary, {{.Args}})
		   if err != nil {
			   return {{.ReturnKO}}
		   }
		   return {{.ReturnOK}}
	   
	   	}`

		var argsPrototype []string
		var args []string
		for _, arg := range customCommand.Args {
			argsPrototype = append(argsPrototype, arg.GetName()+" "+string(arg.GoType()))
			args = append(args, arg.GetName())
		}

		var resultsPrototype []string
		var results []string
		var returnKO []string
		var returnOK []string
		for _, result := range customCommand.Results {
			resultsPrototype = append(resultsPrototype, string(result.Type()))
			results = append(results, result.GetName())
			returnKO = append(returnKO, "nil")
			returnOK = append(returnOK, result.GetName())
		}
		resultsPrototype = append(resultsPrototype, "error")
		results = append(results, "err")
		returnKO = append(returnKO, "err")
		returnOK = append(returnOK, "nil")

		buf := &bytes.Buffer{}
		err := template.Must(template.New("").Parse(content)).Execute(buf, struct {
			Aggregate        *libpersistence.AggregateDefinition
			ModelTitle       string
			Title            string
			ArgsPrototype    string
			Args             string
			ResultsPrototype string
			Results          string
			ReturnKO         string
			ReturnOK         string
		}{
			Aggregate:        aggregate,
			ModelTitle:       table.Title(),
			Title:            customCommand.Title(),
			ArgsPrototype:    strings.Join(argsPrototype, ","),
			Args:             strings.Join(args, ","),
			ResultsPrototype: strings.Join(resultsPrototype, ","),
			Results:          strings.Join(results, ","),
			ReturnKO:         strings.Join(returnKO, ","),
			ReturnOK:         strings.Join(returnOK, ","),
		})
		if err != nil {
			fmt.Println(err)
		}
		return buf.String()
	},
	"getCustomCommandAggregatePrototype": func(aggregate *libpersistence.AggregateDefinition, customFunc *libpersistence.CustomRequest, isAggregate bool) string {

		content := "{{.Title}}Custom{{if .IsCommand}}Command{{else}}Query{{end}} func({{if not .OnFactory}}*{{.AggregateTitle}}Collection,{{else}}*libapplication.ApplicationContext,{{end}} *{{.AggregateTitle}}InternalLibrary, *pb.{{.AggregateTitle}}{{.Title}}{{if .IsCommand}}Command{{else}}Query{{end}}) ({{if not .OnFactory}}map[string]{{end}}*pb.{{.AggregateTitle}}{{.Title}}Results, error)"

		var args []string
		for _, arg := range customFunc.Args {
			args = append(args, string(arg.GoType()))
		}

		var results []string
		for _, result := range customFunc.Results {
			goType := result.GoType()
			if result.Type() == properties.Many2onePropertyType {
				goType = "*" + strcase.ToCamel(result.GetReferenceName())
			}
			results = append(results, string(goType))
		}
		results = append(results, "error")

		buf := &bytes.Buffer{}
		err := template.Must(template.New("").Parse(content)).Execute(buf, struct {
			AggregateTitle string
			IsCommand      bool
			OnFactory      bool
			IsAggregate    bool
			Title          string
			Args           string
			Results        string
		}{
			AggregateTitle: aggregate.AggregateRoot.Title(),
			IsCommand:      customFunc.IsCommand(),
			OnFactory:      customFunc.OnFactory,
			IsAggregate:    isAggregate,
			Title:          customFunc.Title(),
			Args:           strings.Join(args, ","),
			Results:        strings.Join(results, ","),
		})
		if err != nil {
			fmt.Println(err)
		}
		return buf.String()
	},
	"getCustomCommandRepositoryPrototype": func(aggregate *libpersistence.AggregateDefinition, customFunc *libpersistence.CustomRequest, isAggregate bool) string {

		content := "{{.Title}}({{if not .IsAggregate}}*{{.AggregateTitle}}Collection,{{else}}*libapplication.ApplicationContext,{{end}} *{{.AggregateTitle}}InternalLibrary, {{.Args}}) ({{.Results}})"

		var args []string
		for _, arg := range customFunc.Args {
			args = append(args, string(arg.GoType()))
		}

		var results []string
		for _, result := range customFunc.Results {
			goType := result.GoType()
			if result.Type() == properties.Many2onePropertyType {
				goType = "*" + strcase.ToCamel(result.GetReferenceName())
			}
			results = append(results, string(goType))
		}
		results = append(results, "error")

		buf := &bytes.Buffer{}
		err := template.Must(template.New("").Parse(content)).Execute(buf, struct {
			AggregateTitle string
			IsAggregate    bool
			Title          string
			Args           string
			Results        string
		}{
			AggregateTitle: aggregate.AggregateRoot.Title(),
			IsAggregate:    isAggregate,
			Title:          customFunc.Title(),
			Args:           strings.Join(args, ","),
			Results:        strings.Join(results, ","),
		})
		if err != nil {
			fmt.Println(err)
		}
		return buf.String()
	},
	"getCustomCommandAggregate": func(aggregate *libpersistence.AggregateDefinition, customCommand *libpersistence.CustomRequest) string {

		content := `/* {{.Title}} todo
		*/
		type {{.Aggregate.AggregateRoot.Title}}{{.Title}}Results struct{
			{{- range .Results}}
			{{.Title}} {{.GoType}}
			{{- end}}
		}
		{{if .OnFactory}}
	   	func (a *{{.Aggregate.AggregateRoot.Title}}Factory) {{.Title}}(command *pb.{{.Aggregate.AggregateRoot.Title}}{{.Title}}{{if .IsCommand}}Command{{else}}Query{{end}}) (*pb.{{.Aggregate.AggregateRoot.Title}}{{.Title}}Results, error) {
	   
		   results, err := {{.Aggregate.AggregateRoot.Title}}Aggregate.{{.Title}}Custom{{if .IsCommand}}Command{{else}}Query{{end}}(a.Workflow, {{.Aggregate.AggregateRoot.Name}}InternalLibrary, command)
		   if err != nil {
			   return nil, err
		   }
		   return results, nil
	   
	   	}
		{{else}}
		func (a *{{.Aggregate.AggregateRoot.Title}}Collection) {{.Title}}(command *pb.{{.Aggregate.AggregateRoot.Title}}{{.Title}}{{if .IsCommand}}Command{{else}}Query{{end}}) (map[string]*pb.{{.Aggregate.AggregateRoot.Title}}{{.Title}}Results, error) {
	   
			results, err := {{.Aggregate.AggregateRoot.Title}}Aggregate.{{.Title}}Custom{{if .IsCommand}}Command{{else}}Query{{end}}(a, {{.Aggregate.AggregateRoot.Name}}InternalLibrary, command)
			if err != nil {
				return nil, err
			}

			if results == nil {
				results = map[string]*pb.{{.Aggregate.AggregateRoot.Title}}{{.Title}}Results{}
			}
			for _, record := range a.Slice() {
				if _, ok := results[record.pb.ID]; !ok {
					results[record.pb.ID] = &pb.{{.Aggregate.AggregateRoot.Title}}{{.Title}}Results{}
				}
			}

			return results, nil
		
		}
		func (a *{{.Aggregate.AggregateRoot.Title}}) {{.Title}}(command *pb.{{.Aggregate.AggregateRoot.Title}}{{.Title}}{{if .IsCommand}}Command{{else}}Query{{end}}) (*pb.{{.Aggregate.AggregateRoot.Title}}{{.Title}}Results, error) {
			
			/*
			collection := &{{.Aggregate.AggregateRoot.Title}}Collection{
				Collection: &libpersistence.Collection{
					Aggregate: {{.Aggregate.AggregateRoot.Title}}Aggregate,
					Workflow: a.Workflow,
				},
			}
			collection.Init([]libpersistence.DomainObjectInterface{a})
			*/

			collection := {{.Aggregate.AggregateRoot.Title}}Aggregate.NewFactory(a.Workflow).NewCollection([]*{{.Aggregate.AggregateRoot.Title}}{a})
	   
			results, err := collection.{{.Title}}(command)
			if err != nil {
				return nil, err
			}

			return results[a.pb.ID], nil
		
		}	
		{{end}}
		`

		var argsPrototype []string
		var args []string
		for _, arg := range customCommand.Args {
			goType := arg.GoType()
			if arg.Type() == properties.Many2onePropertyType {
				goType = "*" + strcase.ToCamel(arg.GetReferenceName())
			}
			argsPrototype = append(argsPrototype, arg.GetName()+" "+goType)
			args = append(args, arg.GetName())
		}

		var resultsPrototype []string
		var results []string
		var returnKO []string
		var returnOK []string
		for _, result := range customCommand.Results {
			goType := result.GoType()
			if result.Type() == properties.Many2onePropertyType {
				goType = "*" + strcase.ToCamel(result.GetReferenceName())
			}
			resultsPrototype = append(resultsPrototype, string(goType))
			results = append(results, result.GetName())
			returnKO = append(returnKO, result.GoNil())
			returnOK = append(returnOK, result.GetName())
		}
		resultsPrototype = append(resultsPrototype, "error")
		results = append(results, "err")
		returnKO = append(returnKO, "err")
		returnOK = append(returnOK, "nil")

		buf := &bytes.Buffer{}
		err := template.Must(template.New("").Parse(content)).Execute(buf, struct {
			Aggregate        *libpersistence.AggregateDefinition
			IsCommand        bool
			OnFactory        bool
			ModelTitle       string
			Title            string
			ArgsPrototype    string
			Args             string
			ResultsPrototype string
			Results          []libpersistence.ArgsInterface
			ReturnKO         string
			ReturnOK         string
		}{
			Aggregate:        aggregate,
			IsCommand:        customCommand.IsCommand(),
			OnFactory:        customCommand.OnFactory,
			Title:            customCommand.Title(),
			ArgsPrototype:    strings.Join(argsPrototype, ","),
			Args:             strings.Join(args, ","),
			ResultsPrototype: strings.Join(resultsPrototype, ","),
			Results:          customCommand.Results,
			ReturnKO:         strings.Join(returnKO, ","),
			ReturnOK:         strings.Join(returnOK, ","),
		})
		if err != nil {
			fmt.Println(err)
		}
		return buf.String()
	},
	"getRecursiveOne2many": func(entity *libpersistence.TableDefinition) []struct {
		Table    libpersistence.TableInterface
		Property *properties.One2manyDefinition
		Path     string
	} {

		results := recursiveOne2ManySearch(entity, "", []struct {
			Table    libpersistence.TableInterface
			Property *properties.One2manyDefinition
			Path     string
		}{})

		for i, j := 0, len(results)-1; i < j; i, j = i+1, j-1 {
			results[i], results[j] = results[j], results[i]
		}

		return results

	},
	"convertJson": func(field libpersistence.Property) string {
		if field.Type() == properties.FloatPropertyType {
			return fmt.Sprintf("%vField := data[\"%v\"].(float64)", field.GetName(), field.GetName())
		}
		return fmt.Sprintf("%vField := data[\"%v\"].(string)", field.GetName(), field.GetName())
	},
	"getImportExternal": func(repository string, aggregate *libpersistence.AggregateDefinition) string {
		importsMap := map[string]string{}
		for _, table := range aggregate.GetTables() {
			for _, field := range table.StoredProperties() {
				if field.GetReference() != nil {
					if field.GetReference().DomainPath() != repository {
						importsMap[field.GetReference().GetName()] = field.GetReference().DomainPath()
					}
				}
			}
		}
		var imports []string
		for name, path := range importsMap {
			imports = append(imports, fmt.Sprintf("%s \"%s/gen\"", name, path))
		}
		return strings.Join(imports, "\n")
	},
	"getExternalEntities": func(repository string, aggregate *libpersistence.AggregateDefinition) string {
		importsMap := map[string]string{}
		for _, table := range aggregate.GetTables() {
			for _, field := range table.StoredProperties() {
				if field.GetReference() != nil {
					if field.GetReference().DomainPath() != repository {
						importsMap[field.GetReference().GetName()] = field.GetReference().Title()
					}
				}
			}
		}
		var imports []string
		for name, entity := range importsMap {
			imports = append(imports, fmt.Sprintf("type %s = %s.%s", entity, name, entity))
			imports = append(imports, fmt.Sprintf("var %sTable = %s.%sTable", entity, name, entity))
		}
		return strings.Join(imports, "\n")
	},
	"timestamp": func() time.Time {
		return time.Now()
	},
})

// nolint:lll
var persistenceFuncs = template.New("").Funcs(template.FuncMap{
	"getCustomCommandPrototype": func(customFunc *libpersistence.CustomRequest) string {

		content := "{{.Title}}({{.Args}}) ({{.Results}})"

		var args []string
		for _, arg := range customFunc.Args {
			args = append(args, string(arg.GoType()))
		}

		var results []string
		for _, result := range customFunc.Results {
			results = append(results, string(result.GoType()))
		}
		results = append(results, "error")

		buf := &bytes.Buffer{}
		err := template.Must(template.New("").Parse(content)).Execute(buf, struct {
			Title   string
			Args    string
			Results string
		}{
			Title:   customFunc.Title(),
			Args:    strings.Join(args, ","),
			Results: strings.Join(results, ","),
		})
		if err != nil {
			fmt.Println(err)
		}
		return buf.String()
	},
	"getCustomCommandRepositoryPrototype": func(aggregate *libpersistence.AggregateDefinition, customFunc *libpersistence.CustomRequest) string {

		content := "{{.Title}}CustomCommand func(*libapplication.Workflow, *gendomain.{{.AggregateTitle}}InternalLibrary, {{.Args}}) ({{.Results}})"

		var args []string
		for _, arg := range customFunc.Args {
			args = append(args, string(arg.GoType()))
		}

		var results []string
		for _, result := range customFunc.Results {
			goType := result.GoType()
			if result.Type() == properties.Many2onePropertyType {
				goType = "*gendomain." + strcase.ToCamel(result.GetReferenceName())
			}
			results = append(results, goType)
		}
		results = append(results, "error")

		buf := &bytes.Buffer{}
		err := template.Must(template.New("").Parse(content)).Execute(buf, struct {
			AggregateTitle string
			IsAggregate    bool
			Title          string
			Args           string
			Results        string
		}{
			AggregateTitle: aggregate.AggregateRoot.Title(),
			Title:          customFunc.Title(),
			Args:           strings.Join(args, ","),
			Results:        strings.Join(results, ","),
		})
		if err != nil {
			fmt.Println(err)
		}
		return buf.String()
	},
	"getCustomCommandRepository": func(aggregate *libpersistence.AggregateDefinition, customCommand *libpersistence.CustomRequest) string {

		content := `/* {{.Title}} todo
		*/
	   	func (a *{{.Aggregate.AggregateRoot.Name}}Repository) {{.Title}}(wk libpersistence.WorkflowInterface, library *gendomain.{{.Aggregate.AggregateRoot.Title}}InternalLibrary, {{.ArgsPrototype}}) ({{.ResultsPrototype}}) {
	   
		   {{.Results}} := a.{{.Title}}CustomCommand(wk.(*libapplication.Workflow), library, {{.Args}})
		   if err != nil {
			   return {{.ReturnKO}}
		   }
		   return {{.ReturnOK}}
	   
	   	}`

		var argsPrototype []string
		var args []string
		for _, arg := range customCommand.Args {
			argsPrototype = append(argsPrototype, arg.GetName()+" "+string(arg.GoType()))
			args = append(args, arg.GetName())
		}

		var resultsPrototype []string
		var results []string
		var returnKO []string
		var returnOK []string
		for _, result := range customCommand.Results {
			resultsPrototype = append(resultsPrototype, string(result.Type()))
			results = append(results, result.GetName())
			returnKO = append(returnKO, "nil")
			returnOK = append(returnOK, result.GetName())
		}
		resultsPrototype = append(resultsPrototype, "error")
		results = append(results, "err")
		returnKO = append(returnKO, "err")
		returnOK = append(returnOK, "nil")

		buf := &bytes.Buffer{}
		err := template.Must(template.New("").Parse(content)).Execute(buf, struct {
			Aggregate        *libpersistence.AggregateDefinition
			ModelTitle       string
			Title            string
			ArgsPrototype    string
			Args             string
			ResultsPrototype string
			Results          string
			ReturnKO         string
			ReturnOK         string
		}{
			Aggregate:        aggregate,
			Title:            customCommand.Title(),
			ArgsPrototype:    strings.Join(argsPrototype, ","),
			Args:             strings.Join(args, ","),
			ResultsPrototype: strings.Join(resultsPrototype, ","),
			Results:          strings.Join(results, ","),
			ReturnKO:         strings.Join(returnKO, ","),
			ReturnOK:         strings.Join(returnOK, ","),
		})
		if err != nil {
			fmt.Println(err)
		}
		return buf.String()
	},
	"getImportExternal": func(aggregate *libpersistence.AggregateDefinition) string {
		var imports []string
		return strings.Join(imports, "\n")
	},
	"getImport": func(datasourceClient libpersistence.DatasourceClient, aggregate *libpersistence.AggregateDefinition) string {
		imports := map[string]string{datasourceClient.GetImportRepository(): datasourceClient.GetImportRepository()}
		for _, entity := range aggregate.GetEntities() {
			if entity.GetExternalDatasource() != nil {
				imports[entity.GetExternalDatasource().GetImportRepository()] = entity.GetExternalDatasource().GetImportRepository()
			}
			for _, property := range entity.StoredProperties() {
				if property.GetExternalDatasource() != nil {
					imports[property.GetExternalDatasource().GetImportRepository()] = property.GetExternalDatasource().GetImportRepository()
				}
			}
		}

		var importsStr []string
		for name := range imports {
			importsStr = append(importsStr, name)
		}
		return strings.Join(importsStr, "\n")
	},
	"getExternalSwitchInsertBuildArgsFunc": func(table libpersistence.TableInterface) string {

		datasources := map[libpersistence.DatasourceClient]libpersistence.DatasourceClient{}
		for _, property := range table.StoredProperties() {
			if property.GetExternalDatasource() != nil {
				datasources[property.GetExternalDatasource()] = property.GetExternalDatasource()
			}
		}
		var scanFuncs []string
		for datasourceClient := range datasources {
			scanFuncs = append(scanFuncs, fmt.Sprintf(`			
			case "%s":
				return s.%s%sInsertBuildArgs(records)`, datasourceClient.GetDriverName(), table.GetName(), datasourceClient.GetDriverName()))
		}
		return strings.Join(scanFuncs, "\n")
	},
	"getExternalInsertBuildArgsFunc": func(table libpersistence.TableInterface) string {

		datasources := map[libpersistence.DatasourceClient]libpersistence.DatasourceClient{}
		for _, property := range table.StoredProperties() {
			if property.GetExternalDatasource() != nil {
				datasources[property.GetExternalDatasource()] = property.GetExternalDatasource()
			}
		}
		var scanFuncs []string
		for datasourceClient := range datasources {
			scanFuncs = append(scanFuncs, datasourceClient.GenerateInsertBuildArgsFunc(datasourceClient, table))
		}
		return strings.Join(scanFuncs, "\n")
	},
	"getScanFunc": func(table libpersistence.TableInterface, datasourceClient libpersistence.DatasourceClient) string {
		if datasourceClient != nil {
			return datasourceClient.GenerateScanFunc(datasourceClient, "", table)
		}
		return fmt.Sprintf(`
		func (d *%sPool) scan(
			rows interface{}, collection *liborm.Collection,
		) ([]libdata.ModelInterface, error) {
			var records []libdata.ModelInterface
			return records, nil
		}`, table.Title())
	},
	"getExternalSwitchScanFunc": func(table libpersistence.TableInterface) string {

		datasources := map[libpersistence.DatasourceClient]libpersistence.DatasourceClient{}
		for _, property := range table.StoredProperties() {
			if property.GetExternalDatasource() != nil {
				datasources[property.GetExternalDatasource()] = property.GetExternalDatasource()
			}
		}
		var scanFuncs []string
		for datasourceClient := range datasources {
			scanFuncs = append(scanFuncs, fmt.Sprintf(`			
			case %s:
				return s.%s%sScan(rows, collection, wk, properties)`, datasourceClient.GetScanType(), table.GetName(), datasourceClient.GetDriverName()))
		}
		return strings.Join(scanFuncs, "\n")
	},
	"getExternalScanFunc": func(table libpersistence.TableInterface) string {

		datasources := map[libpersistence.DatasourceClient]libpersistence.DatasourceClient{}
		for _, property := range table.StoredProperties() {
			if property.GetExternalDatasource() != nil {
				datasources[property.GetExternalDatasource()] = property.GetExternalDatasource()
			}
		}
		var scanFuncs []string
		for datasourceClient := range datasources {
			scanFuncs = append(scanFuncs, datasourceClient.GenerateScanFunc(datasourceClient, datasourceClient.GetDriverName(), table))
		}
		return strings.Join(scanFuncs, "\n")
	},
	// "getImportName": func(field libdata.Field) string {
	// 	return field.GetReference().ExternalDriver.GetImportName()
	// },
	"timestamp": func() time.Time {
		return time.Now()
	},
})

// nolint:lll
var dataTemplate = template.Must(template.New("").Parse(`

`))

// nolint:lll
var initTemplate = template.Must(template.New("").Parse(`
`))

func recursiveOne2ManySearch(
	table libpersistence.TableInterface,
	path string,
	results []struct {
		Table    libpersistence.TableInterface
		Property *properties.One2manyDefinition
		Path     string
	},
) []struct {
	Table    libpersistence.TableInterface
	Property *properties.One2manyDefinition
	Path     string
} {

	for _, property := range table.NestedProperties() {
		if property.Type() == properties.One2manyPropertyType {
			results = append(results, struct {
				Table    libpersistence.TableInterface
				Property *properties.One2manyDefinition
				Path     string
			}{
				Table:    table,
				Property: property.(*properties.One2manyDefinition),
				Path:     path,
			})
			path = path + property.GetName() + "/"
			results = recursiveOne2ManySearch(property.GetReference(), path, results)
		}
	}

	return results
}
