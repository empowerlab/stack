package libpersistence

import (
	// "gitlab.com/empowerlab/stack/lib-go/libdata"
)

type modelInterface interface {
	GetID() string
}

// Model represent a single record.
type Model struct{}

// ModelInterface force to define the function relative to Model.
type ModelInterface interface {
	// Select(
	// 	*WorkflowInterface, []*libdata.Filter,
	// 	[]string, *uint, *string, *string,
	// ) (*Collection, error)
	// Create(*WorkflowInterface, []interface{}) (*Collection, error)
	// Delete(
	// 	*WorkflowInterface, []*libdata.Filter, []string,
	// ) (*Collection, error)
	// SetStore(interface{}) error
	// SetNew(func() interface{}) error
	GetID() string
}

type DomainObjectInterface interface {
	GetID() string
}

// Env contain the context in which the current transaction is occurring.
// type Env struct {
// 	Context     workflow.Context
// 	Transaction *libdata.Transaction
// }
