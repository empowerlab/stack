package properties

import (
	"fmt"

	"github.com/iancoleman/strcase"
	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain"
	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain/properties"
	"gitlab.com/empowerlab/stack/lib-go/libinfrastructure/libpersistence"
)

func init() {
	libpersistence.SetAggregateIDProperty = func(reference string) libpersistence.Property {
		return Many2one("aggregateID",true, reference, nil, libdomain.OnDeleteCascade,false, 3,4, nil, nil)
	}
}

// Many2onePropertyType contains the field type for Many2one
const Many2onePropertyType = libpersistence.PropertyType("Many2oneType")

/*Many2one is the field type you can use in definition to declare a Many2one field.
 */
type Many2oneDefinition struct {
	*properties.IDDefinition
	*PersistenceOptions
	// ReferencePrefix        string
	// ReferencePath          string
	ReferenceDefinition    libpersistence.TableInterface
	// WithEntityInDomain     bool
	// ReturnDetailsInTests   bool
	// LoadedPositionEntity int
	// PositionEntity       int
}

func Many2one(name string, required bool, referenceName string, referenceDefinition libpersistence.TableInterface, onDelete libdomain.OnDeleteValue, withEntity bool, position int, loadedPosition int, options *properties.IDOptions, persistenceOptions *PersistenceOptions) *Many2oneDefinition {

	if referenceDefinition != nil {
		if referenceName != "" {
			panic(fmt.Sprintf("You can't set a reference name and a reference definition: %s", name))
		}
		referenceName = referenceDefinition.GetName()
	}

	idDefinition := properties.ID(name, required, referenceName, nil, onDelete, position, loadedPosition, options)
	if withEntity {
		idDefinition = properties.IDWithEntity(name, required, referenceName, nil, onDelete, position, loadedPosition, position, loadedPosition, options)
	}

	if persistenceOptions == nil {
		persistenceOptions = &PersistenceOptions{}
	}

	result := &Many2oneDefinition{
		IDDefinition: idDefinition,
		PersistenceOptions: persistenceOptions,
	}	
	result.ReferenceDefinition = referenceDefinition
	result.PositionEntity = position
	result.LoadedPositionEntity = loadedPosition
	return result
}

// // GetName return the name of the field.
// func (f *Many2one) GetName() string {
// 	return f.Name
// }

// // Title return the title of the field.
// func (f *Many2one) Title() string {
// 	titleName := strcase.ToCamel(f.Name)
// 	if f.TitleName != "" {
// 		titleName = f.TitleName
// 	}
// 	return titleName
// }

// // Title return the title of the field.
// func (f *Many2one) NameWithoutID() string {
// 	return strings.Replace(f.Name, "ID", "", -1)
// }

// // Title return the title of the field.
// func (f *Many2one) TitleWithoutID() string {
// 	return strings.Replace(f.Title(), "ID", "", -1)
// }

// // Snake return the name of the field, in snake_case. This is essentially used for database.
// func (f *Many2one) Snake() string {
// 	dbName := strcase.ToSnake(f.Name)
// 	if f.DBName != "" {
// 		dbName = f.DBName
// 	}
// 	return dbName
// }

// func (f *Many2one) Upper() string {
// 	return strings.ToUpper(f.Snake())
// }

// // Type return the type of the field.
func (f *Many2oneDefinition) Type() libpersistence.PropertyType {
	return Many2onePropertyType
}

// // Type return the type of the field.
func (f *Many2oneDefinition) GoType() string {
	return "*" + strcase.ToCamel(f.Reference)
	// return libpersistence.STRING
}

// // Type return the type of the field.
// func (f *Many2one) GoTypeID() string {
// 	return "string"
// 	// return libpersistence.STRING
// }

// // Type return the type of the field.
func (f *Many2oneDefinition) GoTypeWithPath() string {
	prefix := ""
	if f.ReferencePrefix != "" {
		prefix = f.ReferencePrefix + "."
	}
	return prefix + strcase.ToCamel(f.Reference)
	// return libpersistence.STRING
}

// // Type return the type of the field.
// func (f *Many2one) GoNil() string {
// 	return "nil"
// }

// // Type return the type of the field.
// func (f *Many2one) JSONType() string {
// 	return libpersistence.STRING
// }

// // ProtoType return the protobuf type for this field.
// func (f *Many2one) ProtoType() string {
// 	return strcase.ToCamel(f.Reference)
// }

// func (f *Many2one) ProtoTypeArg() string {
// 	return "string"
// }

// // ProtoType return the protobuf type for this field.
// func (f *Many2one) ProtoTypeOptional() string {
// 	return f.ProtoType()
// }

// // ProtoType return the protobuf type for this field.
// func (f *Many2one) ProtoTypeOptionalArg() string {
// 	return "string"
// }

// // ProtoType return the protobuf type for this field.
func (f *Many2oneDefinition) DBType() *libpersistence.DBType {
	return &libpersistence.DBType{
		Type:  "sql.NullString",
		Value: "String",
	}
}

// // Type return the type of the field.
// func (f *Many2one) DiffTypeInterface() string {
// 	return "*libdomain.TextDiff"
// }

// // Type return the type of the field.
// func (f *Many2one) GraphqlType() string {
// 	return "graphql.ID"
// }

// // Type return the type of the field.
// func (f *Many2one) GraphqlSchemaType() string {
// 	return "ID"
// }

// GetReference return the name of referenced model, if this field is linked to another model.
// func (f *Many2oneDefinition) GetReferenceName() string {
// 	return f.Reference
// }

// GetReferenceDefinition return the definition of referenced model,
// if this field is linked to another model.
func (f *Many2oneDefinition) GetReference() libpersistence.TableInterface {
	return f.ReferenceDefinition
}

// SetReferenceDefinition set the definition of referenced model,
// if this field is linked to another model.
func (f *Many2oneDefinition) SetReference(d libpersistence.TableInterface) {
	f.ReferenceDefinition = d
}

// // GetInverseProperty return the inverse field, if applicable.
// func (f *Many2one) GetInverseProperty() string {
// 	return ""
// }

// // GetRequired return if this field is required or not.
// func (f *Many2one) GetRequired() bool {
// 	return f.Required
// }

// // GetPrimaryKey return if this field is a primary key or not.
// func (f *Many2one) GetPrimaryKey() bool {
// 	return f.PrimaryKey
// }

// func (f *Many2one) GetWithEntityInDomain() bool {
// 	return f.WithEntityInDomain
// }

// func (f *Many2one) IsStored() bool {
// 	return true
// }

// func (f *Many2one) IsNested() bool {
// 	return false
// }

// //GetFieldData return the field information in a format exploimodel by templates.
// // func (f *Many2one) GetFieldData() *libpersistence.FieldData {

// // 	// var reference *ReferenceData
// // 	// referenceDefinition := f.GetReferenceDefinition()
// // 	// if referenceDefinition != nil {
// // 	// 	reference = &ReferenceData{}
// // 	// 	if referenceDefinition != f.Model {
// // 	// 		reference = &ReferenceData{
// // 	// 			Name:  referenceDefinition.GetTemplateData().Name,
// // 	// 			Title: referenceDefinition.GetTemplateData().Title,
// // 	// 		}
// // 	// 	} else {
// // 	// 		reference = &ReferenceData{
// // 	// 			Name:  result.Name,
// // 	// 			Title: result.Title,
// // 	// 		}
// // 	// 	}
// // 	// }

// // 	return &libpersistence.FieldData{
// // 		Name:            f.Name,
// // 		NameWithoutID: strings.Replace(f.Name, "ID", "", -1),
// // 		Type:            "Many2one",
// // 		// GoType:          Many2onePropertyType.GoType(),
// // 		// ProtoType:       Many2onePropertyType.ProtoType(),
// // 		DBType: &libpersistence.DBType{
// // 			Type:  "sql.NullString",
// // 			Value: "String",
// // 		},
// // 		Title:            f.Title(),
// // 		TitleWithoutID: strings.Replace(f.Title(), "ID", "", -1),
// // 		LesserTitle:      strings.Title(f.GetName()),
// // 		Snake:            f.Snake(),
// // 		Required:         f.Required,
// // 		Nested:           false,
// // 		InverseProperty:     "",
// // 		// Definition:       f,
// // 	}
// // }

// func (f *Many2one) SetPosition(position int) {
// 	f.Position = position
// }

// func (f *Many2one) GetPosition() int {
// 	return f.Position
// }

// func (r *Many2one) IsRepeated() bool {
// 	return false
// }

// func (r *Many2one) IsTranslatable() bool {
// 	return false
// }

// func (r *Many2one) IsIndexed() bool {
// 	return r.Index
// }

// func (r *Many2one) GetReturnDetailsInTests() bool {
// 	return r.ReturnDetailsInTests
// }

// func (r *Many2one) GetLoadedPosition() int {
// 	return r.LoadedPosition
// }
// func (r *Many2one) GetLoadedPositionMany2one() int {
// 	return r.LoadedPositionMany2one
// }
// func (r *Many2one) GetPositionMany2one() int {
// 	return r.PositionMany2one
// }

// func (r *Many2one) GetExternalDatasource() libpersistence.DatasourceClient {
// 	return r.ExternalDatasource
// }

// func (r *Many2one) GetExcludeFromInterface() bool {
// 	return r.ExcludeFromInterface
// }
