package properties

import (
	// "strings"

	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain/properties"
	"gitlab.com/empowerlab/stack/lib-go/libinfrastructure/libpersistence"
)

// JSONPropertyType contains the field type for JSON
const BinaryPropertyType = libpersistence.PropertyType("BinaryType")

/*JSON is the field type you can use in definition to declare a JSON field.
 */
type BinaryDefinition struct {
	*properties.BinaryDefinition
	*PersistenceOptions
}

func Binary(name string, required bool, position int, loadedPosition int,  options *properties.BinaryOptions, persistenceOptions *PersistenceOptions) *BinaryDefinition {

	if persistenceOptions == nil {
		persistenceOptions = &PersistenceOptions{}
	}

	return &BinaryDefinition{
		BinaryDefinition: properties.Binary(name, required, position, loadedPosition, options),
		PersistenceOptions: persistenceOptions,
	}	
}

// Type return the type of the field.
func (f *BinaryDefinition) Type() libpersistence.PropertyType {
	return BinaryPropertyType
}

func (f *BinaryDefinition) DBType() *libpersistence.DBType {
	return &libpersistence.DBType{
		Type:  "sql.NullString",
		Value: "String",
	}
}