package properties

import (
	// "strings"

	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain/properties"
	"gitlab.com/empowerlab/stack/lib-go/libinfrastructure/libpersistence"
)

// IntegerPropertyType contains the field type for Interger
const IntegerPropertyType = libpersistence.PropertyType("IntegerType")

/*Integer is the field type you can use in definition to declare an Integer field.
 */
type IntegerDefinition struct {
	*properties.IntegerDefinition
	*PersistenceOptions
}

func Integer(name string, required bool, position int, loadedPosition int,  options *properties.IntegerOptions, persistenceOptions *PersistenceOptions) *IntegerDefinition {

	if persistenceOptions == nil {
		persistenceOptions = &PersistenceOptions{}
	}
	
	return &IntegerDefinition{
		IntegerDefinition: properties.Integer(name, required, position, loadedPosition, options),
		PersistenceOptions: persistenceOptions,
	}	
}

// Type return the type of the field.
func (f *IntegerDefinition) Type() libpersistence.PropertyType {
	return IntegerPropertyType
}

func (f *IntegerDefinition) DBType() *libpersistence.DBType {
	return &libpersistence.DBType{
		Type:  "sql.NullInt64",
		Value: "Int64",
	}
}

func (f *IntegerDefinition) DiffTypeInterface() string {
	return "*libdomain.IntegerDiff"
}