package properties

import (
	// "strings"

	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain/properties"
	"gitlab.com/empowerlab/stack/lib-go/libinfrastructure/libpersistence"
)

// BooleanPropertyType contains the field type for Boolean
const BooleanPropertyType = libpersistence.PropertyType("BooleanType")

/*Boolean is the field type you can use in definition to declare a Boolean field.
 */
type BooleanDefinition struct {
	*properties.BooleanDefinition
	*PersistenceOptions
}

func Boolean(name string, required bool, position int, loadedPosition int,  options *properties.BooleanOptions, persistenceOptions *PersistenceOptions) *BooleanDefinition {

	if persistenceOptions == nil {
		persistenceOptions = &PersistenceOptions{}
	}
	
	return &BooleanDefinition{
		BooleanDefinition: properties.Boolean(name, required, position, loadedPosition, options),
		PersistenceOptions: persistenceOptions,
	}	
}

func (f *BooleanDefinition) Type() libpersistence.PropertyType {
	return BooleanPropertyType
}
func (f *BooleanDefinition) DBType() *libpersistence.DBType {
	return &libpersistence.DBType{
		Type:  "sql.NullBool",
		Value: "Bool",
	}
}

func (f *BooleanDefinition) DiffTypeInterface() string {
	return "*libdomain.BooleanDiff"
}