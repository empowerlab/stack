package properties

import (
	// "strings"

	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain/properties"
	"gitlab.com/empowerlab/stack/lib-go/libinfrastructure/libpersistence"
)

// DatePropertyType contains the field type for Date
const DatePropertyType = libpersistence.PropertyType("DateType")

/*Date is the field type you can use in definition to declare a Date field.
 */
type DateDefinition struct {
	*properties.DateDefinition
	*PersistenceOptions
}

func Date(name string, required bool, position int, loadedPosition int,  options *properties.DateOptions, persistenceOptions *PersistenceOptions) *DateDefinition {

	if persistenceOptions == nil {
		persistenceOptions = &PersistenceOptions{}
	}
	
	return &DateDefinition{
		DateDefinition: properties.Date(name, required, position, loadedPosition, options),
		PersistenceOptions: persistenceOptions,
	}	
}

// Type return the type of the field.
func (f *DateDefinition) Type() libpersistence.PropertyType {
	return DatePropertyType
}


func (f *DateDefinition) DBType() *libpersistence.DBType {
	return &libpersistence.DBType{
		Type:  "ptypes.Timestamp",
		Value: "",
	}
}

func (f *DateDefinition) DiffTypeInterface() string {
	return "*libdomain.TimeDiff"
}