package properties

import (
	// "strings"

	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain/properties"
	"gitlab.com/empowerlab/stack/lib-go/libinfrastructure/libpersistence"
)

// TextPropertyType contains the field type for Text
const SelectionPropertyType = libpersistence.PropertyType("SelectionType")

/*Text is the field type you can use in definition to declare a Text field.
 */
type SelectionDefinition struct {
	*properties.SelectionDefinition
	*PersistenceOptions
	Options             []string
	// datasource          libpersistence.DatasourceClient
}

func Selection(name string, required bool, options []string, position int, loadedPosition int,  selectionOptions *properties.SelectionOptions, persistenceOptions *PersistenceOptions) *SelectionDefinition {

	if persistenceOptions == nil {
		persistenceOptions = &PersistenceOptions{}
	}
	
	return &SelectionDefinition{
		SelectionDefinition: properties.Selection(name, required, "", nil, position, loadedPosition, selectionOptions),
		PersistenceOptions: persistenceOptions,
		Options: options,
	}	
}

// // GetName return the name of the field.
// func (f *Selection) GetName() string {
// 	return f.Name
// }

// // Title return the title of the field.
// func (f *Selection) Title() string {
// 	titleName := strcase.ToCamel(f.Name)
// 	if f.TitleName != "" {
// 		titleName = f.TitleName
// 	}
// 	return titleName
// }

// // Title return the title of the field.
// func (f *Selection) NameWithoutID() string {
// 	return f.Name
// }

// // Title return the title of the field.
// func (f *Selection) TitleWithoutID() string {
// 	return f.Title()
// }

// // Snake return the name of the field, in snake_case. This is essentially used for database.
// func (f *Selection) Snake() string {
// 	dbName := strcase.ToSnake(f.Name)
// 	if f.DBName != "" {
// 		dbName = f.DBName
// 	}
// 	return dbName
// }

// func (f *Selection) Upper() string {
// 	return strings.ToUpper(f.Snake())
// }

// // Type return the type of the field.
func (f *SelectionDefinition) Type() libpersistence.PropertyType {
	return SelectionPropertyType
}

// // Type return the type of the field.
// func (f *Selection) GoType() string {
// 	return libpersistence.STRING
// }

// // Type return the type of the field.
// func (f *Selection) GoTypeID() string {
// 	return f.GoType()
// }

// // Type return the type of the field.
// func (f *Selection) GoNil() string {
// 	return "\"\""
// }

// // Type return the type of the field.
// func (f *Selection) JSONType() string {
// 	return f.GoType()
// }

// // ProtoType return the protobuf type for this field.
// func (f *Selection) ProtoType() string {
// 	return f.GoType()
// }

// func (f *Selection) ProtoTypeArg() string {
// 	return f.ProtoType()
// }

// // ProtoType return the protobuf type for this field.
// func (f *Selection) ProtoTypeOptional() string {
// 	return f.ProtoType()
// }

// // ProtoType return the protobuf type for this field.
func (f *SelectionDefinition) DBType() *libpersistence.DBType {
	return &libpersistence.DBType{
		Type:  "sql.NullString",
		Value: "String",
	}
}

// // Type return the type of the field.
// func (f *Selection) DiffTypeInterface() string {
// 	return "*libdomain.TextDiff"
// }

// // Type return the type of the field.
// func (f *Selection) GraphqlType() string {
// 	return f.GoType()
// }

// // Type return the type of the field.
// func (f *Selection) GraphqlSchemaType() string {
// 	return "String"
// }

// // GetReference return the name of referenced model, if this field is linked to another model.
// func (f *Selection) GetReferenceName() string {
// 	return ""
// }

// // GetReferenceDefinition return the definition of referenced model,
// // if this field is linked to another model.
func (f *SelectionDefinition) GetReference() libpersistence.TableInterface {
	return nil
}

// // SetReferenceDefinition set the definition of referenced model,
// if this field is linked to another model.
func (f *SelectionDefinition) SetReference(libpersistence.TableInterface) {
}

// // GetInverseProperty return the inverse field, if applicable.
// func (f *Selection) GetInverseProperty() string {
// 	return ""
// }

// // GetRequired return if this field is required or not.
// func (f *Selection) GetRequired() bool {
// 	return f.Required
// }

// // GetPrimaryKey return if this field is a primary key or not.
// func (f *Selection) GetPrimaryKey() bool {
// 	return f.PrimaryKey
// }

// func (f *Selection) GetWithEntityInDomain() bool {
// 	return false
// }

// func (f *Selection) IsStored() bool {
// 	return true
// }

// func (f *Selection) IsNested() bool {
// 	return false
// }

// //GetFieldData return the field information in a format exploimodel by templates.
// // func (f *Selection) GetFieldData() *libpersistence.FieldData {
// // 	return &libpersistence.FieldData{
// // 		Name:            f.Name,
// // 		NameWithoutID: f.Name,
// // 		// Type:            TextPropertyType.GoType(),
// // 		// GoType:          TextPropertyType.GoType(),
// // 		// ProtoType:       TextPropertyType.ProtoType(),
// // 		DBType: &libpersistence.DBType{
// // 			Type:  "sql.NullString",
// // 			Value: "String",
// // 		},
// // 		Title:            f.Title(),
// // 		TitleWithoutID: f.Title(),
// // 		LesserTitle:      strings.Title(f.GetName()),
// // 		Snake:            f.Snake(),
// // 		Required:         f.Required,
// // 		Nested:           false,
// // 		Reference:        nil,
// // 		InverseProperty:     "",
// // 		// Definition:       f,
// // 	}
// // }

// func (f *Selection) SetPosition(position int) {
// 	f.Position = position
// }

// func (f *Selection) GetPosition() int {
// 	return f.Position
// }

// func (r *Selection) IsRepeated() bool {
// 	return false
// }

// func (r *Selection) IsTranslatable() bool {
// 	return r.Translatable
// }

// func (r *Selection) IsIndexed() bool {
// 	return false
// }

// func (r *Selection) GetReturnDetailsInTests() bool {
// 	return false
// }

// func (r *Selection) GetLoadedPosition() int {
// 	return r.LoadedPosition
// }
// func (r *Selection) GetLoadedPositionMany2one() int {
// 	return 0
// }
// func (r *Selection) GetPositionMany2one() int {
// 	return 0
// }

// func (r *Selection) GetExternalDatasource() libpersistence.DatasourceClient {
// 	return r.ExternalDatasource
// }

// func (r *Selection) GetExcludeFromInterface() bool {
// 	return r.ExcludeFromInterface
// }