package libpersistence

import "context"

type Workflow struct {
	Context context.Context
	// WorkflowID string
	// WorkflowStep int
	TenantID string
	JWT string
	JWTDecrypted *JWTClaims
	// UseCache    bool
	// Transaction interface{}
	// TransactionClient       TransactionClient
	// TransactionAlwaysCommit bool
	// TenantClient            DatasourceClient
	// OrchestratorClient OrchestratorClient
	events []*Event
}

func (w *Workflow) AddEvent(event *Event) {
	w.events = append(w.events, event)
}
func (w *Workflow) GetEvents() []*Event {
	return w.events
}

// type WorkflowInterface interface {
// 	Workflow() *Workflow
// 	Commit() error
// }
