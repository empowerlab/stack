package generator

import (
	"bytes"
	"embed"
	"fmt"
	"go/format"
	"io/ioutil"
	"os"
	"strings"
	"text/template"
	"time"

	"github.com/iancoleman/strcase"

	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain"
	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain/properties"
	"gitlab.com/empowerlab/stack/lib-go/libinfrastructure/libactiverecords"
	"gitlab.com/empowerlab/stack/lib-go/libinfrastructure/libpersistence"
	"gitlab.com/empowerlab/stack/lib-go/libutils"
)

//go:embed *.go.tmpl
var fdomain embed.FS

// Domain will generate the models files.
func Domains(defs *libactiverecords.Definitions) {

	var err error
	fileInit, _ := fdomain.ReadFile("init.go.tmpl")
	initTemplate := template.Must(initFuncs.Parse(string(fileInit)))

	file, _ := fdomain.ReadFile("definition.go.tmpl")
	domainTemplate := template.Must(aggregateFuncs.Parse(string(file)))

	fileMapping, _ := fdomain.ReadFile("mapping.go.tmpl")
	mappingTemplate := template.Must(mappingFuncs.Parse(string(fileMapping)))


	err = os.MkdirAll("../gen/definitions", os.ModePerm)
	if err != nil {
		fmt.Println(err)
	}
	err = os.MkdirAll("../gen/mappings", os.ModePerm)
	if err != nil {
		fmt.Println(err)
	}

	var aggregates []*libpersistence.AggregateDefinition
	for _, definition := range defs.Slice() {

		d := struct {
			RepositoryPath string
			PersistencePath string
			ActiveRecord *libactiverecords.ActiveRecordDefinition
			Import string
		}{
			RepositoryPath: defs.RepositoryPath,
			PersistencePath: defs.PersistencePath,
			ActiveRecord: definition,
		}

		imports := map[string]string{}
		// for _, table := range definition.Aggregate.GetEntities() {
		// 	path := table.DomainPackage() + "identities \"" + table.DomainPath() + "/identities\""
		// 	imports[path] = path
		// 	path = table.DomainPackage() + " \"" + table.DomainPath() + "\""
		// 	imports[path] = path
		// 	for _, property := range table.GetProperties() {
		// 		switch v := property.(type) {
		// 		case *properties.OrderedMap:
		// 			if v.GetReference().DomainPath() != "" {
		// 				path := v.GetReference().DomainPackage() + "identities \"" + v.GetReference().DomainPath() + "/identities\""
		// 				imports[path] = path
		// 				path = v.GetReference().DomainPackage() + " \"" + v.GetReference().DomainPath() + "\""
		// 				imports[path] = path
		// 			}
		// 		case *properties.ID:
		// 			if v.GetReference().DomainPath() != "" {
		// 				path := v.GetReference().DomainPackage() + "identities \"" + v.GetReference().DomainPath() + "/identities\""
		// 				imports[path] = path
		// 			}
		// 		case *properties.ValueObject:
		// 			if v.GetReference().DomainPath() != "" {
		// 				path := v.GetReference().DomainPackage() + " \"" + v.GetReference().DomainPath() + "\""
		// 				imports[path] = path
		// 			}
		// 			for _, property2 := range v.GetReference().GetProperties() {
		// 				switch vv := property2.(type) {
		// 				case *properties.OrderedMap:
		// 					if vv.GetReference().DomainPath() != "" {
		// 						path := vv.GetReference().DomainPackage() + "identities \"" + vv.GetReference().DomainPath() + "/identities\""
		// 						imports[path] = path
		// 						path = vv.GetReference().DomainPackage() + " \"" + vv.GetReference().DomainPath() + "\""
		// 						imports[path] = path
		// 					}
		// 				case *properties.ID:
		// 					if vv.GetReference().DomainPath() != "" {
		// 						path := vv.GetReference().DomainPackage() + "identities \"" + vv.GetReference().DomainPath() + "/identities\""
		// 						imports[path] = path								
		// 						if vv.WithEntity {
		// 							path := vv.GetReference().DomainPackage() + " \"" + vv.GetReference().DomainPath() + "\""
		// 							imports[path] = path
		// 						}
		// 					}
		// 				case *properties.ValueObject:
		// 					if vv.GetReference().DomainPath() != "" {
		// 						path := vv.GetReference().DomainPackage() + " \"" + vv.GetReference().DomainPath() + "\""
		// 						imports[path] = path
		// 					}
		// 				}
		// 			}
		// 		}
		// 	}
		// }
		// for _, function := range definition.Aggregate.RepositoryFunctions {
		// 	for _, property := range function.Args {
		// 		switch v := property.(type) {
		// 		case *properties.Entity, *properties.GetProperties:
		// 			if v.GetReference().DomainPath() != "" {
		// 				path := v.GetReference().DomainPackage() + " \"" + v.GetReference().DomainPath() + "\""
		// 				imports[path] = path
		// 			}
		// 		case *properties.ID:
		// 			if v.GetReference().DomainPath() != "" {
		// 				path := v.GetReference().DomainPackage() + "identities \"" + v.GetReference().DomainPath() + "/identities\""
		// 				imports[path] = path
		// 			}
		// 		}
		// 	}
		// }
		d.Import = strings.Join(libutils.MapKeys(imports), "\n")

		buf := &bytes.Buffer{}
		err := domainTemplate.Execute(buf, d)
		if err != nil {
			fmt.Println(buf)
			fmt.Println("execute ", err)
		}
		content, err := format.Source(buf.Bytes())
		if err != nil {
			fmt.Println("model ", err)
			content = buf.Bytes()
		}
		err = ioutil.WriteFile(
			fmt.Sprintf("../gen/definitions/%s.gen.go", strcase.ToSnake(definition.Name())),
			content, 0644)
		if err != nil {
			fmt.Println(err)
		}

		
		buf = &bytes.Buffer{}
		err = mappingTemplate.Execute(buf, d)
		if err != nil {
			fmt.Println(buf)
			fmt.Println("execute ", err)
		}
		content, err = format.Source(buf.Bytes())
		if err != nil {
			fmt.Println("model ", err)
			content = buf.Bytes()
		}
		err = ioutil.WriteFile(
			fmt.Sprintf("../gen/mappings/%s.gen.go", strcase.ToSnake(definition.Name())),
			content, 0644)
		if err != nil {
			fmt.Println(err)
		}

	}

	d := struct {
		Aggregates []*libpersistence.AggregateDefinition
	}{
		Aggregates: aggregates,
	}
	buf := &bytes.Buffer{}
	err = initTemplate.Execute(buf, d)
	if err != nil {
		fmt.Println(buf)
		fmt.Println("execute ", err)
	}
	content, err := format.Source(buf.Bytes())
	if err != nil {
		fmt.Println("model ", err)
		content = buf.Bytes()
	}
	err = ioutil.WriteFile(
		fmt.Sprintf("../gen/init.gen.go"),
		content, 0644)
	if err != nil {
		fmt.Println(err)
	}

}

var initFuncs = template.New("").Funcs(template.FuncMap{
	"timestamp": func() time.Time {
		return time.Now()
	},
})


func generateProperty (activeRecord *libactiverecords.ActiveRecordDefinition, entity *libdomain.EntityDefinition, prefix string, name string, property libdomain.PropertyDefinition) string {

	commonContent := `"{{.Name}}", {{.Property.GetRequired}},`
	commonPositionContent := `{{.Property.GetPosition}}, {{.Property.GetLoadedPosition}},`
	commonOptionsContent := `
		{{- if .Property.GetUnique}}Unique: {{.Property.GetUnique}},{{end}} 
		{{- if .Property.GetPrimaryKey}}PrimaryKey: {{.Property.GetPrimaryKey}},{{end}} 
	`

	name = name
	if prefix != "" {	
		name = prefix + strcase.ToCamel(name)
	}

	common := &bytes.Buffer{}
	err := template.Must(template.New("").Parse(commonContent)).Execute(common, struct {
		ActiveRecord *libactiverecords.ActiveRecordDefinition
		Entity *libdomain.EntityDefinition
		Name string
		Property libdomain.PropertyDefinition
	}{
		ActiveRecord: activeRecord,
		Entity: entity,
		Name: name,
		Property: property,
	})
	if err != nil {
		fmt.Println(err)
	}	

	commonPosition := &bytes.Buffer{}
	err = template.Must(template.New("").Parse(commonPositionContent)).Execute(commonPosition, struct {
		ActiveRecord *libactiverecords.ActiveRecordDefinition
		Entity *libdomain.EntityDefinition
		Name string
		Property libdomain.PropertyDefinition
	}{
		ActiveRecord: activeRecord,
		Entity: entity,
		Name: name,
		Property: property,
	})
	if err != nil {
		fmt.Println(err)
	}	

	commonOptions := &bytes.Buffer{}
	err = template.Must(template.New("").Parse(commonOptionsContent)).Execute(commonOptions, struct {
		ActiveRecord *libactiverecords.ActiveRecordDefinition
		Entity *libdomain.EntityDefinition
		Name string
		Property libdomain.PropertyDefinition
	}{
		ActiveRecord: activeRecord,
		Entity: entity,
		Name: name,
		Property: property,
	})
	if err != nil {
		fmt.Println(err)
	}	

	content := ""
	if property.CollectionType() == properties.OrderedMapPropertyType {
		content = `
		properties.One2many({{.Common}} "{{.ActiveRecord.GetRenamedEntity .Property.GetReference.Name}}", nil, "{{.Property.GetInverseProperty}}", {{.CommonPosition}} &domainproperties.EntityOptions{ {{.CommonOptions}} }, nil),`
	} else if property.CollectionType() == properties.SlicePropertyType {
		content = `
		properties.Many2many(
			{{.Common}} "{{.ActiveRecord.GetRenamedEntity .Property.GetReference.Name}}", nil, "{{.ActiveRecord.GetRenamedEntity .Entity.Name}}{{$.Title}}", 
			"{{.Entity.Name}}ID", "{{.Property.TargetProperty}}", libdomain.{{.Property.PropertyDefinition.OnDelete}}, {{.Property.PropertyDefinition.WithEntity}}, {{.CommonPosition}} &domainproperties.IDOptions{ {{.CommonOptions}} }, nil),`
	} else if property.Type() == properties.ValueObjectPropertyType {
		for _, voProperty := range property.(*properties.ValueObjectDefinition).GetReference().GetProperties() {
			content += generateProperty(activeRecord, entity, name, voProperty.Title(), voProperty)
		}
	} else if property.Type() == properties.IDPropertyType {
		content = `
		properties.Many2one({{.Common}} "{{.ActiveRecord.GetRenamedEntity .Property.GetReference.Name}}", nil, libdomain.{{.Property.OnDelete}}, {{.Property.WithEntity}},{{.CommonPosition}} &domainproperties.IDOptions{ {{.CommonOptions}} }, nil),`
	} else if property.Type() == properties.SelectionPropertyType {
		content = `
		properties.Selection( {{.Common}} []string{
			{{- range .Property.GetReference.Values}}
			"{{.}}",
			{{- end }}
		}, {{.CommonPosition}} &domainproperties.SelectionOptions{ {{.CommonOptions}} }, nil),`
	} else if property.Type() == properties.TextPropertyType {
		if property.(*properties.TextDefinition).Translatable == properties.TranslatableWYSIWYG {
			content = `
			properties.TextWYSIWYG( {{.Common}} {{.Property.Patch}}, {{.CommonPosition}} {{.Property.PositionTerms}}, {{.Property.LoadedPositionTerms}},  &domainproperties.TextOptions{ {{.CommonOptions}} }, nil),`
		} else if property.(*properties.TextDefinition).Translatable == properties.TranslatableSimple {
			content = `
			properties.TextTranslatable( {{.Common}} {{.CommonPosition}} &domainproperties.TextOptions{ {{.CommonOptions}} }, nil),`
		} else if property.(*properties.TextDefinition).Patch {
			content = `
			properties.TextPatch( {{.Common}} {{.CommonPosition}} &domainproperties.TextOptions{ {{.CommonOptions}} }, nil),`
		} else {
			content = `
			properties.Text( {{.Common}} {{.CommonPosition}} &domainproperties.TextOptions{ {{.CommonOptions}} }, nil),`
		}
	} else if property.Type() == properties.BooleanPropertyType{
		content = `
		properties.Boolean( {{.Common}} {{.CommonPosition}} &domainproperties.BooleanOptions{ {{.CommonOptions}} }, nil),`
	} else if property.Type() == properties.IntegerPropertyType {
		content = `
		properties.Integer( {{.Common}} {{.CommonPosition}} &domainproperties.IntegerOptions{ {{.CommonOptions}} }, nil),`
	} else if property.Type() == properties.FloatPropertyType {
		content = `
		properties.Float( {{.Common}} {{.CommonPosition}} &domainproperties.FloatOptions{ {{.CommonOptions}} }, nil),`
	} else if property.Type() == properties.DatetimePropertyType {
		content = `
		properties.Datetime( {{.Common}} {{.CommonPosition}} &domainproperties.DatetimeOptions{ {{.CommonOptions}} }, nil),`
	} else if property.Type() == properties.DatePropertyType {
		content = `
		properties.Date( {{.Common}} {{.CommonPosition}} &domainproperties.DateOptions{ {{.CommonOptions}} }, nil),`
	} else if property.Type() == properties.MapPropertyType {
		content = `
		properties.JSON( {{.Common}} {{.CommonPosition}} &domainproperties.MapOptions{ {{.CommonOptions}} }, nil),`
	}
	
	buf := &bytes.Buffer{}
	err = template.Must(template.New("").Parse(content)).Execute(buf, struct {
		ActiveRecord *libactiverecords.ActiveRecordDefinition
		Entity *libdomain.EntityDefinition
		Property libdomain.PropertyDefinition
		Name string
		Title string
		Common string
		CommonPosition string
		CommonOptions string
	}{
		ActiveRecord: activeRecord,
		Entity: entity,
		Property: property,
		Name: name,
		Title : strcase.ToCamel(name),
		Common: common.String(),
		CommonPosition: commonPosition.String(),
		CommonOptions: commonOptions.String(),
	})
	if err != nil {
		fmt.Println(err)
	}

	return buf.String()
}

// nolint:lll
var aggregateFuncs = template.New("").Funcs(template.FuncMap{
	"generateProperties": func(activeRecord *libactiverecords.ActiveRecordDefinition, entity *libdomain.EntityDefinition, entityProperties []libdomain.PropertyDefinition) string {

		results := ""
		for _, property := range entityProperties {
			results += generateProperty(activeRecord, entity, "", property.GetName(), property)
		}

		return results
			
	},
	"generateDeletedEvent": func(aggregate *libdomain.AggregateDefinition, entity *libdomain.EntityDefinition) bool {
		for _, event := range aggregate.Events {
			if event.Name == entity.Title()+"Deleted" {
				return false
			}
		}
		return true
	},
	"getCustomCommandSingleton": func(aggregate *libpersistence.AggregateDefinition, table libpersistence.TableInterface, customCommand *libpersistence.CustomRequest) string {

		content := `/* {{.Title}} todo
		*/
	   	func (t *{{.ModelTitle}}) {{.Title}}({{.ArgsPrototype}}) ({{.ResultsPrototype}}) {
	   
		   collection := &{{.ModelTitle}}Collection{}
		   collection.Init([]libpersistence.DomainObjectInterface{t})
		   
		   {{.Results}} := {{.ModelTitle}}Aggregate.{{.Title}}(collection, {{.Aggregate.AggregateRoot.Name}}InternalLibrary, {{.Args}})
		   if err != nil {
			   return {{.ReturnKO}}
		   }
		   return {{.ReturnOK}}
	   
	   	}`

		var argsPrototype []string
		var args []string
		for _, arg := range customCommand.Args {
			argsPrototype = append(argsPrototype, arg.GetName()+" "+string(arg.GoType()))
			args = append(args, arg.GetName())
		}

		var resultsPrototype []string
		var results []string
		var returnKO []string
		var returnOK []string
		for _, result := range customCommand.Results {
			resultsPrototype = append(resultsPrototype, string(result.Type()))
			results = append(results, result.GetName())
			returnKO = append(returnKO, "nil")
			returnOK = append(returnOK, result.GetName())
		}
		resultsPrototype = append(resultsPrototype, "error")
		results = append(results, "err")
		returnKO = append(returnKO, "err")
		returnOK = append(returnOK, "nil")

		buf := &bytes.Buffer{}
		err := template.Must(template.New("").Parse(content)).Execute(buf, struct {
			Aggregate        *libpersistence.AggregateDefinition
			ModelTitle       string
			Title            string
			ArgsPrototype    string
			Args             string
			ResultsPrototype string
			Results          string
			ReturnKO         string
			ReturnOK         string
		}{
			Aggregate:        aggregate,
			ModelTitle:       table.Title(),
			Title:            customCommand.Title(),
			ArgsPrototype:    strings.Join(argsPrototype, ","),
			Args:             strings.Join(args, ","),
			ResultsPrototype: strings.Join(resultsPrototype, ","),
			Results:          strings.Join(results, ","),
			ReturnKO:         strings.Join(returnKO, ","),
			ReturnOK:         strings.Join(returnOK, ","),
		})
		if err != nil {
			fmt.Println(err)
		}
		return buf.String()
	},
	"getCustomCommandPrototype": func(customFunc *libpersistence.CustomRequest) string {

		content := "{{.Title}}({{.Args}}) ({{.Results}})"

		var args []string
		for _, arg := range customFunc.Args {
			args = append(args, string(arg.GoType()))
		}

		var results []string
		for _, result := range customFunc.Results {
			results = append(results, string(result.GoType()))
		}
		results = append(results, "error")

		buf := &bytes.Buffer{}
		err := template.Must(template.New("").Parse(content)).Execute(buf, struct {
			Title   string
			Args    string
			Results string
		}{
			Title:   customFunc.Title(),
			Args:    strings.Join(args, ","),
			Results: strings.Join(results, ","),
		})
		if err != nil {
			fmt.Println(err)
		}
		return buf.String()
	},
	"getCustomCommandCollection": func(aggregate *libpersistence.AggregateDefinition, table libpersistence.TableInterface, customCommand *libpersistence.CustomRequest) string {

		content := `/* {{.Title}} todo
		*/
	   	func (c *{{.ModelTitle}}Collection) {{.Title}}({{.ArgsPrototype}}) ({{.ResultsPrototype}}) {
	   
		   {{.Results}} := {{.ModelTitle}}Aggregate.{{.Title}}(c, {{.Aggregate.AggregateRoot.Name}}InternalLibrary, {{.Args}})
		   if err != nil {
			   return {{.ReturnKO}}
		   }
		   return {{.ReturnOK}}
	   
	   	}`

		var argsPrototype []string
		var args []string
		for _, arg := range customCommand.Args {
			argsPrototype = append(argsPrototype, arg.GetName()+" "+string(arg.GoType()))
			args = append(args, arg.GetName())
		}

		var resultsPrototype []string
		var results []string
		var returnKO []string
		var returnOK []string
		for _, result := range customCommand.Results {
			resultsPrototype = append(resultsPrototype, string(result.Type()))
			results = append(results, result.GetName())
			returnKO = append(returnKO, "nil")
			returnOK = append(returnOK, result.GetName())
		}
		resultsPrototype = append(resultsPrototype, "error")
		results = append(results, "err")
		returnKO = append(returnKO, "err")
		returnOK = append(returnOK, "nil")

		buf := &bytes.Buffer{}
		err := template.Must(template.New("").Parse(content)).Execute(buf, struct {
			Aggregate        *libpersistence.AggregateDefinition
			ModelTitle       string
			Title            string
			ArgsPrototype    string
			Args             string
			ResultsPrototype string
			Results          string
			ReturnKO         string
			ReturnOK         string
		}{
			Aggregate:        aggregate,
			ModelTitle:       table.Title(),
			Title:            customCommand.Title(),
			ArgsPrototype:    strings.Join(argsPrototype, ","),
			Args:             strings.Join(args, ","),
			ResultsPrototype: strings.Join(resultsPrototype, ","),
			Results:          strings.Join(results, ","),
			ReturnKO:         strings.Join(returnKO, ","),
			ReturnOK:         strings.Join(returnOK, ","),
		})
		if err != nil {
			fmt.Println(err)
		}
		return buf.String()
	},
	"timestamp": func() time.Time {
		return time.Now()
	},
})

func generateMappingPersistenceProperty (activeRecord *libactiverecords.ActiveRecordDefinition, entity *libdomain.EntityDefinition, valueObjectsPath []string, property libdomain.PropertyDefinition) string {

	content := ""
	if property.CollectionType() == properties.OrderedMapPropertyType {
		content = `func (p *{{$.Entity.Title}}Persistence) {{range .ValueObjectsPath}}{{.}}{{end}}{{.Property.TitleWithoutID}}() []repository.{{.Property.GetReference.Title}}EntityPersistenceInterface {
			var {{.Property.Name}} []repository.{{.Property.GetReference.Title}}EntityPersistenceInterface
			for _, {{.Property.GetReference.Name}} := range p.{{$.ActiveRecord.GetRenamedEntityTitle $.Entity.Name}}.{{range .ValueObjectsPath}}{{.}}{{end}}{{.Property.TitleWithoutID}}() {
				{{.Property.Name}} = append({{.Property.Name}}, &{{.Property.GetReference.Title}}Persistence{ {{.Property.GetReference.Name}} })
			}
			return {{.Property.Name}}
		}
		`
	} else if property.CollectionType() == properties.SlicePropertyType {
		content = `{{- if .Property.GetWithEntity}}
			func (p *{{$.Entity.Title}}Persistence) {{range .ValueObjectsPath}}{{.}}{{end}}{{.Property.TitleWithoutID}}() []repository.{{.Property.GetReference.Title}}EntityPersistenceInterface {
				var {{.Property.Name}} []repository.{{.Property.GetReference.Title}}EntityPersistenceInterface
				for _, {{.Property.GetReference.Title}} := range p.{{$.ActiveRecord.GetRenamedEntityTitle $.Entity.Name}}.{{range .ValueObjectsPath}}{{.}}{{end}}{{.Property.TitleWithoutID}}() {
					{{.Property.Name}} = append({{.Property.Name}}, &{{.Property.GetReference.Title}}Persistence{ {{.Property.GetReference.Title}} })
				}
				return {{.Property.Name}}
			}
			{{- end}}
			`
	} else if property.Type() == properties.ValueObjectPropertyType {
		content = `func (p *{{$.Entity.Title}}Persistence) {{range .ValueObjectsPath}}{{.}}{{end}}{{.Property.Title}}Loaded() bool {
				return true
			}
			`
		for _, voProperty := range property.(*properties.ValueObjectDefinition).GetReference().GetProperties() {
			content += generateMappingPersistenceProperty(activeRecord, entity, append(valueObjectsPath, property.(*properties.ValueObjectDefinition).Title()), voProperty)
		}
	} else if property.Type() == properties.IDPropertyType {
		content = `{{- if .Property.GetWithEntity}}
			func (p *{{.Entity.Title}}Persistence) {{range .ValueObjectsPath}}{{.}}{{end}}{{.Property.TitleWithoutID}}() repository.{{.Property.GetReference.Title}}EntityPersistenceInterface {
				if p.{{$.ActiveRecord.GetRenamedEntityTitle .Entity.Name}}.{{range .ValueObjectsPath}}{{.}}{{end}}{{.Property.TitleWithoutID}}() != nil {
					return &{{.Property.GetReference.Title}}Persistence{p.{{$.ActiveRecord.GetRenamedEntityTitle .Entity.Name}}.{{range .ValueObjectsPath}}{{.}}{{end}}{{.Property.TitleWithoutID}}()}
				}
				return nil
			}
			{{- end}}
			`
	} 
	
	buf := &bytes.Buffer{}
	err := template.Must(template.New("").Parse(content)).Execute(buf, struct {
		ActiveRecord *libactiverecords.ActiveRecordDefinition
		Entity *libdomain.EntityDefinition
		ValueObjectsPath []string
		Property libdomain.PropertyDefinition
	}{
		ActiveRecord: activeRecord,
		Entity: entity,
		ValueObjectsPath: valueObjectsPath,
		Property: property,
	})
	if err != nil {
		fmt.Println(err)
	}

	return buf.String()
}

// nolint:lll
var mappingFuncs = template.New("").Funcs(template.FuncMap{
	"timestamp": func() time.Time {
		return time.Now()
	},	
	"generateMappingPersistenceProperty": func(activeRecord *libactiverecords.ActiveRecordDefinition, entity *libdomain.EntityDefinition, property libdomain.PropertyDefinition) string {

		return generateMappingPersistenceProperty(activeRecord, entity, []string{}, property)
			
	},
})
