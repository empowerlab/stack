package repository

import (
	"fmt"
	"time"
	"os"
	"strings"

	"github.com/google/uuid"

	"gitlab.com/empowerlab/stack/lib-go/libdomain/libapplication"
	"gitlab.com/empowerlab/stack/lib-go/libdomain/libdomain"
	// {{.Repository.Name}} "{{.Repository.Aggregate.AggregateRoot.DomainPath}}"
	{{if .Repository.Aggregate.Events}}
	{{.Repository.Name}}events "{{.Repository.Aggregate.AggregateRoot.DomainPath}}/events"
	{{end}}

	{{.Import}}
)

type {{.Repository.Title}}Repository struct{
	persistence {{.Repository.Title}}PersistenceInterface
}

func New{{.Repository.Title}}Repository(persistence {{.Repository.Title}}PersistenceInterface) *{{.Repository.Title}}Repository {
	return &{{.Repository.Title}}Repository{
		persistence: persistence,
	}
}

type {{.Repository.Title}}PersistenceInterface interface {
	AddAggregate(*libapplication.ApplicationContext, *{{.Repository.Aggregate.AggregateRoot.DomainPackage}}.{{.Repository.Title}}) error
	UpdateFromDiff(*libapplication.ApplicationContext, string, *{{.Repository.Aggregate.AggregateRoot.DomainPackage}}.{{.Repository.Title}}Diff) error
	{{- range .Repository.Aggregate.GetEntities}}
	Get{{.Title}}ByIds(*libapplication.ApplicationContext, []string, *{{$.Repository.Aggregate.AggregateRoot.DomainPackage}}.Get{{.Title}}Properties) ([]{{.Title}}EntityPersistenceInterface, error)
	{{- if $.Repository.GetDeterministicTestIdsForEntity .Name}}
	Get{{.Title}}LastSequence(*libapplication.ApplicationContext) (int, error)
	{{- end}}
	{{- end}}	
}

func (r *{{.Repository.Title}}Repository) Add(wk libdomain.ApplicationContext, {{.Repository.Name}} *{{.Repository.Aggregate.AggregateRoot.DomainPackage}}.{{.Repository.Title}}) error {
	err := r.persistence.AddAggregate(wk.(*libapplication.ApplicationContext), {{.Repository.Name}})
	if err != nil {
		return err
	}

	wk.(*libapplication.ApplicationContext).AppendEventStore({{.Repository.Name}})

	err = r.Save(wk, {{.Repository.Name}})
	if err != nil {
		return err
	}

	return nil
}

{{- range .Repository.Aggregate.Events}}
var Save{{.Title}}Implementation = func(
	r *{{$.Repository.Title}}Repository, wk *libapplication.ApplicationContext, {{$.Repository.Name}} *{{$.Repository.Aggregate.AggregateRoot.DomainPackage}}.{{$.Repository.Title}}, 
	event *{{$.Repository.Name}}events.{{.Title}}Event,
) error {
	return r.persistence.UpdateFromDiff(wk, string({{$.Repository.Name}}.Identity().ID()), event.Diff().(*{{$.Repository.Aggregate.AggregateRoot.DomainPackage}}.{{$.Repository.Title}}Diff))
}
{{- end}}
{{- range .Repository.Aggregate.GetEntities}}
{{if $.Repository.Aggregate.Events}}
var Save{{.Title}}ArchivedImplementation = func(
	r *{{$.Repository.Title}}Repository, wk *libapplication.ApplicationContext, {{$.Repository.Name}} *{{$.Repository.Aggregate.AggregateRoot.DomainPackage}}.{{$.Repository.Title}}, 
	event *{{$.Repository.Name}}events.{{.Title}}ArchivedEvent,
) error {
	return r.persistence.UpdateFromDiff(wk, string({{$.Repository.Name}}.Identity().ID()), event.Diff().(*{{$.Repository.Aggregate.AggregateRoot.DomainPackage}}.{{$.Repository.Title}}Diff))
}
var Save{{.Title}}UnarchivedImplementation = func(
	r *{{$.Repository.Title}}Repository, wk *libapplication.ApplicationContext, {{$.Repository.Name}} *{{$.Repository.Aggregate.AggregateRoot.DomainPackage}}.{{$.Repository.Title}}, 
	event *{{$.Repository.Name}}events.{{.Title}}UnarchivedEvent,
) error {
	return r.persistence.UpdateFromDiff(wk, string({{$.Repository.Name}}.Identity().ID()), event.Diff().(*{{$.Repository.Aggregate.AggregateRoot.DomainPackage}}.{{$.Repository.Title}}Diff))
}
{{if generateDeletedEvent $.Repository.Aggregate .}}
var Save{{.Title}}DeletedImplementation = func(
	r *{{$.Repository.Title}}Repository, wk *libapplication.ApplicationContext, {{$.Repository.Name}} *{{$.Repository.Aggregate.AggregateRoot.DomainPackage}}.{{$.Repository.Title}}, 
	event *{{$.Repository.Name}}events.{{.Title}}DeletedEvent,
) error {
	return r.persistence.UpdateFromDiff(wk, string({{$.Repository.Name}}.Identity().ID()), event.Diff().(*{{$.Repository.Aggregate.AggregateRoot.DomainPackage}}.{{$.Repository.Title}}Diff))
}
{{end}}
{{end}}
{{- end}}

func (r *{{.Repository.Title}}Repository) Save(wk libdomain.ApplicationContext, {{.Repository.Name}} *{{.Repository.Aggregate.AggregateRoot.DomainPackage}}.{{.Repository.Title}}) error {

	{{if .Repository.Aggregate.Events}}

	for _, event := range {{.Repository.Name}}.MutatingEvents() {

		{{- range .Repository.Aggregate.Events}}
		if event.Type() == {{$.Repository.Name}}events.{{.Title}}EventType {
			event := event.(*{{$.Repository.Name}}events.{{.Title}}Event)

			err := Save{{.Title}}Implementation(r, wk.(*libapplication.ApplicationContext), {{$.Repository.Name}}, event)
			if err != nil {
				return err
			}
		}
		{{- end}}
		
		{{- range .Repository.Aggregate.GetEntities}}
		{{if $.Repository.Aggregate.Events}}
		if event.Type() == {{$.Repository.Name}}events.{{.Title}}ArchivedEventType {
			event := event.(*{{$.Repository.Name}}events.{{.Title}}ArchivedEvent)

			err := Save{{.Title}}ArchivedImplementation(r, wk.(*libapplication.ApplicationContext), {{$.Repository.Name}}, event)
			if err != nil {
				return err
			}
		}
		if event.Type() == {{$.Repository.Name}}events.{{.Title}}UnarchivedEventType {
			event := event.(*{{$.Repository.Name}}events.{{.Title}}UnarchivedEvent)

			err := Save{{.Title}}UnarchivedImplementation(r, wk.(*libapplication.ApplicationContext), {{$.Repository.Name}}, event)
			if err != nil {
				return err
			}
		}
		{{if generateDeletedEvent $.Repository.Aggregate .}}
		if event.Type() == {{$.Repository.Name}}events.{{.Title}}DeletedEventType {
			event := event.(*{{$.Repository.Name}}events.{{.Title}}DeletedEvent)

			err := Save{{.Title}}DeletedImplementation(r, wk.(*libapplication.ApplicationContext), {{$.Repository.Name}}, event)
			if err != nil {
				return err
			}
		}
		{{end}}
		{{end}}
		{{- end}}

	}

	{{end}}

	wk.(*libapplication.ApplicationContext).AppendEventStore({{.Repository.Name}})

	return nil
}

{{- range .Repository.Aggregate.GetEntities}}

func (r *{{$.Repository.Title}}Repository) {{.Title}}ByID(wk libdomain.ApplicationContext, id {{$.Repository.Aggregate.AggregateRoot.DomainPackage}}identities.{{.Title}}ID, getProperties *{{$.Repository.Aggregate.AggregateRoot.DomainPackage}}.Get{{.Title}}Properties) (
	*{{$.Repository.Aggregate.AggregateRoot.DomainPackage}}.{{.Title}}, error,
) {

	{{.Name}}ByIds, _, err := r.{{.Title}}ByIds(wk.(*libapplication.ApplicationContext), []{{$.Repository.Aggregate.AggregateRoot.DomainPackage}}identities.{{.Title}}ID{id}, getProperties)
	if err != nil {
		return nil, err
	}

	if v, ok := {{.Name}}ByIds[id]; ok {
		return v, nil
	} else {
		return nil, nil
	}

	// {{if not .IsAggregateRoot}}{{$.Repository.Aggregate.AggregateRoot.Name}}Domain := Convert{{$.Repository.Aggregate.AggregateRoot.Title}}ToDomain({{$.Repository.Aggregate.AggregateRoot.Name}}){{end}}
	// return {{if not .IsAggregateRoot}}{{$.Repository.Aggregate.AggregateRoot.Name}}Domain, {{end}} Convert{{.Title}}ToDomain({{if not .IsAggregateRoot}}{{$.Repository.Aggregate.AggregateRoot.Name}}Domain, {{end}}
	//return {{.Name}}s[0], nil
}

func (r *{{$.Repository.Title}}Repository) {{.Title}}ByIds(wk libdomain.ApplicationContext, {{.Name}}Ids []{{$.Repository.Aggregate.AggregateRoot.DomainPackage}}identities.{{.Title}}ID, getProperties *{{$.Repository.Aggregate.AggregateRoot.DomainPackage}}.Get{{.Title}}Properties) (map[{{$.Repository.Aggregate.AggregateRoot.DomainPackage}}identities.{{.Title}}ID]*{{$.Repository.Aggregate.AggregateRoot.DomainPackage}}.{{.Title}}, map[string]*{{$.Repository.Aggregate.AggregateRoot.DomainPackage}}.{{.Title}}, error) {

	ids := []string{}
	for _, id := range {{.Name}}Ids {
		ids = append(ids, string(id))
	}

	{{.Name}}s, err := r.persistence.Get{{.Title}}ByIds(wk.(*libapplication.ApplicationContext), ids, getProperties)
	if err != nil {
		return nil, nil, err
	}

	resultByIds := map[{{$.Repository.Aggregate.AggregateRoot.DomainPackage}}identities.{{.Title}}ID]*{{$.Repository.Aggregate.AggregateRoot.DomainPackage}}.{{.Title}}{}
	resultByCodes := map[string]*{{$.Repository.Aggregate.AggregateRoot.DomainPackage}}.{{.Title}}{}
	for _, {{.Name}} := range {{.Name}}s {
		{{.Name}}Domain := Convert{{.Title}}ToDomain({{.Name}})
		resultByIds[{{$.Repository.Aggregate.AggregateRoot.DomainPackage}}identities.{{.Title}}ID({{.Name}}.IDString())] = {{.Name}}Domain
		resultByCodes[{{.Name}}.IDString()] = {{.Name}}Domain
	}
	return resultByIds, resultByCodes, nil

}

var {{.Title}}OfExternalIdImplementation = func(wk libdomain.ApplicationContext,aModule string, anExternalTenantId string, getProperties *{{$.Repository.Aggregate.AggregateRoot.DomainPackage}}.Get{{.Title}}Properties) (
	{{if not .IsAggregateRoot}}{{$.Repository.Aggregate.AggregateRoot.Title}}EntityPersistenceInterface, {{end}}{{.Title}}EntityPersistenceInterface, error,
) {
	panic("{{.Title}}OfExternalIdImplementation not implemented")
}

func (r *{{$.Repository.Title}}Repository) {{.Title}}OfExternalId(wk libdomain.ApplicationContext, aModule string, anExternalTenantId string, getProperties *{{$.Repository.Aggregate.AggregateRoot.DomainPackage}}.Get{{.Title}}Properties) (
	{{if not .IsAggregateRoot}}*{{$.Repository.Aggregate.AggregateRoot.DomainPackage}}.{{$.Repository.Aggregate.AggregateRoot.Title}}, {{end}}*{{$.Repository.Aggregate.AggregateRoot.DomainPackage}}.{{.Title}}, error,
) {

	fmt.Println(wk)

	{{if not .IsAggregateRoot}}{{$.Repository.Aggregate.AggregateRoot.Name}}, {{end}}{{.Name}}, err := {{.Title}}OfExternalIdImplementation(wk, aModule, anExternalTenantId, getProperties)
	if err != nil {
		return {{if not .IsAggregateRoot}}nil, {{end}}nil, err
	}

	{{if not .IsAggregateRoot}}{{$.Repository.Aggregate.AggregateRoot.Name}}Domain := Convert{{$.Repository.Aggregate.AggregateRoot.Title}}ToDomain({{$.Repository.Aggregate.AggregateRoot.Name}}){{end}}
	return {{if not .IsAggregateRoot}}{{$.Repository.Aggregate.AggregateRoot.Name}}Domain, {{end}} Convert{{.Title}}ToDomain({{.Name}}), nil
}

var {{.Title}}sByExternalIdsImplementation = func(wk libdomain.ApplicationContext, module string, {{.Name}}ExternalIds []string, getProperties *{{$.Repository.Aggregate.AggregateRoot.DomainPackage}}.Get{{.Title}}Properties) ([]*{{$.Repository.Aggregate.AggregateRoot.DomainPackage}}.{{.Title}}, error) {
	panic("{{.Title}}sByExternalIdsImplementation not implemented")
}

func (r *{{$.Repository.Title}}Repository) {{.Title}}sByExternalIds(wk libdomain.ApplicationContext, module string, {{.Name}}ExternalIds []string, getProperties *{{$.Repository.Aggregate.AggregateRoot.DomainPackage}}.Get{{.Title}}Properties) ([]*{{$.Repository.Aggregate.AggregateRoot.DomainPackage}}.{{.Title}}, error) {

	{{.Name}}s, err := {{.Title}}sByExternalIdsImplementation(wk, module, {{.Name}}ExternalIds, getProperties)
	if err != nil {
		return nil, err
	}

	return {{.Name}}s, nil

}

var {{.Title}}sByCodesImplementation = func(r *{{$.Repository.Title}}Repository, wk libdomain.ApplicationContext, module string, {{.Name}}Codes []string, existingDatas map[string]interface{}, getProperties *{{$.Repository.Aggregate.AggregateRoot.DomainPackage}}.Get{{.Title}}Properties) (map[{{$.Repository.Aggregate.AggregateRoot.DomainPackage}}identities.{{.Title}}ID]*{{$.Repository.Aggregate.AggregateRoot.DomainPackage}}.{{.Title}}, map[string]*{{$.Repository.Aggregate.AggregateRoot.DomainPackage}}.{{.Title}}, error) {
	var {{.Name}}Ids []{{$.Repository.Aggregate.AggregateRoot.DomainPackage}}identities.{{.Title}}ID
	for _, {{.Name}}Code := range {{.Name}}Codes {
		{{.Name}}Ids = append({{.Name}}Ids, {{$.Repository.Aggregate.AggregateRoot.DomainPackage}}identities.{{.Title}}ID({{.Name}}Code))
	}
	return r.{{.Title}}ByIds(wk, {{.Name}}Ids, getProperties)
}

func (r *{{$.Repository.Title}}Repository) {{.Title}}sByCodes(wk libdomain.ApplicationContext, module string, {{.Name}}Codes []string, existingDatas map[string]interface{},  getProperties *{{$.Repository.Aggregate.AggregateRoot.DomainPackage}}.Get{{.Title}}Properties) (map[{{$.Repository.Aggregate.AggregateRoot.DomainPackage}}identities.{{.Title}}ID]*{{$.Repository.Aggregate.AggregateRoot.DomainPackage}}.{{.Title}}, map[string]*{{$.Repository.Aggregate.AggregateRoot.DomainPackage}}.{{.Title}}, error) {

	{{.Name}}ByIds, {{.Name}}ByCodes, err := {{.Title}}sByCodesImplementation(r, wk, module, {{.Name}}Codes, existingDatas, getProperties)
	if err != nil {
		return nil, nil, err
	}

	return {{.Name}}ByIds, {{.Name}}ByCodes, nil

}

var {{.Title}}sImplementation = func(wk libdomain.ApplicationContext, getProperties *{{$.Repository.Aggregate.AggregateRoot.DomainPackage}}.Get{{.Title}}Properties) (interface{}, error) {
	panic("{{.Title}}sImplementation not implemented")
}

func (r *{{$.Repository.Title}}Repository) {{.Title}}s(wk libdomain.ApplicationContext, getProperties *{{$.Repository.Aggregate.AggregateRoot.DomainPackage}}.Get{{.Title}}Properties) ([]*{{$.Repository.Aggregate.AggregateRoot.DomainPackage}}.{{.Title}}, error) {

	{{.Name}}s, err := {{.Title}}sImplementation(wk, getProperties)
	if err != nil {
		return nil, err
	}

	return {{.Name}}s.([]*{{$.Repository.Aggregate.AggregateRoot.DomainPackage}}.{{.Title}}), nil

}
{{- end}}

{{- range .Repository.Aggregate.RepositoryFunctions}}

var {{$.Repository.Title}}{{.Title}}Implementation = func(
	wk libdomain.ApplicationContext,
	{{- range .Args}}
	{{ if eq .CollectionType "Slice" }}
		{{ if eq .Type "EntityType" }}
		{{.GetName}} []*{{.GetReference.DomainPackage}}.{{.GetReference.Title}}{{.InputType}},
		{{ else if eq .Type "ValueObjectType" }}
		{{.GetName}} []*{{.GetReference.DomainPackage}}.{{.GetReference.Title}}{{.InputType}},
		{{- else}}
		{{.GetName}} {{.GoTypeWithPackage}},
		{{- end}}
	{{- else }}	
		{{ if eq .Type "EntityType" }}
		{{.GetName}} *{{.GetReference.DomainPackage}}.{{.GetReference.Title}}{{.InputType}},
		{{ else if eq .Type "ValueObjectType" }}
		{{.GetName}} *{{.GetReference.DomainPackage}}.{{.GetReference.Title}}{{.InputType}},
		{{- else}}
		{{.GetName}} {{.GoTypeWithPackage}},
		{{- end}}	
	{{- end}}
	{{- end}}
) (
	{{- range .Results}}
	{{ if eq .CollectionType "Slice" }}
		{{ if eq .Type "EntityType" }}
		{{.GetName}} []*{{.GetReference.DomainPackage}}.{{.GetReference.Title}}{{.InputType}},
		{{ else if eq .Type "ValueObjectType" }}
		{{.GetName}} []*{{.GetReference.DomainPackage}}.{{.GetReference.Title}}{{.InputType}},
		{{- else}}
		{{.GetName}} {{.GoTypeWithPackage}},
		{{- end}}
	{{- else }}	
		{{ if eq .Type "EntityType" }}
		{{.GetName}} *{{.GetReference.DomainPackage}}.{{.GetReference.Title}}{{.InputType}},
		{{ else if eq .Type "ValueObjectType" }}
		{{.GetName}} *{{.GetReference.DomainPackage}}.{{.GetReference.Title}}{{.InputType}},
		{{- else}}
		{{.GetName}} {{.GoTypeWithPackage}},
		{{- end}}	
	{{- end}}
	{{- end}}
	err error,
) {
	panic("{{$.Repository.Title}}{{.Title}}Implementation not implemented")
}

func (r *{{$.Repository.Title}}Repository) {{.Title}}(
	wk libdomain.ApplicationContext,
	{{- range .Args}}
	{{ if eq .CollectionType "Slice" }}
		{{ if eq .Type "EntityType" }}
		{{.GetName}} []*{{.GetReference.DomainPackage}}.{{.GetReference.Title}}{{.InputType}},
		{{ else if eq .Type "ValueObjectType" }}
		{{.GetName}} []*{{.GetReference.DomainPackage}}.{{.GetReference.Title}}{{.InputType}},
		{{- else}}
		{{.GetName}} {{.GoTypeWithPackage}},
		{{- end}}
	{{- else }}	
		{{ if eq .Type "EntityType" }}
		{{.GetName}} *{{.GetReference.DomainPackage}}.{{.GetReference.Title}}{{.InputType}},
		{{ else if eq .Type "ValueObjectType" }}
		{{.GetName}} *{{.GetReference.DomainPackage}}.{{.GetReference.Title}}{{.InputType}},
		{{- else}}
		{{.GetName}} {{.GoTypeWithPackage}},
		{{- end}}	
	{{- end}}
	{{- end}}
) (
	{{- range .Results}}
	{{ if eq .CollectionType "Slice" }}
		{{ if eq .Type "EntityType" }}
		{{.GetName}} []*{{.GetReference.DomainPackage}}.{{.GetReference.Title}}{{.InputType}},
		{{ else if eq .Type "ValueObjectType" }}
		{{.GetName}} []*{{.GetReference.DomainPackage}}.{{.GetReference.Title}}{{.InputType}},
		{{- else}}
		{{.GetName}} {{.GoTypeWithPackage}},
		{{- end}}
	{{- else }}	
		{{ if eq .Type "EntityType" }}
		{{.GetName}} *{{.GetReference.DomainPackage}}.{{.GetReference.Title}}{{.InputType}},
		{{ else if eq .Type "ValueObjectType" }}
		{{.GetName}} *{{.GetReference.DomainPackage}}.{{.GetReference.Title}}{{.InputType}},
		{{- else}}
		{{.GetName}} {{.GoTypeWithPackage}},
		{{- end}}	
	{{- end}}
	{{- end}}
	err error,
) {
	return {{$.Repository.Title}}{{.Title}}Implementation(
		wk,
		{{- range .Args}}
		{{.GetName}},
		{{- end}}
	)
}


{{- end}}

{{- range .Repository.Aggregate.GetEntities}}
{{$entity := .}}
func (r *{{$.Repository.Title}}Repository) Next{{.Title}}Identity(wk libdomain.ApplicationContext) ({{$.Repository.Aggregate.AggregateRoot.DomainPackage}}identities.{{.Title}}ID, error) {

	{{- if $.Repository.GetDeterministicTestIdsForEntity .Name}}
	if wk.Workflow().TenantID!= "" && string(wk.Workflow().TenantID) == os.Getenv("TEST_TENANT_ID") {

		var err error
		currentSequence := wk.(*libapplication.ApplicationContext).GetDeterministicTestIds("{{.Name}}")
		if currentSequence == 0 {
			currentSequence, err = r.persistence.Get{{.Title}}LastSequence(wk.(*libapplication.ApplicationContext))
			if err != nil {
				return "", err
			}
		}

		newSequence := currentSequence + 1
		wk.(*libapplication.ApplicationContext).SetDeterministicTestIds("{{.Name}}", newSequence)

		tenantPart := strings.Split(string(wk.Workflow().TenantID), "-")[0]
		aggregatePart := "{{$.Repository.GetDeterministicTestIdsForEntity .Name}}"

		newUUID := fmt.Sprintf("%s-%s-%s-%s-%s", tenantPart, aggregatePart, "0000", "0000", fmt.Sprintf("%012d", newSequence))
		return {{$.Repository.Aggregate.AggregateRoot.DomainPackage}}identities.{{.Title}}ID(newUUID), nil
	}
	{{- end}}

	newUUID, err := uuid.NewV7()
	if err != nil {
		return "", err
	}
	return {{$.Repository.Aggregate.AggregateRoot.DomainPackage}}identities.{{.Title}}ID(newUUID.String()), nil
}
{{- range .GetProperties}}
{{- if and (eq .Type "TextType") (eq .Translatable "TranslatableWYSIWYG")}}
var {{$entity.Title}}{{.Title}}UploadAssetsImplementation = func(wk *libapplication.ApplicationContext, entityID {{$.Repository.Aggregate.AggregateRoot.DomainPackage}}identities.{{$entity.Title}}ID, assets []*libdomain.WYSIWYGAssetInput) ([]*libdomain.WYSIWYGAssetInput, error) {
	return UploadAssetsImplementation(wk, "{{$entity.Name}}", string(entityID), "{{.Name}}", assets)
}
func (r *{{$.Repository.Title}}Repository) {{$entity.Title}}{{.Title}}UploadAssets(wk libdomain.ApplicationContext, entityID {{$.Repository.Aggregate.AggregateRoot.DomainPackage}}identities.{{$entity.Title}}ID, assets []*libdomain.WYSIWYGAssetInput) ([]*libdomain.WYSIWYGAssetInput, error) {
	return {{$entity.Title}}{{.Title}}UploadAssetsImplementation(wk.(*libapplication.ApplicationContext),entityID, assets)
}
var {{$entity.Title}}{{.Title}}DeleteAssetsImplementation = func(wk *libapplication.ApplicationContext, entityID {{$.Repository.Aggregate.AggregateRoot.DomainPackage}}identities.{{$entity.Title}}ID, assets []*libdomain.WYSIWYGAssetInput) error {
	return DeleteAssetsImplementation(wk, "{{$entity.Name}}", string(entityID), "{{.Name}}", assets)
}
func (r *{{$.Repository.Title}}Repository) {{$entity.Title}}{{.Title}}DeleteAssets(wk libdomain.ApplicationContext, entityID {{$.Repository.Aggregate.AggregateRoot.DomainPackage}}identities.{{$entity.Title}}ID, assets []*libdomain.WYSIWYGAssetInput) error {
	return {{$entity.Title}}{{.Title}}DeleteAssetsImplementation(wk.(*libapplication.ApplicationContext),entityID, assets)
}
{{- end}}
{{- end}}
{{- end}}
func (r *{{.Repository.Title}}Repository) Get{{.Repository.Title}}History(wk libdomain.ApplicationContext, {{.Repository.Name}}ID {{$.Repository.Aggregate.AggregateRoot.DomainPackage}}identities.{{.Repository.Title}}ID) ([]*libdomain.EventHistory, error) {

	events, err := wk.(*libapplication.ApplicationContext).TransactionClient.GetAggregateHistory(
		wk.(*libapplication.ApplicationContext), {{.Repository.Aggregate.AggregateRoot.DomainPackage}}.{{.Repository.Title}}Definition, string({{.Repository.Name}}ID),
	)
	if err != nil {
		return nil, err
	}

	return events, nil
}
func (r *{{.Repository.Title}}Repository) Get{{.Repository.Title}}Events(wk libdomain.ApplicationContext, {{.Repository.Name}}ID {{$.Repository.Aggregate.AggregateRoot.DomainPackage}}identities.{{.Repository.Title}}ID) ([]libdomain.DomainEvent, error) {

	events, err := wk.(*libapplication.ApplicationContext).TransactionClient.GetAggregateEvents(
		wk.(*libapplication.ApplicationContext), {{.Repository.Aggregate.AggregateRoot.DomainPackage}}.{{.Repository.Title}}Definition, string({{.Repository.Name}}ID),
	)
	if err != nil {
		return nil, err
	}

	fmt.Printf("events: %+v\n", events)

	var results []libdomain.DomainEvent
	{{if .Repository.Aggregate.Events}}
	for _, event := range events {

		{{- range .Repository.Aggregate.Events }}
		if event.Data.Event == "{{.Title}}" {
			results = append(results, {{$.Repository.Name}}events.Regenerate{{.Title}}FromEvents(event.Data.Payload))
		}
		{{- end }}
		{{- range .Repository.Aggregate.GetEntities}}
		if event.Data.Event == "{{.Title}}Archived" {
			results = append(results, {{$.Repository.Name}}events.Regenerate{{.Title}}ArchivedFromEvents(event.Data.Payload))
		}
		if event.Data.Event == "{{.Title}}Unarchived" {
			results = append(results, {{$.Repository.Name}}events.Regenerate{{.Title}}UnarchivedFromEvents(event.Data.Payload))
		}
		{{- if generateDeletedEvent $.Repository.Aggregate .}}
		if event.Data.Event == "{{.Title}}Deleted" {
			results = append(results, {{$.Repository.Name}}events.Regenerate{{.Title}}DeletedFromEvents(event.Data.Payload))
		}
		{{- end}}
		{{- end }}

	}
	{{end}}

	fmt.Printf("events: %+v\n", results)

	return results, nil
}

{{- range .Repository.Aggregate.GetEntities}}
{{$entity := .}}

type {{.Title}}EntityPersistenceInterface interface {
	{{- if and .IsAggregateRoot $.Repository.Aggregate.UseTenants}}
	TenantID() string
	{{- end}}
	IDString() string
	{{- range .Properties}}
	{{ generatePropertyPersistenceInterface . }}
	{{- end}}
	Archived() bool
	ArchivedLoaded() bool
	CreatedAt() time.Time
	CreatedAtLoaded() bool
	CreatedBy() string
	CreatedByLoaded() bool
	UpdatedAt() time.Time
	UpdatedAtLoaded() bool
	UpdatedBy() string
	UpdatedByLoaded() bool
	ArchivedAt() time.Time
	ArchivedAtLoaded() bool
	ArchivedBy() string
	ArchivedByLoaded() bool
	{{- if .IsAggregateRoot}}
	Version() int
	{{- else}}
	// Aggregate() {{$.Repository.Title}}EntityPersistenceInterface
	{{- end}}
}

var Convert{{.Title}}ToDomainImplementation func({{.Name}}Persistence {{.Title}}EntityPersistenceInterface) ({{if and .IsAggregateRoot $.Repository.Aggregate.UseTenants}}string,{{end}} string, *{{$.Repository.Aggregate.AggregateRoot.DomainPackage}}.{{.Title}}Data{{if .IsAggregateRoot}}, int{{end}})
func init() {
	Convert{{.Title}}ToDomainImplementation = func({{.Name}}Persistence {{.Title}}EntityPersistenceInterface) ({{if and .IsAggregateRoot $.Repository.Aggregate.UseTenants}}string,{{end}} string, *{{$.Repository.Aggregate.AggregateRoot.DomainPackage}}.{{.Title}}Data{{if .IsAggregateRoot}}, int{{end}}) {


		{{.Name}}Data := &{{$.Repository.Aggregate.AggregateRoot.DomainPackage}}.{{.Title}}Data{}
		{{- range .Properties}}

		{{ generatePropertyConversionToDomain $entity.Name . }}

		{{- end}}

		if {{.Name}}Persistence.ArchivedLoaded() {
			{{.Name}}Data.Archived = {{$entity.Name}}Persistence.Archived()
			{{.Name}}Data.ArchivedIsLoaded = true
		}
		if {{.Name}}Persistence.CreatedAtLoaded() {
			{{.Name}}Data.CreatedAt = {{$entity.Name}}Persistence.CreatedAt()
			{{.Name}}Data.CreatedAtIsLoaded = true
		}
		if {{.Name}}Persistence.CreatedByLoaded() {
			{{.Name}}Data.CreatedBy = {{$entity.Name}}Persistence.CreatedBy()
			{{.Name}}Data.CreatedByIsLoaded = true
		}
		if {{.Name}}Persistence.UpdatedAtLoaded() {
			{{.Name}}Data.UpdatedAt = {{$entity.Name}}Persistence.UpdatedAt()
			{{.Name}}Data.UpdatedAtIsLoaded = true
		}
		if {{.Name}}Persistence.UpdatedByLoaded() {
			{{.Name}}Data.UpdatedBy = {{$entity.Name}}Persistence.UpdatedBy()
			{{.Name}}Data.UpdatedByIsLoaded = true
		}
		if {{.Name}}Persistence.ArchivedAtLoaded() {
			{{.Name}}Data.ArchivedAt = {{$entity.Name}}Persistence.ArchivedAt()
			{{.Name}}Data.ArchivedAtIsLoaded = true
		}
		if {{.Name}}Persistence.ArchivedByLoaded() {
			{{.Name}}Data.ArchivedBy = {{$entity.Name}}Persistence.ArchivedBy()
			{{.Name}}Data.ArchivedByIsLoaded = true
		}

		return {{if and .IsAggregateRoot $.Repository.Aggregate.UseTenants}}{{.Name}}Persistence.TenantID(),{{end}}{{.Name}}Persistence.IDString(), {{.Name}}Data{{if .IsAggregateRoot}}, {{.Name}}Persistence.Version(){{end}}
	}
}

var Convert{{.Title}}ToDomain = func({{.Name}} {{.Title}}EntityPersistenceInterface) *{{$.Repository.Aggregate.AggregateRoot.DomainPackage}}.{{.Title}} {

	if {{.Name}} == nil || {{.Name}}.IDString() == "" {
		return nil
	}

	{{if and .IsAggregateRoot $.Repository.Aggregate.UseTenants}}tenantID,{{end}} id, data{{if .IsAggregateRoot}}, version{{end}} := Convert{{.Title}}ToDomainImplementation({{.Name}})
	result := {{$.Repository.Aggregate.AggregateRoot.DomainPackage}}.New{{.Title}}(
		{{if and .IsAggregateRoot $.Repository.Aggregate.UseTenants}}libdomain.TenantID(tenantID),{{end}} {{$.Repository.Aggregate.AggregateRoot.DomainPackage}}identities.{{.Title}}ID(id), data,
		{{if not .IsAggregateRoot}}Convert{{$.Repository.Aggregate.AggregateRoot.Title}}ToDomain({{.Name}}.Aggregate()){{else}}version, []libdomain.DomainEvent{}{{end}})
	return result
}
{{- end}}

var _ os.File
var _ strings.Reader